﻿<%@ Page Title="Usuários | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia.admin_usuarios_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li><a href="listagem.aspx">Usuários</a></li>
    <li class="active">Cadastro de Usuários</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Usuários</h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="conteudo_txtNome">Nome de Usuário</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNome" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50" placeholder="insira o nome de usuário"></asp:TextBox>
                        <asp:Label ID="lblNome" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="conteudo_txtEmail">Email</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="100" placeholder="email@email.com" />
                        <asp:Label ID="lblEmail" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="conteudo_txtSenha">Senha</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtSenha" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="20" placeholder="senha" TextMode="Password" />
                        <span class="help-inline col-xs-12 col-sm-7"><span class="middle">Instruções de criação de senha</span> </span>
                        <asp:Label ID="lblSenha" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="conteudo_txtConfirmarSenha">Confirme a Senha</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtConfirmarSenha" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="20" placeholder="senha" TextMode="Password" />
                        <span class="help-inline col-xs-12 col-sm-7"><span class="middle">Instruções de criação de senha</span> </span>
                        <asp:Label ID="lblConfirmarSenha" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        <asp:CheckBox ID="chkAtivo" runat="server" />
						<label class="lbl" for="conteudo_chkAtivo"> Ativo</label>
                        <asp:Label ID="lblAtivo" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-times"></i> Cancelar</button>
                    <button type="submit" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i> Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>