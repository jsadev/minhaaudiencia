﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_usuarios_listagem : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Usuario> usuarios = new Admin.Usuario().ListarUsuario();
            rptListagem.DataSource = usuarios;
            rptListagem.DataBind();
        }

        public string Status(object Ativo)
        {
            if (Convert.ToBoolean(Ativo))
                return "<span class='label label-success middle'>Ativo</span>";
            else
                return "<span class='label label-white middle'>Inativo</span>";
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Usuarios = Request.Form["chkExcluir"].Split(',');
                foreach (string IDUsuario in Usuarios)
                {
                    Entity.Retorno ret = new Admin.Usuario().ExcluirUsuario(IDUsuario);
                    if (!ret.Status)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                        return;
                    }
                }
            }
            MontarListagem();
        }

        protected void lnkExcluir_Command(object sender, CommandEventArgs e)
        {
            Entity.Retorno ret = new Admin.Usuario().ExcluirUsuario(e.CommandArgument);
            if (!ret.Status)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                return;
            }
            MontarListagem();
        }
    }
}