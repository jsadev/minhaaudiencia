﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEntrar_ServerClick(object sender, EventArgs e)
        {
            string Email = txtEmail.Text.ToPrepare();
            string Senha = txtSenha.Text.ToPrepare();

            labMsg.Text = "";

            Entity.Usuario usuario = new Admin.Login().ConsultarLogin(Email, Senha);
            if (usuario.IDUsuario == 0)
            {
                labMsg.Text = "E-mail e/ou Senha inválido.";
                return;
            }

            Session[Constante.Sessions.ADM_LOGIN] = usuario;

            if (Request["u"] == null)
                Response.Redirect("Default.aspx");
            else
                Response.Redirect(Request["u"]);
        }
    }
}