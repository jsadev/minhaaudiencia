﻿<%@ Page Title="Advogados | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia.admin_advogados_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li><a href="listagem.aspx">Advogados</a></li>
    <li class="active">Cadastro de Advogados</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Advogados</h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nome Completo</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNome" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="100" placeholder="insira o nome completo" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblNome" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Tipo</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtTipo" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="100" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"><asp:Label ID="lblTipo" runat="server"></asp:Label></label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCpf" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-cpf" MaxLength="14" placeholder="(apenas números)" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblCpf" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">OAB</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtOab" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="6" placeholder="(apenas números)" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblOab" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Seccional</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtSeccional" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="2" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblSeccional" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Endereço (Rua, Número e Complemento)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtEndereco" runat="server" CssClass="form-control" MaxLength="200" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblEndereco" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Bairro</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtBairro" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50" placeholder="insira o nome completo" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblBairro" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Cidade</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCidade" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50" placeholder="insira o nome completo" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblCidade" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Estado</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" Enabled="false"></asp:DropDownList>
                        <asp:Label ID="lblEstado" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-4">CEP</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCep" runat="server" CssClass="input-medium input-mask-cep form-control" MaxLength="9" placeholder="99999-999" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblCep" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-2">Telefone<small class="text-warning"></small> </label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="ace-icon fa fa-phone"></i></span>
                            <asp:TextBox ID="txtTelefone" runat="server" CssClass="input-mask-phone col-xs-10 col-sm-5" placeholder="+99 (99) 9999-9999" ReadOnly="true"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblTelefone" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-2">Celular<small class="text-warning">(preferencialmente com WhatsApp)</small> </label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="ace-icon fa fa-phone"></i></span>
                            <asp:TextBox ID="txtCelular" runat="server" CssClass="input-mask-cell col-xs-10 col-sm-5" placeholder="+99 (99) 9999-9999" ReadOnly="true"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblCelular" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Site ou Perfil de Redes Socias(opcional)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtSite" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="200" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblSite" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Email</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="col-xs-10 col-sm-5" placeholder="email@email.com" MaxLength="100" ReadOnly="true" />
                        <asp:Label ID="lblEmail" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-11">Apresentação</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtApresentacao" runat="server" CssClass="autosize-transition form-control col-xs-10 col-sm-5" rows="7" TextMode="MultiLine" placeholder="Faça uma pequena apresentação sobre o seu perfil e qualificações profissionais" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblApresentacao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>

                <!-- start Planos -->
                <hr />
                <div class="form-group">
                    <h4 class="col-sm-3 control-label no-padding-right bolder blue">Escolha um Plano</h4>
                    <div class="col-sm-9">
                        <asp:Label ID="lblPlano" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        <asp:Repeater ID="rptPlano" runat="server">
                            <ItemTemplate>
                                <hr />
                                <div>
                                    <input id="rad<%# Eval("IDPlano") %>" type="radio" name="radPlano" class="ace" value="<%# Eval("IDPlano") %>" />
                                    <label class="lbl" for="rad<%# Eval("IDPlano") %>"> <%# Plano(Eval("Valor")) %>
                                </div>
                                <small>Oferecer Audiências e usar como Agenda</small></span>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!-- end Planos -->

                <!-- start Áreas de Atuação -->
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Área(s) de atuação</label>
                    <div id="areas" class="col-sm-9">
                        <asp:Repeater ID="rptArea" runat="server">
                            <ItemTemplate>
                                <input id="chkArea<%# Eval("IDArea") %>" name="chkArea" type="checkbox" class="ace" value="<%# Eval("IDArea") %>" checked="checked" disabled="disabled" />
                                <label for="chkArea<%# Eval("IDArea") %>" class="lbl"> <%# Eval("Nome") %></label>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlCidadeAtuacao" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" Enabled="false"></asp:DropDownList>
                    </div>
                </div>
                <!-- end Áreas de Atuação -->

                <!-- start Fóruns -->
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Selecione os Fóruns que Atende</label>
                    <div id="foruns" class="col-sm-9">
                        <asp:Repeater ID="rptForum" runat="server">
                            <ItemTemplate>
                                <input id="chkForum<%# Eval("IDForum") %>" type="checkbox" name="chkForum" class="ace" value="<%# Eval("IDForum") %>" checked="checked" disabled="disabled" />
                                <label class="lbl col-sm-3" for="chkForum<%# Eval("IDForum") %>"> <%# Eval("Nome") %></label>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!-- end Fóruns -->

                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Informe o Valor Mínimo para receber ofertas</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtValorMinimo" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="Informe o Valor Mínimo para receber ofertas" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblValorMinimo" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Como deseja receber as Notificações?</label>
                    <div class="col-sm-9">
                        <div>
                            <asp:RadioButton ID="radNotificacao1" runat="server" GroupName="radNotificacao" />
                            <label class="lbl" for="conteudo_radNotificacao1"> A Cada interação</label>
                        </div>
                        <div>
                            <asp:RadioButton ID="radNotificacao2" runat="server" GroupName="radNotificacao" />
                            <label class="lbl" for="conteudo_radNotificacao2"> Resumo diário</label>
                        </div>
                        <asp:Label ID="lblNotificacao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        <asp:CheckBox ID="chkAtivo" runat="server" />
						<label class="lbl" for="conteudo_chkAtivo"> Ativo</label>
                        <asp:Label ID="lblAtivo" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-times"></i>Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../assets/js/jquery.maskedinput.min.js"></script>
    <script src="../assets/js/jquery.maskMoney.min.js"></script>
    <script type="text/javascript">
        var root = '<%=ConfigurationManager.AppSettings["Url"]%>';
        var uid = '<%=Request["uid"]%>';

        jQuery(function () {
            $('.input-mask-cep').mask('99999-999');
            $('.input-mask-phone').mask('+99 (99) 9999-9999');
            $('.input-mask-cell').mask('+99 (99) 99999-9999');
            $('.input-mask-money').maskMoney({ thousands: '.', decimal: ',' });

            $('#btnArea').click(function () {
                var area_id = $('#conteudo_ddlArea').val();
                var text = $('#conteudo_ddlArea option:selected').text();

                if (area_id == 0) {
                    alert('Selecione a Área antes de adicionar.');
                    return;
                }

                var areas = $('#areas');

                if (areas.html().indexOf('chkArea' + area_id) == -1) {
                    var html = '<input id="chkArea' + area_id + '" name="chkArea" type="checkbox" class="ace" value="' + area_id + '" checked="checked" />\n';
                    html += '<label for="chkArea' + area_id + '" class="lbl"> ' + text + '</label>\n';

                    areas.append(html);
                }
            });

            $('#conteudo_ddlCidadeAtuacao').change(function () {
                var cidade = $(this).val();

                if (cidade == "") {
                    $('#foruns').html("");
                    return;
                }

                $.ajax({
                    url: '/_Admin/api/advogado/forum-listar.aspx',
                    data: { cidade: cidade },
                    success: function (data) {
                        data = eval('(' + data + ')');

                        var html = "";
                        for (i = 0; i < data.foruns.length; i++) {
                            html += '<input id="chkForum' + data.foruns[i].id + '" name="chkForum" type="checkbox" class="ace" value="' + data.foruns[i].id + '" checked="checked" />\n';
                            html += '<label for="chkForum' + data.foruns[i].id + '" class="lbl"> ' + data.foruns[i].nome + '</label>\n';
                        }

                        $('#foruns').html(html);
                    },
                    error: function (xhr, exception) {
                        switch (xhr.status) {
                            case 404:
                                alert('Requested page not found. [404]');
                                break;
                            case 500:
                                alert('Internal Server Error [500]');
                                break;
                            default:
                                alert(exception);
                                break;
                        }
                    }
                });
            });
        });
    </script>
</asp:Content>