﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_advogados_listagem : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Advogado> advogados = new Admin.Advogado().ListarAdvogado();
            rptListagem.DataSource = advogados;
            rptListagem.DataBind();
        }

        public string Status(object Ativo)
        {
            if (Convert.ToBoolean(Ativo))
                return "<span class='label label-success middle'>Ativo</span>";
            else
                return "<span class='label label-white middle'>Inativo</span>";
        }

        public string AreasAtuacao(object IDAdvogado)
        {
            List<string> areas = new List<string>();

            new Admin.Advogado.Area().ListarArea(IDAdvogado).ForEach(delegate(Entity.Advogado.Area area)
            {
                areas.Add(area.Nome);
            });            
            
            return String.Join(", ", areas);
        }
    }
}