﻿<%@ Page Title="Audiências, Portal | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia._Admin_portal_audiencias_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>
        Portal
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li>
        <a href="listagem.aspx">Audiências</a>
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li class="active"><% Migalha(); %></li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <h3 class="header smaller"><small>Info</small></h3>

    <div class="row-fluid form-horizontal">
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Matéria *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtMateria" runat="server" Width="300px" MaxLength="100"></asp:TextBox>
				    <i id="icoMateria" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblMateria" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Área *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlArea" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoArea" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblArea" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Cidade *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCidade" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
				    <i id="icoCidade" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCidade" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Fórum *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlForum" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoForum" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblForum" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Vara *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlVara" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoVara" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblVara" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Data / Hora *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtData" runat="server" CssClass="_data" Width="150px" MaxLength="16"></asp:TextBox>
				    <i id="icoData" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblData" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Parte que representa *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtParteRepresenta" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
				    <i id="icoParteRepresenta" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblParteRepresenta" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Parte da Audiência *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtParteAudiencia" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
				    <i id="icoParteAudiencia" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblParteAudiencia" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Valor a Pagar *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtValor" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
				    <i id="icoValor" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblValor" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Descrição</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtDescricao" runat="server" Width="600px" Rows="9" TextMode="MultiLine"></asp:TextBox>
				    <i id="icoDescricao" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblDescricao" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
    </div>

    <h3 class="header smaller"><small>Advogados</small></h3>

    <div class="row-fluid form-horizontal">
        <asp:Repeater ID="rptAdvogado" runat="server">
            <ItemTemplate>
                <div class="control-group">
		            <label class="control-label" for="form-field-1"></label>
		            <div class="controls">
                        <div class="form-group">
                            <input type="checkbox" name="chkAdvogado" value="<%# Eval("IDAdvogado") %>" <%# IsAdvogado(Eval("IDAdvogado")) %> />
                            <span class="lbl"> <%# Eval("Nome") %></span>
                        </div>
		            </div>
	            </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div class="form-actions">
		<button id="btnSalvar" runat="server" class="btn btn-success" onserverclick="btnSalvar_ServerClick">
			<i class="icon-ok bigger-110"></i>
			salvar
		</button>
		&nbsp; &nbsp;
		<button class="btn" onclick="window.location='listagem.aspx'; return false;">
			<i class="icon-undo bigger-110"></i>
			cancelar
		</button>
	</div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../../assets/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        jQuery(function () {
            $('._data').mask("99/99/9999 99:99");
        });
    </script>
</asp:Content>