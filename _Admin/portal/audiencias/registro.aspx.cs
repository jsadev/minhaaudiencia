﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class _Admin_portal_audiencias_registro : BaseRegistro
    {
        private DateTime minDate = DateTime.MinValue;
        private decimal minDecimal = decimal.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MontarAreas();
                MontarForuns();
                MontarVaras();
                MontarAdvogados();

                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        private void MontarAreas()
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add("Selecione");
            new Admin.Area().ListarArea().ForEach(delegate(Entity.Area area)
            {
                ddlArea.Items.Add(new ListItem(area.Nome, area.IDArea.ToString()));
            });
        }

        private void MontarForuns()
        {
            ddlForum.Items.Clear();
            ddlForum.Items.Add("Selecione");
            new Admin.Forum().ListarForum().ForEach(delegate(Entity.Forum forum)
            {
                ddlForum.Items.Add(new ListItem(forum.Nome, forum.IDForum.ToString()));
            });
        }

        private void MontarVaras()
        {
            ddlVara.Items.Clear();
            ddlVara.Items.Add("Selecione");
            new Admin.Vara().ListarVara().ForEach(delegate(Entity.Vara vara)
            {
                ddlVara.Items.Add(new ListItem(vara.Nome, vara.IDVara.ToString()));
            });
        }

        private void MontarAdvogados()
        {
            rptAdvogado.DataSource = new Admin.Advogado().ListarAdvogado();
            rptAdvogado.DataBind();
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Nova Audiência");
            else
                Response.Write("Alterar Audiência");
        }

        private void PreencherCampos()
        {
            Entity.Audiencia audiencia = new Admin.Audiencia().ConsultarAudiencia(Request["uid"]);

            txtMateria.Text = audiencia.Materia;
            ddlArea.Items.FindByValue(audiencia.Area_ID.ToString()).Selected = true;
            txtCidade.Text = audiencia.Cidade;
            ddlForum.Items.FindByValue(audiencia.Forum_ID.ToString()).Selected = true;
            ddlVara.Items.FindByValue(audiencia.Vara_ID.ToString()).Selected = true;
            txtData.Text = audiencia.Data.ToString("dd/MM/yyyy HH:mm");
            txtParteRepresenta.Text = audiencia.ParteRepresenta.ToString("n2");
            txtParteAudiencia.Text = audiencia.ParteAudiencia.ToString("n2");
            txtValor.Text = audiencia.Valor.ToString("n2");
            txtDescricao.Text = audiencia.Descricao;
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Materia = txtMateria.Text.ToPrepare();
            string Area = ddlArea.SelectedValue;
            string Cidade = txtCidade.Text.ToPrepare();
            string Forum = ddlForum.SelectedValue;
            string Vara = ddlVara.SelectedValue;
            string Data = txtData.Text.ToPrepare();
            string ParteRepresenta = txtParteRepresenta.Text.ToPrepare();
            string ParteAudiencia = txtParteAudiencia.Text.ToPrepare();
            string Valor = txtValor.Text.ToPrepare();
            string Descricao = txtDescricao.Text.ToPrepare();
            bool Validar = true;

            LimparLabel();

            if (!Materia.isFilled())
            {
                Validar = false;
                lblMateria.Text = "- campo obrigatório";
                txtMateria.BackColor = Color.LightYellow;
                icoMateria.Visible = true;
            }
            if (ddlArea.SelectedIndex == 0)
            {
                Validar = false;
                lblArea.Text = "- campo obrigatório";
                ddlArea.BackColor = Color.LightYellow;
                icoArea.Visible = true;
            }
            if (!Cidade.isFilled())
            {
                Validar = false;
                lblCidade.Text = "- campo obrigatório";
                txtCidade.BackColor = Color.LightYellow;
                icoCidade.Visible = true;
            }
            if (ddlForum.SelectedIndex == 0)
            {
                Validar = false;
                lblForum.Text = "- campo obrigatório";
                ddlForum.BackColor = Color.LightYellow;
                icoForum.Visible = true;
            }
            if (ddlVara.SelectedIndex == 0)
            {
                Validar = false;
                lblVara.Text = "- campo obrigatório";
                ddlVara.BackColor = Color.LightYellow;
                icoVara.Visible = true;
            }
            if (!Data.isFilled())
            {
                Validar = false;
                lblData.Text = "- campo obrigatório";
                txtData.BackColor = Color.LightYellow;
                icoData.Visible = true;
            }
            if (Data.isFilled() && !DateTime.TryParse(Data, out minDate))
            {
                Validar = false;
                lblData.Text = "- data inválida";
                txtData.BackColor = Color.LightYellow;
                icoData.Visible = true;
            }
            if (!ParteRepresenta.isFilled())
            {
                Validar = false;
                lblParteRepresenta.Text = "- campo obrigatório";
                txtParteRepresenta.BackColor = Color.LightYellow;
                icoParteRepresenta.Visible = true;
            }
            if (ParteRepresenta.isFilled() && !decimal.TryParse(ParteRepresenta, out minDecimal))
            {
                Validar = false;
                lblParteRepresenta.Text = "- valor inválido";
                txtParteRepresenta.BackColor = Color.LightYellow;
                icoParteRepresenta.Visible = true;
            }
            if (!ParteAudiencia.isFilled())
            {
                Validar = false;
                lblParteAudiencia.Text = "- campo obrigatório";
                txtParteAudiencia.BackColor = Color.LightYellow;
                icoParteAudiencia.Visible = true;
            }
            if (ParteAudiencia.isFilled() && !decimal.TryParse(ParteAudiencia, out minDecimal))
            {
                Validar = false;
                lblParteAudiencia.Text = "- valor inválido";
                txtParteAudiencia.BackColor = Color.LightYellow;
                icoParteAudiencia.Visible = true;
            }
            if (!Valor.isFilled())
            {
                Validar = false;
                lblValor.Text = "- campo obrigatório";
                txtValor.BackColor = Color.LightYellow;
                icoValor.Visible = true;
            }
            if (Valor.isFilled() && !decimal.TryParse(Valor, out minDecimal))
            {
                Validar = false;
                lblValor.Text = "- valor inválido";
                txtValor.BackColor = Color.LightYellow;
                icoValor.Visible = true;
            }

            if (Validar)
            {
                Entity.Audiencia audiencia = new Entity.Audiencia();
                audiencia.Materia = Materia;
                audiencia.Area_ID = Convert.ToInt32(Area);
                audiencia.Cidade = Cidade;
                audiencia.Forum_ID = Convert.ToInt32(Forum);
                audiencia.Vara_ID = Convert.ToInt32(Vara);
                audiencia.Data = Convert.ToDateTime(Data);
                audiencia.ParteRepresenta = Convert.ToDecimal(ParteRepresenta);
                audiencia.ParteAudiencia = Convert.ToDecimal(ParteAudiencia);
                audiencia.Valor = Convert.ToDecimal(Valor);
                audiencia.Descricao = Descricao;

                if (Request["uid"] != null)
                {
                    audiencia.IDAudiencia = Convert.ToInt32(Request["uid"]);
                    Entity.Retorno ret = new Admin.Audiencia().AlterarAudiencia(audiencia);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    // altera advogados
                    new Admin.Audiencia.Advogado().ExcluirAdvogado(audiencia.IDAudiencia);
                    if (Request.Form["chkAdvogado"] != null)
                    {
                        string[] Advogados = Request.Form["chkAdvogado"].Split(',');
                        foreach (string IDAdvogado in Advogados)
                            new Admin.Audiencia.Advogado().InserirAdvogado(audiencia.IDAudiencia, IDAdvogado);
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                    PreencherCampos();
                }
                else
                {
                    Entity.Retorno ret = new Admin.Audiencia().InserirAudiencia(audiencia);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    // adiciona advogados
                    if (Request.Form["chkAdvogado"] != null)
                    {
                        string[] Advogados = Request.Form["chkAdvogado"].Split(',');
                        foreach (string IDAdvogado in Advogados)
                            new Admin.Audiencia.Advogado().InserirAdvogado(ret.Identity, IDAdvogado);
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Cadastro realizado com sucesso.");
                    LimparCampos();
                }
            }
        }

        public string IsAdvogado(object IDAdvogado)
        {
            if (new Admin.Audiencia.Advogado().ConsultarAdvogado(Request["uid"], IDAdvogado))
                return "checked='checked'";
            else
                return "";
        }
    }
}