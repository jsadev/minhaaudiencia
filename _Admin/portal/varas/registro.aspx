﻿<%@ Page Title="Varas, Portal | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia._Admin_portal_varas_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>
        Portal
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li>
        <a href="listagem.aspx">Varas</a>
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li class="active"><% Migalha(); %></li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <h3 class="header smaller"><small>Info</small></h3>

    <div class="row-fluid form-horizontal">
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Nome *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtNome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
				    <i id="icoNome" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblNome" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
    </div>

    <div class="form-actions">
		<button id="btnSalvar" runat="server" class="btn btn-success" onserverclick="btnSalvar_ServerClick">
			<i class="icon-ok bigger-110"></i>
			salvar
		</button>
		&nbsp; &nbsp;
		<button class="btn" onclick="window.location='listagem.aspx'; return false;">
			<i class="icon-undo bigger-110"></i>
			cancelar
		</button>
	</div>
</asp:Content>