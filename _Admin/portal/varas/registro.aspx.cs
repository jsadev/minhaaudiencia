﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class _Admin_portal_varas_registro : BaseRegistro
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Nova Vara");
            else
                Response.Write("Alterar Vara");
        }

        private void PreencherCampos()
        {
            Entity.Vara vara = new Admin.Vara().ConsultarVara(Request["uid"]);

            txtNome.Text = vara.Nome;
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
                icoNome.Visible = true;
            }

            if (Validar)
            {
                Entity.Vara vara = new Entity.Vara();
                vara.Nome = Nome;

                if (Request["uid"] != null)
                {
                    vara.IDVara = Convert.ToInt32(Request["uid"]);
                    Entity.Retorno ret = new Admin.Vara().AlterarVara(vara);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                    PreencherCampos();
                }
                else
                {
                    Entity.Retorno ret = new Admin.Vara().InserirVara(vara);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Cadastro realizado com sucesso.");
                    LimparCampos();
                }
            }
        }
    }
}