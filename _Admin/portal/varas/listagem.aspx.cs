﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class _Admin_portal_varas_listagem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Vara> varas = new Admin.Vara().ListarVara();
            rptListagem.DataSource = varas;
            rptListagem.DataBind();
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Varas = Request.Form["chkExcluir"].Split(',');
                Entity.Retorno ret = new Admin.Vara().ExcluirVara(Varas);
                if (!ret.Status)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                    return;
                }
            }
            MontarListagem();
        }
    }
}