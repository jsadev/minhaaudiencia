﻿<%@ Page Title="Advogados, Portal | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia._Admin_portal_advogados_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>
        Portal
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li>
        <a href="listagem.aspx">Advogados</a>
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li class="active"><% Migalha(); %></li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <h3 class="header smaller"><small>Info</small></h3>

    <div class="row-fluid form-horizontal">
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Nome *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtNome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
				    <i id="icoNome" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblNome" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">CPF *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCpf" runat="server" CssClass="_cpf" Width="120px" MaxLength="14"></asp:TextBox>
				    <i id="icoCpf" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCpf" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">OAB *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtOab" runat="server" Width="100px" MaxLength="6"></asp:TextBox>
				    <i id="icoOab" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblOab" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Endereço *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtEndereco" runat="server" Width="300px" MaxLength="200"></asp:TextBox>
				    <i id="icoEndereco" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblEndereco" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Bairro *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtBairro" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
				    <i id="icoBairro" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblBairro" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Cidade *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCidade" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
				    <i id="icoCidade" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCidade" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Estado *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlEstado" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoEstado" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblEstado" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">CEP *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCep" runat="server" CssClass="_cep" Width="100px" MaxLength="9"></asp:TextBox>
				    <i id="icoCep" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCep" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Telefone</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtTelefone" runat="server" Width="150px" MaxLength="15"></asp:TextBox>
				    <i id="icoTelefone" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblTelefone" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Celular</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCelular" runat="server" Width="150px" MaxLength="15"></asp:TextBox>
				    <i id="icoCelular" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCelular" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Site ou Perfil de Redes Socias</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtSite" runat="server" Width="300px" MaxLength="200"></asp:TextBox>
				    <i id="icoSite" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblSite" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">E-mail</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtEmail" runat="server" Width="300px" MaxLength="100"></asp:TextBox>
				    <i id="icoEmail" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblEmail" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Senha *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtSenha" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
				    <i id="icoSenha" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblSenha" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Apresentação</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtApresentacao" runat="server" Width="600px" Rows="9" TextMode="MultiLine"></asp:TextBox>
				    <i id="icoApresentacao" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblApresentacao" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Plano *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlPlano" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoPlano" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblPlano" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Valor Mínimo para receber ofertas *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtValorMinimo" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
				    <i id="icoValorMinimo" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblValorMinimo" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Como deseja receber as Notificações?</label>
		    <div class="controls">
                <div class="form-group">
                    <asp:RadioButton ID="rad1" runat="server" GroupName="radNotificacao" Checked="true" />
                    <span class="lbl"> A Cada interação</span>

                    <asp:RadioButton ID="rad2" runat="server" GroupName="radNotificacao" />
                    <span class="lbl"> Resumo diário</span>
                </div>
                <asp:Label ID="lblNotificacao" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
    </div>

    <h3 class="header smaller"><small>Fóruns</small></h3>

    <div class="row-fluid form-horizontal">
        <asp:Repeater ID="rptForum" runat="server">
            <ItemTemplate>
                <div class="control-group">
		            <label class="control-label" for="form-field-1"></label>
		            <div class="controls">
                        <div class="form-group">
                            <input type="checkbox" name="chkForum" value="<%# Eval("IDForum") %>" <%# IsForum(Eval("IDForum")) %> />
                            <span class="lbl"> <%# Eval("Nome") %></span>
                        </div>
		            </div>
	            </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <h3 class="header smaller"><small>Áreas de Atuação</small></h3>

    <div class="row-fluid form-horizontal">
        <asp:Repeater ID="rptArea" runat="server">
            <ItemTemplate>
                <div class="control-group">
		            <label class="control-label" for="form-field-1"></label>
		            <div class="controls">
                        <div class="form-group">
                            <input type="checkbox" name="chkArea" value="<%# Eval("IDArea") %>" <%# IsArea(Eval("IDArea")) %> />
                            <span class="lbl"> <%# Eval("Nome") %></span>
                        </div>
		            </div>
	            </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div class="form-actions">
		<button id="btnSalvar" runat="server" class="btn btn-success" onserverclick="btnSalvar_ServerClick">
			<i class="icon-ok bigger-110"></i>
			salvar
		</button>
		&nbsp; &nbsp;
		<button class="btn" onclick="window.location='listagem.aspx'; return false;">
			<i class="icon-undo bigger-110"></i>
			cancelar
		</button>
	</div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../../assets/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        jQuery(function () {
            $('._cpf').mask("999.999.999-99");
            $('._cep').mask("99999-999");
        });
    </script>
</asp:Content>