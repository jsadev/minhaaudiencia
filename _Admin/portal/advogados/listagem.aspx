﻿<%@ Page Title="Advogados, Portal | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="listagem.aspx.cs" Inherits="minhaaudiencia._Admin_portal_advogados_listagem" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>
        Portal
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li class="active">Advogados</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <div class="page-header position-relative">
        <h1>Advogados</h1>
    </div>

    <div class="row-fluid">
        <button class="btn btn-primary" onclick="window.location='registro.aspx'; return false;">
            <i class="fa fa-plus"></i>
            novo advogado
        </button>

        <button id="btnExcluir" runat="server" class="btn btn-danger" onserverclick="btnExcluir_ServerClick">
            <i class="fa fa-remove"></i>
            excluir selecionado(s)
        </button>
    </div>

    <div class="page-header position-relative">
    </div>

    <div class="row-fluid">
        <table id="table_report" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="center" style="width: 5%">
						<label>
							<input type="checkbox" />
							<span class="lbl"></span>
						</label>
					</th>
					<th>Nome</th>
					<th style="width: 10%"></th>
				</tr>
			</thead>
			<tbody>
				<asp:Repeater ID="rptListagem" runat="server">
                    <ItemTemplate>
                        <tr>
					        <td class="center">
						        <label>
							        <input type="checkbox" name="chkExcluir" value="<%# Eval("IDAdvogado") %>" />
							        <span class="lbl"></span>
						        </label>
					        </td>
					        <td><a href="registro.aspx?uid=<%# Eval("IDAdvogado") %>"><%# Eval("Nome") %></a></td>
					        <td class="td-actions">
							    <button class="btn btn-mini btn-info" title="Alterar" onclick="window.location='registro.aspx?uid=<%# Eval("IDAdvogado") %>'; return false;">
								    <i class="icon-edit"></i>
							    </button>
					        </td>
				        </tr>
                    </ItemTemplate>
				</asp:Repeater>
			</tbody>
		</table>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../../assets/js/jquery.dataTables.min.js"></script>
    <script src="../../assets/js/jquery.dataTables.bootstrap.js"></script>

    <script type="text/javascript">
        $(function () {
            var oTable1 = $('#table_report').dataTable({
                "oLanguage": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Exibir _MENU_ resultados por pagína",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
                "aoColumns": [
                    { "bSortable": false },
                    null,
                    { "bSortable": false }
                ]
            });

            $('table th input:checkbox').on('click', function () {
                var that = this;
                $(this).closest('table').find('tr > td:first-child input:checkbox').each(function () {
                    if ($(this).attr("disabled") != "disabled") {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    }
                });
            });

            $('[data-rel=tooltip]').tooltip();
        });
	</script>
</asp:Content>