﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class _Admin_portal_advogados_registro : BaseRegistro
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                new Util.Estado(ddlEstado);
                MontarPlanos();
                MontarForuns();
                MontarAreas();

                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        private void MontarPlanos()
        {
            ddlPlano.Items.Add("Selecione");
            ddlPlano.Items.Add(new ListItem("Grátis", "1"));
        }

        private void MontarForuns()
        {
            rptForum.DataSource = new Admin.Forum().ListarForum();
            rptForum.DataBind();
        }

        private void MontarAreas()
        {
            rptArea.DataSource = new Admin.Area().ListarArea();
            rptArea.DataBind();
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Novo Advogado");
            else
                Response.Write("Alterar Advogado");
        }

        private void PreencherCampos()
        {
            Entity.Advogado advogado = new Admin.Advogado().ConsultarAdvogado(Request["uid"]);

            txtNome.Text = advogado.Nome;
            txtCpf.Text = advogado.Cpf;
            txtOab.Text = advogado.Oab;
            txtEndereco.Text = advogado.Endereco;
            txtBairro.Text = advogado.Bairro;
            txtCidade.Text = advogado.Cidade;
            ddlEstado.Items.FindByValue(advogado.Estado).Selected = true;
            txtCep.Text = advogado.Cep;
            txtTelefone.Text = advogado.Telefone;
            txtCelular.Text = advogado.Celular;
            txtSite.Text = advogado.Site;
            txtEmail.Text = advogado.Email;
            txtApresentacao.Text = advogado.Apresentacao;
            ddlPlano.Items.FindByValue(advogado.Plano_ID.ToString()).Selected = true;
            txtValorMinimo.Text = advogado.ValorMinimo.ToString("n2");
            rad1.Checked = (advogado.Notificacao == 1);
            rad2.Checked = (advogado.Notificacao == 2);
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Cpf = txtCpf.Text.ToPrepare();
            string Oab = txtOab.Text.ToPrepare();
            string Endereco = txtEndereco.Text.ToPrepare();
            string Bairro = txtBairro.Text.ToPrepare();
            string Cidade = txtCidade.Text.ToPrepare();
            string Estado = ddlEstado.SelectedValue;
            string Cep = txtCep.Text.ToPrepare();
            string Telefone = txtTelefone.Text.ToPrepare();
            string Celular = txtCelular.Text.ToPrepare();
            string Site = txtSite.Text.ToPrepare();
            string Email = txtEmail.Text.ToPrepare();
            string Senha = txtSenha.Text.ToPrepare();
            string Apresentacao = txtApresentacao.Text.ToPrepare();
            string Plano = ddlPlano.SelectedValue;
            string ValorMinimo = txtValorMinimo.Text.ToPrepare();
            int Notificacao = (rad1.Checked) ? 1 : (rad2.Checked) ? 2 : 0;
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
                icoNome.Visible = true;
            }
            if (!Cpf.isFilled())
            {
                Validar = false;
                lblCpf.Text = "- campo obrigatório";
                txtCpf.BackColor = Color.LightYellow;
                icoCpf.Visible = true;
            }
            if (!Oab.isFilled())
            {
                Validar = false;
                lblOab.Text = "- campo obrigatório";
                txtOab.BackColor = Color.LightYellow;
                icoOab.Visible = true;
            }
            if (!Endereco.isFilled())
            {
                Validar = false;
                lblEndereco.Text = "- campo obrigatório";
                txtEndereco.BackColor = Color.LightYellow;
                icoEndereco.Visible = true;
            }
            if (!Bairro.isFilled())
            {
                Validar = false;
                lblBairro.Text = "- campo obrigatório";
                txtBairro.BackColor = Color.LightYellow;
                icoBairro.Visible = true;
            }
            if (!Cidade.isFilled())
            {
                Validar = false;
                lblCidade.Text = "- campo obrigatório";
                txtCidade.BackColor = Color.LightYellow;
                icoCidade.Visible = true;
            }
            if (ddlEstado.SelectedIndex == 0)
            {
                Validar = false;
                lblEstado.Text = "- campo obrigatório";
                ddlEstado.BackColor = Color.LightYellow;
                icoEstado.Visible = true;
            }
            if (!Cep.isFilled())
            {
                Validar = false;
                lblCep.Text = "- campo obrigatório";
                txtCep.BackColor = Color.LightYellow;
                icoCep.Visible = true;
            }
            if (ddlPlano.SelectedIndex == 0)
            {
                Validar = false;
                lblPlano.Text = "- campo obrigatório";
                ddlPlano.BackColor = Color.LightYellow;
                icoPlano.Visible = true;
            }
            if (!ValorMinimo.isFilled())
            {
                Validar = false;
                lblValorMinimo.Text = "- campo obrigatório";
                txtValorMinimo.BackColor = Color.LightYellow;
                icoValorMinimo.Visible = true;
            }

            if (Validar)
            {
                Entity.Advogado advogado = new Entity.Advogado();
                advogado.Nome = Nome;
                advogado.Cpf = Cpf.RemoveMask();
                advogado.Oab = Oab;
                advogado.Endereco = Endereco;
                advogado.Bairro = Bairro;
                advogado.Cidade = Cidade;
                advogado.Estado = Estado;
                advogado.Cep = Cep.RemoveMask();
                advogado.Telefone = Telefone;
                advogado.Celular = Celular;
                advogado.Site = Site;
                advogado.Email = Email;
                advogado.Senha = Senha;
                advogado.Apresentacao = Apresentacao;
                advogado.Plano_ID = Convert.ToInt32(Plano);
                advogado.ValorMinimo = Convert.ToDecimal(ValorMinimo);
                advogado.Notificacao = Notificacao;

                if (Request["uid"] != null)
                {
                    advogado.IDAdvogado = Convert.ToInt32(Request["uid"]);
                    Entity.Retorno ret = new Admin.Advogado().AlterarAdvogado(advogado);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    // altera fóruns
                    new Admin.Advogado.Forum().ExcluirForum(advogado.IDAdvogado);
                    if (Request.Form["chkForum"] != null)
                    {
                        string[] Foruns = Request.Form["chkForum"].Split(',');
                        foreach (string IDForum in Foruns)
                            new Admin.Advogado.Forum().InserirForum(ret.Identity, IDForum);
                    }

                    // altera áreas de atuação
                    new Admin.Advogado.Area().ExcluirArea(advogado.IDAdvogado);
                    if (Request.Form["chkArea"] != null)
                    {
                        string[] Areas = Request.Form["chkArea"].Split(',');
                        foreach (string IDArea in Areas)
                            new Admin.Advogado.Area().InserirArea(ret.Identity, IDArea);
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                    PreencherCampos();
                }
                else
                {
                    Entity.Retorno ret = new Admin.Advogado().InserirAdvogado(advogado);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    // adiciona fóruns
                    if (Request.Form["chkForum"] != null)
                    {
                        string[] Foruns = Request.Form["chkForum"].Split(',');
                        foreach (string IDForum in Foruns)
                            new Admin.Advogado.Forum().InserirForum(ret.Identity, IDForum);
                    }

                    // adiciona áreas de atuação
                    if (Request.Form["chkArea"] != null)
                    {
                        string[] Areas = Request.Form["chkArea"].Split(',');
                        foreach (string IDArea in Areas)
                            new Admin.Advogado.Area().InserirArea(ret.Identity, IDArea);
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Cadastro realizado com sucesso.");
                    LimparCampos();
                }
            }
        }

        public string IsForum(object IDForum)
        {
            if (new Admin.Advogado.Forum().ConsultarForum(Request["uid"], IDForum))
                return "checked='checked'";
            else
                return "";
        }

        public string IsArea(object IDArea)
        {
            if (new Admin.Advogado.Area().ConsultarArea(Request["uid"], IDArea))
                return "checked='checked'";
            else
                return "";
        }
    }
}