﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class _Admin_portal_advogados_listagem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Advogado> advogados = new Admin.Advogado().ListarAdvogado();
            rptListagem.DataSource = advogados;
            rptListagem.DataBind();
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Advogados = Request.Form["chkExcluir"].Split(',');
                Entity.Retorno ret = new Admin.Advogado().ExcluirAdvogado(Advogados);
                if (!ret.Status)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                    return;
                }
            }
            MontarListagem();
        }
    }
}