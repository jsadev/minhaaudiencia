﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class _Admin_portal_foruns_registro : BaseRegistro
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MontarVaras();
                new Util.Estado(ddlEstado);
                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Novo Fórum");
            else
                Response.Write("Alterar Fórum");
        }

        private void MontarVaras()
        {
            ddlVara.Items.Clear();
            ddlVara.Items.Add("Selecione");
            new Admin.Vara().ListarVara().ForEach(delegate(Entity.Vara vara)
            {
                ddlVara.Items.Add(new ListItem(vara.Nome, vara.IDVara.ToString()));
            });
        }

        private void PreencherCampos()
        {
            Entity.Forum forum = new Admin.Forum().ConsultarForum(Request["uid"]);

            txtNome.Text = forum.Nome;
            ddlVara.Items.FindByValue(forum.Vara_ID.ToString()).Selected = true;
            txtEndereco.Text = forum.Endereco;
            txtBairro.Text = forum.Bairro;
            txtCidade.Text = forum.Cidade;
            ddlEstado.Items.FindByValue(forum.Estado).Selected = true;
            txtCep.Text = forum.Cep;
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Vara = ddlVara.SelectedValue;
            string Endereco = txtEndereco.Text.ToPrepare();
            string Bairro = txtBairro.Text.ToPrepare();
            string Cidade = txtCidade.Text.ToPrepare();
            string Estado = ddlEstado.SelectedValue;
            string Cep = txtCep.Text.ToPrepare();
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
                icoNome.Visible = true;
            }
            if (ddlVara.SelectedIndex == 0)
            {
                Validar = false;
                lblVara.Text = "- campo obrigatório";
                ddlVara.BackColor = Color.LightYellow;
                icoVara.Visible = true;
            }
            if (!Endereco.isFilled())
            {
                Validar = false;
                lblEndereco.Text = "- campo obrigatório";
                txtEndereco.BackColor = Color.LightYellow;
                icoEndereco.Visible = true;
            }
            if (!Bairro.isFilled())
            {
                Validar = false;
                lblBairro.Text = "- campo obrigatório";
                txtBairro.BackColor = Color.LightYellow;
                icoBairro.Visible = true;
            }
            if (!Cidade.isFilled())
            {
                Validar = false;
                lblCidade.Text = "- campo obrigatório";
                txtCidade.BackColor = Color.LightYellow;
                icoCidade.Visible = true;
            }
            if (ddlEstado.SelectedIndex == 0)
            {
                Validar = false;
                lblEstado.Text = "- campo obrigatório";
                ddlEstado.BackColor = Color.LightYellow;
                icoEstado.Visible = true;
            }
            if (!Cep.isFilled())
            {
                Validar = false;
                lblCep.Text = "- campo obrigatório";
                txtCep.BackColor = Color.LightYellow;
                icoCep.Visible = true;
            }

            if (Validar)
            {
                Entity.Forum forum = new Entity.Forum();
                forum.Nome = Nome;
                forum.Vara_ID = Convert.ToInt32(Vara);
                forum.Endereco = Endereco;
                forum.Bairro = Bairro;
                forum.Cidade = Cidade;
                forum.Estado = Estado;
                forum.Cep = Cep.RemoveMask();

                if (Request["uid"] != null)
                {
                    forum.IDForum = Convert.ToInt32(Request["uid"]);
                    Entity.Retorno ret = new Admin.Forum().AlterarForum(forum);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                    PreencherCampos();
                }
                else
                {
                    Entity.Retorno ret = new Admin.Forum().InserirForum(forum);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Cadastro realizado com sucesso.");
                    LimparCampos();
                }
            }
        }
    }
}