﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class _Admin_portal_foruns_listagem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Forum> foruns = new Admin.Forum().ListarForum();
            rptListagem.DataSource = foruns;
            rptListagem.DataBind();
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Foruns = Request.Form["chkExcluir"].Split(',');
                Entity.Retorno ret = new Admin.Forum().ExcluirForum(Foruns);
                if (!ret.Status)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                    return;
                }
            }
            MontarListagem();
        }
    }
}