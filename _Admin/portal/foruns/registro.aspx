﻿<%@ Page Title="Fóruns, Portal | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia._Admin_portal_foruns_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>
        Portal
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li>
        <a href="listagem.aspx">Fóruns</a>
        <span class="divider">
            <i class="icon-angle-right"></i>
        </span>
    </li>
    <li class="active"><% Migalha(); %></li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <h3 class="header smaller"><small>Info</small></h3>

    <div class="row-fluid form-horizontal">
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Nome *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtNome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
				    <i id="icoNome" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblNome" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Vara *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlVara" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoVara" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblVara" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Endereço *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtEndereco" runat="server" Width="300px" MaxLength="200"></asp:TextBox>
				    <i id="icoEndereco" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblEndereco" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Bairro *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtBairro" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
				    <i id="icoBairro" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblBairro" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Cidade *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCidade" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
				    <i id="icoCidade" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCidade" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">Estado *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:DropDownList ID="ddlEstado" runat="server" style="width: auto"></asp:DropDownList>
				    <i id="icoEstado" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblEstado" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
        <div class="control-group">
		    <label class="control-label" for="form-field-1">CEP *</label>
		    <div class="controls">
                <span class="input-icon input-icon-right">
				    <asp:TextBox ID="txtCep" runat="server" CssClass="_cep" Width="100px" MaxLength="8"></asp:TextBox>
				    <i id="icoCep" runat="server" class="icon-remove-circle red" visible="false"></i>
			    </span>
                <asp:Label ID="lblCep" runat="server" ForeColor="Red"></asp:Label>
		    </div>
	    </div>
    </div>

    <div class="form-actions">
		<button id="btnSalvar" runat="server" class="btn btn-success" onserverclick="btnSalvar_ServerClick">
			<i class="icon-ok bigger-110"></i>
			salvar
		</button>
		&nbsp; &nbsp;
		<button class="btn" onclick="window.location='listagem.aspx'; return false;">
			<i class="icon-undo bigger-110"></i>
			cancelar
		</button>
	</div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../../assets/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        jQuery(function () {
            $('._cep').mask("99999-999");
        });
    </script>
</asp:Content>