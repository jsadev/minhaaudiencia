﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Web.Script.Serialization;

    public partial class _Admin_api_advogado_advogado_consultar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["advogado_id"] == null)
            {
                Error error = new Error();

                error.code = "1006";
                error.error = true;
                error.message = "Parâmetro advogado_id está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Advogado advogado = new Advogado();
            advogado.allow_delete = !(new Admin.Audiencia.Convite().ContemConvite(Request["advogado_id"]) || new Admin.Audiencia().ContemAudiencia(Request["advogado_id"]));
            advogado.convite = new Admin.Audiencia.Convite().ContemConvite(Request["advogado_id"]);
            advogado.audiencia = new Admin.Audiencia().ContemAudiencia(Request["advogado_id"]);
            Response.Write(new JavaScriptSerializer().Serialize(advogado));
        }

        public class Advogado : Error
        {
            public bool allow_delete { get; set; }
            public bool convite { get; set; }
            public bool audiencia { get; set; }
        }

        public class Error
        {
            public string code { get; set; }
            public bool error { get; set; }
            public string message { get; set; }
        }
    }
}