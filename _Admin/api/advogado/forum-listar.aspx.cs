﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Web.Script.Serialization;

    public partial class _Admin_api_advogado_forum_listar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Forum.ForumHolder holder = new Forum.ForumHolder();

            if (Request["cidade"] == null)
            {
                holder.code = "1006";
                holder.error = true;
                holder.message = "Parâmetro cidade está faltando.";
                Response.Write(new JavaScriptSerializer().Serialize(holder));
                return;
            }

            holder.foruns = new List<Forum>();
            new Admin.Forum().ListarForum(Request["cidade"]).ForEach(delegate (Entity.Forum forum)
            {
                holder.foruns.Add(new Forum() { nome = forum.Nome, id = forum.IDForum });
            });

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Forum
        {
            public string nome { get; set; }
            public int id { get; set; }

            public class ForumHolder : Error
            {
                public List<Forum> foruns { get; set; }
            }
        }

        public class Error
        {
            public string code { get; set; }
            public bool error { get; set; }
            public string message { get; set; }
        }
    }
}