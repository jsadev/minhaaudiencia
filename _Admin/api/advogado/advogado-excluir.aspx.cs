﻿namespace minhaaudiencia
{
    using System;
    using System.Web.Script.Serialization;

    public partial class _Admin_api_advogado_advogado_excluir : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Error error = new Error();

            if (Request["advogado_id"] == null)
            {
                error.code = "13";
                error.error = true;
                error.message = "Parâmetro advogado_id está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Entity.Retorno ret = new Admin.Audiencia.Convite().ExcluirConvite(Request["advogado_id"]);
            if (!ret.Status)
            {
                error.code = "7";
                error.error = true;
                error.message = ret.Erro;

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            ret = new Admin.Audiencia().ExcluirAudienciaAdvogado(Request["advogado_id"]);
            if (!ret.Status)
            {
                error.code = "7";
                error.error = true;
                error.message = ret.Erro;

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            ret = new Admin.Advogado().ExcluirAdvogado(Request["advogado_id"]);
            if (!ret.Status)
            {
                error.code = "7";
                error.error = true;
                error.message = ret.Erro;

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Response.Write(new JavaScriptSerializer().Serialize(error));
        }

        public class Error
        {
            public string code { get; set; }
            public bool error { get; set; }
            public string message { get; set; }
        }
    }
}