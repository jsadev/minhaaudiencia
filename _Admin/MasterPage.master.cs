﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_MasterPage : System.Web.UI.MasterPage
    {
        public Entity.Usuario usuario = new Entity.Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[Constante.Sessions.ADM_LOGIN] != null)
                usuario = (Entity.Usuario)Session[Constante.Sessions.ADM_LOGIN];
        }

        public void setActive(string Url)
        {
            string uri = Request.Url.AbsolutePath;
            if (uri.Contains(Url))
                Response.Write("class='active'");
        }

        protected void lnkSair_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/_Admin/login.aspx");
        }
}
}