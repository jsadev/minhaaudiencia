﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_foruns_listagem : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Forum> foruns = new Admin.Forum().ListarForum();
            rptListagem.DataSource = foruns;
            rptListagem.DataBind();
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Foruns = Request.Form["chkExcluir"].Split(',');
                foreach (string IDForum in Foruns)
                {
                    Entity.Retorno ret = new Admin.Forum().ExcluirForum(IDForum);
                    if (!ret.Status)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                        return;
                    }
                }
            }
            MontarListagem();
        }

        protected void lnkExcluir_Command(object sender, CommandEventArgs e)
        {
            Entity.Retorno ret = new Admin.Forum().ExcluirForum(e.CommandArgument);
            if (!ret.Status)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                return;
            }
            MontarListagem();
        }
    }
}