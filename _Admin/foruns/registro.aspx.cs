﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class admin_foruns_registro : BaseRegistro
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MontarAreas();
                MontarCidades();
                new Util.Estado(ddlEstado);

                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        private void MontarAreas()
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add(new ListItem("Selecione a Área de Atuação", "0"));
            new Admin.Area().ListarArea().ForEach(delegate (Entity.Area area)
            {
                ddlArea.Items.Add(new ListItem(area.Nome, area.IDArea.ToString()));
            });
        }

        private void MontarCidades()
        {
            ddlCidade.Items.Clear();
            ddlCidade.Items.Add(new ListItem("Selecione a Jurisdição", "0"));
            new Admin.Cidade().ListarCidade().ForEach(delegate (Entity.Cidade cidade)
            {
                ddlCidade.Items.Add(new ListItem(cidade.Nome, cidade.IDCidade.ToString()));
            });
        }

        private void PreencherCampos()
        {
            Entity.Forum forum = new Admin.Forum().ConsultarForum(Request["uid"]);

            txtNome.Text = forum.Nome;
            txtEndereco.Text = forum.Endereco;
            txtBairro.Text = forum.Bairro;
            txtCidade.Text = forum.Cidade;
            ddlEstado.Items.FindByValue(forum.Estado).Selected = true;
            txtCep.Text = forum.Cep;

            // áreas
            rptArea.DataSource = new Admin.Forum.Area().ListarArea(forum.IDForum);
            rptArea.DataBind();

            // jurisdições
            rptCidade.DataSource = new Admin.Forum.Jurisdicao().ListarJurisdicao(forum.IDForum);
            rptCidade.DataBind();
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Endereco = txtEndereco.Text.ToPrepare();
            string Bairro = txtBairro.Text.ToPrepare();
            string Cidade = txtCidade.Text.ToPrepare();
            string Estado = ddlEstado.SelectedValue;
            string Cep = txtCep.Text.ToPrepare();
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
            }
            if (!Endereco.isFilled())
            {
                Validar = false;
                lblEndereco.Text = "- campo obrigatório";
                txtEndereco.BackColor = Color.LightYellow;
            }
            if (!Bairro.isFilled())
            {
                Validar = false;
                lblBairro.Text = "- campo obrigatório";
                txtBairro.BackColor = Color.LightYellow;
            }
            if (!Cidade.isFilled())
            {
                Validar = false;
                lblCidade.Text = "- campo obrigatório";
                txtCidade.BackColor = Color.LightYellow;
            }
            if (ddlEstado.SelectedIndex == 0)
            {
                Validar = false;
                lblEstado.Text = "- campo obrigatório";
                ddlEstado.BackColor = Color.LightYellow;
            }
            if (!Cep.isFilled())
            {
                Validar = false;
                lblCep.Text = "- campo obrigatório";
                txtCep.BackColor = Color.LightYellow;
            }

            if (Validar)
            {
                Entity.Forum forum = new Entity.Forum();
                forum.Nome = Nome;
                forum.Endereco = Endereco;
                forum.Bairro = Bairro;
                forum.Cidade = Cidade;
                forum.Estado = Estado;
                forum.Cep = Cep.RemoveMask();

                if (Request["uid"] != null)
                {
                    forum.IDForum = Convert.ToInt32(Request["uid"]);
                    Entity.Retorno ret = new Admin.Forum().AlterarForum(forum);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    // adiciona áreas
                    new Admin.Forum.Area().ExcluirArea(forum.IDForum);
                    if (Request.Form["chkArea"] != null)
                    {
                        string[] Areas = Request.Form["chkArea"].Split(',');
                        foreach (string IDArea in Areas)
                            new Admin.Forum.Area().InserirArea(forum.IDForum, IDArea);
                    }

                    // adiciona jurisdições
                    new Admin.Forum.Jurisdicao().ExcluirJurisdicao(forum.IDForum);
                    if (Request.Form["chkCidade"] != null)
                    {
                        string[] Jurisdicoes = Request.Form["chkCidade"].Split(',');
                        foreach (string IDJurisdicao in Jurisdicoes)
                            new Admin.Forum.Jurisdicao().InserirJurisdicao(forum.IDForum, IDJurisdicao);
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                    PreencherCampos();
                }
                else
                {
                    Entity.Retorno ret = new Admin.Forum().InserirForum(forum);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    // adiciona áreas
                    if (Request.Form["chkArea"] != null)
                    {
                        string[] Areas = Request.Form["chkArea"].Split(',');
                        foreach (string IDArea in Areas)
                            new Admin.Forum.Area().InserirArea(ret.Identity, IDArea);
                    }

                    // adiciona jurisdições
                    if (Request.Form["chkCidade"] != null)
                    {
                        string[] Jurisdicoes = Request.Form["chkCidade"].Split(',');
                        foreach (string IDJurisdicao in Jurisdicoes)
                            new Admin.Forum.Jurisdicao().InserirJurisdicao(ret.Identity, IDJurisdicao);
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Cadastro realizado com sucesso.");
                    LimparCampos();
                }
            }
        }
    }
}