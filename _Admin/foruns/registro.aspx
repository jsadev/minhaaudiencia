﻿<%@ Page Title="Fóruns | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia.admin_foruns_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li><a href="listagem.aspx">Fóruns</a></li>
    <li class="active">Cadastro de Fórum</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Fórum</h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Nome do Fórum</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNome" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblNome" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Endereço (Rua, Número e Complemento)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtEndereco" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblEndereco" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Bairro</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtBairro" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="lblBairro" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Cidade</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCidade" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="lblCidade" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Estado</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <asp:Label ID="lblEstado" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-4">CEP</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCep" runat="server" CssClass="input-medium input-mask-cep form-control" placeholder="99999-999"></asp:TextBox>
                        <asp:Label ID="lblCep" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Jurisdição</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlCidade" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <i id="btnCidade" class="ace-icon fa fa-plus-square bigger-240 blue"></i>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div id="cidades" class="col-sm-9">
                        <asp:Repeater ID="rptCidade" runat="server">
                            <ItemTemplate>
                                <input name="chkCidade" type="checkbox" class="ace" value="<%# Eval("IDCidade") %>" checked="checked" />
                                <label class="lbl"> <%# Eval("Nome") %></label>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Selecione os Fóruns nos quais está disposto a fazer Audiências</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlArea" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <i id="btnArea" class="ace-icon fa fa-plus-square bigger-240 blue"></i>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div id="areas" class="col-sm-9">
                        <asp:Repeater ID="rptArea" runat="server">
                            <ItemTemplate>
                                <input id="chkArea<%# Eval("IDArea") %>" name="chkArea" type="checkbox" class="ace" value="<%# Eval("IDArea") %>" checked="checked" />
                                <label for="chkArea<%# Eval("IDArea") %>" class="lbl"> <%# Eval("Nome") %></label>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-times"></i> Cancelar</button>
                    <button type="submit" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i> Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../assets/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript">
        jQuery(function () {
            $('.input-mask-cep').mask("99999-999");

            $('#btnCidade').click(function () {
                var cidade_id = $('#conteudo_ddlCidade').val();
                var text = $('#conteudo_ddlCidade option:selected').text();

                if (cidade_id == 0) {
                    alert('Selecione a Cidade antes de adicionar.');
                    return;
                }

                var cidades = $('#cidades');

                if (cidades.html().indexOf('chkCidade' + cidade_id) == -1) {
                    var html = '<input name="chkCidade" type="checkbox" class="ace" value="' + cidade_id + '" checked="checked" />\n';
                    html += '<label class="lbl"> ' + text + '</label>\n';

                    cidades.append(html);
                }
            });
            
            $('#btnArea').click(function () {
                var area_id = $('#conteudo_ddlArea').val();
                var text = $('#conteudo_ddlArea option:selected').text();

                if (area_id == 0) {
                    alert('Selecione a Área antes de adicionar.');
                    return;
                }

                var areas = $('#areas');

                if (areas.html().indexOf('chkArea' + area_id) == -1) {
                    var html = '<input id="chkArea' + area_id + '" name="chkArea" type="checkbox" class="ace" value="' + area_id + '" checked="checked" />\n';
                    html += '<label for="chkArea' + area_id + '" class="lbl"> ' + text + '</label>\n';

                    areas.append(html);
                }
            });
        });
    </script>
</asp:Content>