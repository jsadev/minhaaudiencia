﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class admin_audiencias_registro : BaseRegistro
    {
        private DateTime minDate = DateTime.MinValue;
        private decimal minDecimal = decimal.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MontarAreas();
                MontarForuns();
                MontarVaras();
                MontarCidades();
                MontarAdvogados();

                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        private void MontarAreas()
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add("Área de Atuação");
            new Admin.Area().ListarArea().ForEach(delegate(Entity.Area area)
            {
                ddlArea.Items.Add(new ListItem(area.Nome, area.IDArea.ToString()));
            });
        }

        private void MontarForuns()
        {
            ddlForum.Items.Clear();
            ddlForum.Items.Add("Fórum");
            new Admin.Forum().ListarForum().ForEach(delegate(Entity.Forum forum)
            {
                ddlForum.Items.Add(new ListItem(forum.Nome, forum.IDForum.ToString()));
            });
        }

        private void MontarVaras()
        {
            ddlVara.Items.Clear();
            ddlVara.Items.Add("Vara");
            new Admin.Vara().ListarVara().ForEach(delegate(Entity.Vara vara)
            {
                ddlVara.Items.Add(new ListItem(vara.Nome, vara.IDVara.ToString()));
            });
        }

        private void MontarCidades()
        {
            ddlCidade.Items.Clear();
            ddlCidade.Items.Add("Vara");
            new Admin.Cidade().ListarCidade().ForEach(delegate (Entity.Cidade cidade)
            {
                ddlCidade.Items.Add(new ListItem(cidade.Nome, cidade.IDCidade.ToString()));
            });
        }

        private void MontarAdvogados()
        {
            
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Nova Audiência");
            else
                Response.Write("Alterar Audiência");
        }

        private void PreencherCampos()
        {
            Entity.Audiencia audiencia = new Admin.Audiencia().ConsultarAudiencia(Request["uid"]);

            txtMateria.Text = audiencia.Materia;
            ddlArea.Items.FindByValue(audiencia.Area_ID.ToString()).Selected = true;
            ddlCidade.Items.FindByValue(audiencia.Cidade_ID.ToString()).Selected = true;
            ddlForum.Items.FindByValue(audiencia.Forum_ID.ToString()).Selected = true;
            ddlVara.Items.FindByValue(audiencia.Vara_ID.ToString()).Selected = true;
            txtData.Text = audiencia.Data.ToString("dd/MM/yyyy HH:mm");
            txtParteRepresenta.Text = audiencia.ParteRepresenta;
            txtParteAudiencia.Text = audiencia.ParteAudiencia;
            txtValor.Text = audiencia.Valor.ToString("n2");
            txtDescricao.Text = audiencia.Descricao;
        }
    }
}