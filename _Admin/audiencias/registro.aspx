﻿<%@ Page Title="Audiências | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia.admin_audiencias_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li><a href="listagem.aspx">Audiências</a></li>
    <li class="active">Cadastro de Audiência</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Audiência</h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Matéria da Audiência</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtMateria" runat="server" CssClass="col-xs-10 col-sm-5" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblMateria" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Área de Atuação</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlArea" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" Enabled="false"></asp:DropDownList>
                        <asp:Label ID="lblArea" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Cidade</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlCidade" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" Enabled="false"></asp:DropDownList>
                        <asp:Label ID="lblCidade" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Fórum</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlForum" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" Enabled="false"></asp:DropDownList>
                        <asp:Label ID="lblForum" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Vara</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlVara" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" Enabled="false"></asp:DropDownList>
                        <asp:Label ID="lblVara" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="date-timepicker1">Data e Horário</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-clock-o bigger-110"></i></span>
                            <asp:TextBox ID="txtData" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-datetime" ReadOnly="true"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblData" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Parte que Representa</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtParteRepresenta" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="R$ 0,00" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblParteRepresenta" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Partes da Audiência</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtParteAudiencia" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="R$ 0,00" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblParteAudiencia" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Valor a Pagar</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtValor" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="R$ 0,00" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblValor" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-11">Descrição (opcional)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtDescricao" runat="server" CssClass="autosize-transition form-control col-xs-10 col-sm-5" rows="7" TextMode="MultiLine" placeholder="Informação que entende relevante para o Advogado" ReadOnly="true"></asp:TextBox>
                        <asp:Label ID="lblDescricao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-undo"></i> Voltar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../assets/js/jquery.maskedinput.min.js"></script>
    <script src="../assets/js/jquery.maskMoney.min.js"></script>

    <script type="text/javascript">
        jQuery(function () {
            $('.input-mask-datetime').mask("99/99/9999 99:99");
            $('.input-mask-money').maskMoney({ thousands: '.', decimal: ',' });
        });
    </script>
</asp:Content>