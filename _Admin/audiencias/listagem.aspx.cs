﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_audiencias_listagem : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MontarAreas();
                MontarForuns();
                MontarStatus();
            }

            MontarListagem();
        }

        private void MontarAreas()
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add("Área de Atuação");
            new Admin.Area().ListarArea().ForEach(delegate (Entity.Area area)
            {
                ddlArea.Items.Add(new ListItem(area.Nome, area.IDArea.ToString()));
            });
        }

        private void MontarForuns()
        {
            ddlForum.Items.Clear();
            ddlForum.Items.Add("Fórum");
            new Admin.Forum().ListarForum().ForEach(delegate (Entity.Forum forum)
            {
                ddlForum.Items.Add(new ListItem(forum.Nome, forum.IDForum.ToString()));
            });
        }

        private void MontarStatus()
        {
            ddlStatus.Items.Clear();
            ddlStatus.Items.Add("Status");

            ddlStatus.Items.Add(new ListItem("Pendente", "0"));
            ddlStatus.Items.Add(new ListItem("Disponível", "1"));
            ddlStatus.Items.Add(new ListItem("Indisponível", "2"));
            ddlStatus.Items.Add(new ListItem("Contratado", "3"));
            ddlStatus.Items.Add(new ListItem("Aceito", "4"));
            ddlStatus.Items.Add(new ListItem("Desistência", "5"));
            ddlStatus.Items.Add(new ListItem("Cancelado", "6"));
        }

        private void MontarListagem()
        {
            List<Entity.Audiencia> audiencias = new Admin.Audiencia().ListarAudiencia();

            if (txtDatai.Text.isFilled())
                audiencias = audiencias.FindAll(delegate (Entity.Audiencia audiencia) { return audiencia.Data >= Convert.ToDateTime(txtDatai.Text); });
            if (txtDataf.Text.isFilled())
                audiencias = audiencias.FindAll(delegate (Entity.Audiencia audiencia) { return audiencia.Data <= Convert.ToDateTime(txtDataf.Text); });
            if (ddlArea.SelectedIndex > 0)
                audiencias = audiencias.FindAll(delegate (Entity.Audiencia audiencia) { return audiencia.Area_ID.ToString() == ddlArea.SelectedValue; });
            if (ddlForum.SelectedIndex > 0)
                audiencias = audiencias.FindAll(delegate (Entity.Audiencia audiencia) { return audiencia.Forum_ID.ToString() == ddlForum.SelectedValue; });
            if (ddlStatus.SelectedIndex>0)
                audiencias = audiencias.FindAll(delegate (Entity.Audiencia audiencia) { return audiencia.Status.ToString() == ddlStatus.SelectedValue; });

            rptListagem.DataSource = audiencias;
            rptListagem.DataBind();
        }

        public string Status(object status)
        {
            switch (Convert.ToInt32(status))
            {
                case -1: return "";
                case 0: return "Pendente";
                case 1: return "Disponível";
                case 2: return "Indisponível";
                case 3: return "Contratado";
                case 4: return "Aceito";
                case 5: return "Desistência";
                case 6: return "Cancelado";
            }
            return "";
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Audiencias = Request.Form["chkExcluir"].Split(',');
                foreach (string IDAudiencia in Audiencias)
                {
                    Entity.Retorno ret = new Admin.Audiencia().ExcluirAudiencia(IDAudiencia);
                    if (!ret.Status)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                        return;
                    }
                }
            }
            MontarListagem();
        }

        protected void lnkExcluir_Command(object sender, CommandEventArgs e)
        {
            Entity.Retorno ret = new Admin.Audiencia().ExcluirAudiencia(e.CommandArgument);
            if (!ret.Status)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                return;
            }
            MontarListagem();
        }
    }
}