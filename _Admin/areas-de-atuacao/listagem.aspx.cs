﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class admin_areas_de_atuacao_listagem : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Area> areas = new Admin.Area().ListarArea();
            rptListagem.DataSource = areas;
            rptListagem.DataBind();
        }

        protected void btnExcluir_ServerClick(object sender, EventArgs e)
        {
            if (Request.Form["chkExcluir"] != null)
            {
                string[] Areas = Request.Form["chkExcluir"].Split(',');
                foreach (string IDArea in Areas)
                {
                    Entity.Retorno ret = new Admin.Area().ExcluirArea(IDArea);
                    if (!ret.Status)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                        return;
                    }
                }
            }
            MontarListagem();
        }

        protected void lnkExcluir_Command(object sender, CommandEventArgs e)
        {
            Entity.Retorno ret = new Admin.Area().ExcluirArea(e.CommandArgument);
            if (!ret.Status)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert-error", "alert('" + ret.Erro + "');", true);
                return;
            }
            MontarListagem();
        }
    }
}