﻿<%@ Page Title="Varas | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia.admin_varas_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li><a href="listagem.aspx">Varas</a></li>
    <li class="active">Cadastro de Varas</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Varas</h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nome da Vara</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNome" runat="server" CssClass="col-xs-10 col-sm-5" placeholder="insira o nome da vara"></asp:TextBox>
                        <asp:Label ID="lblNome" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Fórum</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlForum" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <i id="btnForum" class="ace-icon fa fa-plus-square bigger-240 blue"></i>
                        <asp:Label ID="lblForum" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div id="foruns" class="col-sm-9">
                        <asp:Repeater ID="rptForum" runat="server">
                            <ItemTemplate>
                                <input id="chkForum<%# Eval("IDForum") %>" name="chkForum" type="checkbox" class="ace" value="<%# Eval("IDForum") %>" checked="checked" />
                                <label for="chkForum<%# Eval("IDForum") %>" class="lbl"> <%# Eval("Nome") %></label>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-times"></i> Cancelar</button>
                    <button type="submit" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i> Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../assets/js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript">
        jQuery(function () {
            $('#btnForum').click(function () {
                var forum_id = $('#conteudo_ddlForum').val();
                var text = $('#conteudo_ddlForum option:selected').text();

                if (forum_id == 0) {
                    alert('Selecione o Fórum antes de adicionar.');
                    return;
                }

                var foruns = $('#foruns');

                if (foruns.html().indexOf('chkForum' + forum_id) == -1) {
                    var html = '<input id="chkForum' + forum_id + '" name="chkForum" type="checkbox" class="ace" value="' + forum_id + '" checked="checked" />\n';
                    html += '<label for="chkForum' + forum_id + '" class="lbl"> ' + text + '</label>\n';

                    foruns.append(html);
                }
            });
        });
    </script>
</asp:Content>