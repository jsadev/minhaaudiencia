﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class _Admin_meus_dados_registro : BaseRegistro
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                PreencherCampos();
        }

        private void PreencherCampos()
        {
            Entity.Usuario usuario = new Admin.Usuario().ConsultarUsuario(((Entity.Usuario)Session[Constante.Sessions.ADM_LOGIN]).IDUsuario);

            txtNome.Text = usuario.Nome;
            txtEmail.Text = usuario.Email;
            chkAtivo.Checked = usuario.Ativo;
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Email = txtEmail.Text.ToPrepare();
            string Senha = txtSenha.Text.ToPrepare();
            string ConfirmarSenha = txtConfirmarSenha.Text.ToPrepare();
            bool Ativo = chkAtivo.Checked;
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
            }
            if (!Email.isFilled())
            {
                Validar = false;
                lblEmail.Text = "- campo obrigatório";
                txtEmail.BackColor = Color.LightYellow;
            }
            if (Senha.isFilled() && Senha != ConfirmarSenha)
            {
                Validar = false;
                lblConfirmarSenha.Text = "- senha não confere";
                txtConfirmarSenha.BackColor = Color.LightYellow;
            }

            if (Validar)
            {
                Entity.Usuario usuario = new Entity.Usuario();
                usuario.Nome = Nome;
                usuario.Email = Email;
                usuario.Senha = Senha;
                usuario.Ativo = Ativo;

                usuario.IDUsuario = ((Entity.Usuario)Session[Constante.Sessions.ADM_LOGIN]).IDUsuario;
                Entity.Retorno ret = new Admin.Usuario().AlterarUsuario(usuario);
                if (!ret.Status)
                {
                    Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                    return;
                }

                if (Senha.isFilled())
                {
                    ret = new Admin.Usuario().AlterarSenha(usuario);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }
                }

                Session[Constante.Sessions.ADM_LOGIN] = new Admin.Usuario().ConsultarUsuario(usuario.IDUsuario);
                Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                PreencherCampos();
            }
        }
    }
}