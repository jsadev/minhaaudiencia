﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class _Admin_jurisdicoes_registro : BaseRegistro
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                new Util.Estado(ddlEstado);
                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Nova Jurisdição");
            else
                Response.Write("Alterar Jurisdição");
        }

        private void PreencherCampos()
        {
            Entity.Cidade cidade = new Admin.Cidade().ConsultarCidade(Request["uid"]);

            txtNome.Text = cidade.Nome;
            ddlEstado.Items.FindByValue(cidade.Estado).Selected = true;
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Estado = ddlEstado.SelectedValue;
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
            }
            if (ddlEstado.SelectedIndex == 0)
            {
                Validar = false;
                lblEstado.Text = "- selecione o estado";
                ddlEstado.BackColor = Color.LightYellow;
            }

            if (Validar)
            {
                Entity.Cidade cidade = new Entity.Cidade();
                cidade.Nome = Nome;
                cidade.Estado = Estado;

                if (Request["uid"] != null)
                {
                    cidade.IDCidade = Convert.ToInt32(Request["uid"]);
                    Entity.Retorno ret = new Admin.Cidade().AlterarCidade(cidade);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                    PreencherCampos();
                }
                else
                {
                    Entity.Retorno ret = new Admin.Cidade().InserirCidade(cidade);
                    if (!ret.Status)
                    {
                        Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                        return;
                    }

                    Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Cadastro realizado com sucesso.");
                    LimparCampos();
                }
            }
        }
    }
}