﻿<%@ Page Title="Jurisdições | MINHA AUDIÊNCIA" Language="C#" MasterPageFile="~/_Admin/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia._Admin_jurisdicoes_registro" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li><a href="listagem.aspx">Jurisdições</a></li>
    <li class="active">Cadastro de Jurisdição</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Jurisdição</h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="conteudo_txtNome">Cidade *</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNome" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50" placeholder="insira o nome da jurisdição"></asp:TextBox>
                        <asp:Label ID="lblNome" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="conteudo_txtNome">Estado *</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlEstado" runat="server"></asp:DropDownList>
                        <asp:Label ID="lblEstado" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-times"></i> Cancelar</button>
                    <button type="submit" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i> Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>