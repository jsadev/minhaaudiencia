﻿using System;

namespace minhaaudiencia.Entity
{
    public class Plano
    {
        public int IDPlano { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}