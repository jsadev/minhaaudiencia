﻿using System;

namespace minhaaudiencia.Entity
{
    public class Cidade
    {
        public int IDCidade { get; set; }
        public string Nome { get; set; }
        public string Estado { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}