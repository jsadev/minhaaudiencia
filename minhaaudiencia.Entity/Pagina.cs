﻿using System;

namespace minhaaudiencia.Entity
{
    /// <summary>
    /// admin.Pagina
    /// </summary>
    public class Pagina
    {
        public int IDPagina { get; set; }
        public string Nome { get; set; }
        public string Url { get; set; }
        public string Icone { get; set; }
        public int parentId { get; set; }
        public int Ordem { get; set; }
        public int Tipo { get; set; }
        public int Nivel { get; set; }
    }
}