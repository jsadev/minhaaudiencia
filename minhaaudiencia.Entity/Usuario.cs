﻿using System;

namespace minhaaudiencia.Entity
{
    /// <summary>
    /// admin.Usuario
    /// </summary>
    public class Usuario
    {
        public int IDUsuario { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public bool Locked { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}