﻿using System;

namespace minhaaudiencia.Entity
{
    public class Forum
    {
        public int IDForum { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Cep { get; set; }
        public DateTime DataInclusao { get; set; }

        public class Area : Entity.Area
        {
        }

        public class Jurisdicao : Entity.Cidade
        {
            public int Forum_ID { get; set; }
            public int Cidade_ID { get; set; }
        }
    }
}