﻿using System;
using System.Collections.Generic;

namespace minhaaudiencia.Entity
{
    public class Audiencia
    {
        public int IDAudiencia { get; set; }
        public int Advogado_ID { get; set; }
        public string Processo { get; set; }
        public string Materia { get; set; }
        public int Area_ID { get; set; }
        public int Cidade_ID { get; set; }
        public int Forum_ID { get; set; }
        public int Vara_ID { get; set; }
        public DateTime Data { get; set; }
        public string ParteRepresenta { get; set; }
        public string ParteAudiencia { get; set; }
        public decimal Valor { get; set; }
        public string Descricao { get; set; }
        public bool Curti { get; set; }
        public bool Indico { get; set; }
        public string Cancelamento { get; set; }
        public DateTime DataInclusao { get; set; }

        public string ForumNome { get; set; }
        public string AreaNome { get; set; }
        public string AdvogadoNome { get; set; }
        public string Telefone { get; set; }

        public int Status { get; set; }
        public int Curtidas { get; set; }
        public int Indicacoes { get; set; }

        public List<Convite> Convites { get; set; }

        public class Convite : Entity.Advogado
        {
            public int IDConvite { get; set; }
            public int Audiencia_ID { get; set; }
            public int Advogado_ID { get; set; }
            public int Status_ID { get; set; }
            public string Motivo { get; set; }
            public bool Curti { get; set; }
            public bool Indico { get; set; }
        }
    }
}