﻿using System;
using System.Collections.Generic;

namespace minhaaudiencia.Entity
{
    public class Advogado
    {
        public int IDAdvogado { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public string Cpf { get; set; }
        public string Oab { get; set; }
        public string Seccional { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Cep { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Site { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Apresentacao { get; set; }
        public int Plano_ID { get; set; }
        public string CidadeAtuacao { get; set; }
        public decimal ValorMinimo { get; set; }
        public int Notificacao { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataInclusao { get; set; }

        public int Curtidas { get; set; }
        public int Indicacoes { get; set; }

        public List<Forum> foruns { get; set; }
        public List<Area> areas { get; set; }

        public class Forum : Entity.Forum
        {
        }

        public class Area : Entity.Area
        {
        }
    }
}