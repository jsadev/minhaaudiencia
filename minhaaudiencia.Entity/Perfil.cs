﻿using System;

namespace minhaaudiencia.Entity
{
    /// <summary>
    /// admin.Perfil
    /// </summary>
    public class Perfil
    {
        public int IDPerfil { get; set; }
        public string Nome { get; set; }
        public bool Locked { get; set; }
        public DateTime DataInclusao { get; set; }

        public int Total { get; set; }
    }
}