﻿using System;

namespace minhaaudiencia.Entity
{
    public class Area
    {
        public int IDArea { get; set; }
        public string Nome { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}