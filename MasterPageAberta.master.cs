﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class MasterPageAberta : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Ativo(string pagina)
        {
            string Url = Request.Url.AbsolutePath;
            if (Url.Contains(pagina))
                Response.Write("class='active'");
        }
    }
}