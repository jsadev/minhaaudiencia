﻿namespace minhaaudiencia
{
    using System;
    using System.Web.Script.Serialization;

    public partial class api_advogado_forum_inserir : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Error error = new Error();

            if (Request["advogado_id"] == null)
            {
                error.code = "13";
                error.error = true;
                error.message = "Parâmetro advogado_id está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["foruns"] == null)
            {
                error.code = "13";
                error.error = true;
                error.message = "Parâmetro foruns está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            string[] foruns = Request["foruns"].Split(',');

            foreach (string IDForum in foruns)
            {
                Entity.Retorno ret = new Admin.Advogado.Forum().InserirForum(Request["advogado_id"], IDForum);
                if (!ret.Status)
                {
                    error.code = "7";
                    error.error = true;
                    error.message = ret.Erro;

                    Response.Write(new JavaScriptSerializer().Serialize(error));
                    return;
                }
            }

            Response.Write(new JavaScriptSerializer().Serialize(error));
        }

        public class Error
        {
            public string code { get; set; }
            public bool error { get; set; }
            public string message { get; set; }
        }
    }
}