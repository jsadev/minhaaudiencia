﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.Script.Serialization;

    public partial class api_cadastro_validacao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Error error = new Error();

            if (Request["campo"] == null)
            {
                error.code = "13";
                error.error = true;
                error.message = "Parâmetro campo está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }
            if (Request["valor"] == null)
            {
                error.code = "13";
                error.error = true;
                error.message = "Parâmetro valor está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            if (Request["campo"] == "email" && new Business.Login().ConsultarEmail(Request["valor"]))
            {
                error.code = "7";
                error.error = true;
                error.message = "Este e-mail já está sendo utilizado.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if ((Request["campo"] == "cpf" || Request["campo"] == "cnpj") && new Business.Login().ConsultarCpf(Request["valor"].Replace(".", "").Replace("-", "").Replace("/", "")))
            {
                error.code = "7";
                error.error = true;
                error.message = "Este " + (Request["campo"] == "cpf" ? "CPF" : "CNPJ") + " já está sendo utilizado.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["campo"] == "cpf" && !new Util.Util().ValidarCpf(Request["valor"]))
            {
                error.code = "7";
                error.error = true;
                error.message = "CPF inválido.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["campo"] == "cnpj" && !new Util.Util().ValidarCnpj(Request["valor"]))
            {
                error.code = "7";
                error.error = true;
                error.message = "CNPJ inválido.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Response.Write(new JavaScriptSerializer().Serialize(error));
        }

        public class Error
        {
            public string code { get; set; }
            public bool error { get; set; }
            public string message { get; set; }
        }
    }
}