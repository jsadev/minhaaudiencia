﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    public partial class api_convites : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Error error = new Error();
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            List<Entity.Audiencia> audiencias = new List<Entity.Audiencia>();
            DateTime firstDay;
            DateTime lastDay;

            switch (Request["filtro"])
            {
                case "hoje":
                    audiencias = new Business.Audiencia().ListarAudienciaConvite(advogado.IDAdvogado, DateTime.Now, DateTime.Now);
                    break;
                case "amanha":
                    audiencias = new Business.Audiencia().ListarAudienciaConvite(advogado.IDAdvogado, DateTime.Now.AddDays(1), DateTime.Now.AddDays(1));
                    break;
                case "ontem":
                    audiencias = new Business.Audiencia().ListarAudienciaConvite(advogado.IDAdvogado, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
                    break;
                case "semana":
                    firstDay = DateTime.Now.AddDays((int)DateTime.Now.DayOfWeek * -1);
                    lastDay = firstDay.AddDays(6);
                    audiencias = new Business.Audiencia().ListarAudienciaConvite(advogado.IDAdvogado, firstDay, lastDay);
                    break;
                case "ano":
                    firstDay = new DateTime(Convert.ToInt32(Request["ano"]), Convert.ToInt32(Request["mes"]), 1);
                    lastDay = firstDay.AddMonths(1).AddDays(-1);
                    audiencias = new Business.Audiencia().ListarAudienciaAgenda(advogado.IDAdvogado, firstDay, lastDay);
                    break;
                case "mes":
                    firstDay = new DateTime(Convert.ToInt32(Request["ano"]), Convert.ToInt32(Request["mes"]), 1);
                    lastDay = firstDay.AddMonths(1).AddDays(-1);
                    audiencias = new Business.Audiencia().ListarAudienciaAgenda(advogado.IDAdvogado, firstDay, lastDay);
                    break;
                default:
                    audiencias = new Business.Audiencia().ListarAudienciaConvite(advogado.IDAdvogado);
                    break;
            }

            Holder holder = new Holder();
            holder.datas = new List<Data>();
            holder.anos = new List<int>();
            holder.total = audiencias.Count;

            audiencias.ForEach(delegate (Entity.Audiencia audiencia)
            {
                string _data = (audiencia.Data.Date == DateTime.Now.Date) ? "Hoje" : audiencia.Data.ToString("dd/MM/yyyy");

                if (!holder.datas.Exists(delegate (Data d) { return d.data == _data; }))
                    holder.datas.Add(new Data() { data = _data, audiencias = new List<Audiencia>() });

                List<Entity.Audiencia.Convite> convites = new Business.Audiencia.Convite().ListarConvite(audiencia.IDAudiencia);
                Entity.Audiencia.Convite convite = convites.Find(delegate (Entity.Audiencia.Convite c) { return c.Advogado_ID == advogado.IDAdvogado; });

                Entity.Advogado contratante = new Business.Advogado().ConsultarAdvogado(audiencia.Advogado_ID);
                Entity.Forum forum = new Business.Forum().ConsultarForum(audiencia.Forum_ID);

                Data data = holder.datas.Find(delegate (Data d) { return d.data == _data; });
                data.audiencias.Add(new Audiencia()
                {
                    data = audiencia.Data.ToString("dd/MM/yyyy"),
                    hora = audiencia.Data.ToString("HH:mm"),
                    materia = audiencia.Materia,
                    area = audiencia.AreaNome,
                    parte_representa = audiencia.ParteRepresenta,
                    parte_audiencia = audiencia.ParteAudiencia,
                    valor = audiencia.Valor.ToString("n2"),
                    descricao = audiencia.Descricao,
                    id = audiencia.IDAudiencia,
                    vencido = audiencia.Data < DateTime.Now,
                    status = audiencia.Status,
                    curti = audiencia.Curti,
                    indico = audiencia.Indico,
                    convite = convite.IDConvite,
                    contratante = new Audiencia.Contratante()
                    {
                        nome = contratante.Nome,
                        oab = contratante.Oab,
                        seccional = contratante.Seccional,
                        endereco = contratante.Endereco,
                        bairro = contratante.Bairro,
                        estado = contratante.Estado,
                        telefone = contratante.Telefone,
                        celular = contratante.Celular,
                        email = contratante.Email,
                        site = contratante.Site,
                        apresentacao = contratante.Apresentacao,
                        curtidas = contratante.Curtidas,
                        indicacoes = contratante.Indicacoes
                    },
                    forum = new Audiencia.Forum()
                    {
                        nome = forum.Nome,
                        endereco = forum.Endereco,
                        bairro = forum.Bairro,
                        cidade = forum.Cidade,
                        estado = forum.Estado,
                        cep = forum.Cep
                    }
                });
            });

            new Business.Audiencia().ListarAudienciaConvite(advogado.IDAdvogado).ForEach(delegate (Entity.Audiencia audiencia)
            {
                if (!holder.anos.Exists(delegate (int a) { return a == audiencia.Data.Year; }))
                    holder.anos.Add(audiencia.Data.Year);
            });

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Holder
        {
            public List<Data> datas { get; set; }
            public List<int> anos { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string data { get; set; }
            public List<Audiencia> audiencias { get; set; }
        }

        public class Audiencia
        {
            public string data { get; set; }
            public string hora { get; set; }
            public string materia { get; set; }
            public string area { get; set; }
            public string parte_representa { get; set; }
            public string parte_audiencia { get; set; }
            public string valor { get; set; }
            public string descricao { get; set; }
            public int id { get; set; }
            public bool vencido { get; set; }
            public int status { get; set; }
            public bool curti { get; set; }
            public bool indico { get; set; }
            public int convite { get; set; }
            public Contratante contratante { get; set; }
            public Forum forum { get; set; }

            public class Contratante
            {
                public string nome { get; set; }
                public string oab { get; set; }
                public string seccional { get; set; }
                public string endereco { get; set; }
                public string bairro { get; set; }
                public string estado { get; set; }
                public string telefone { get; set; }
                public string celular { get; set; }
                public string email { get; set; }
                public string site { get; set; }
                public string apresentacao { get; set; }
                public int curtidas { get; set; }
                public int indicacoes { get; set; }
            }

            public class Forum
            {
                public string nome { get; set; }
                public string endereco { get; set; }
                public string bairro { get; set; }
                public string cidade { get; set; }
                public string estado { get; set; }
                public string cep { get; set; }
            }
        }
    }
}