﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    public partial class api_ofertas_advogados : BaseApi
    {
        private decimal minDecimal = decimal.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Response.Write(new JavaScriptSerializer().Serialize(new Error("13", true, "Falha de autenticação.")));
                return;
            }

            if (Request["area"] == null || Request["area"] == "" || Request["cidade"] == null || Request["cidade"] == "" || Request["data"] == null || Request["data"] == "" || Request["valor"] == null || Request["valor"] == "")
            {
                Response.Write(new JavaScriptSerializer().Serialize(new Error()));
                return;
            }

            if (!decimal.TryParse(Request["valor"], out minDecimal) || Convert.ToDecimal(Request["valor"]) < 0)
            {
                Response.Write(new JavaScriptSerializer().Serialize(new Error("11", true, "Valor inválido.")));
                return;
            }

            string Area_ID = Request["area"];
            string Cidade = Request["cidade"];
            DateTime Data = Convert.ToDateTime(Request["data"]);
            decimal Valor = Convert.ToDecimal(Request["valor"]);

            List<Entity.Advogado> advogados = new Business.Advogado().ListarAdvogado(advogado.IDAdvogado, Area_ID, Cidade, Data, Valor);
            advogados.Sort(delegate (Entity.Advogado a1, Entity.Advogado a2) { return a1.Nome.CompareTo(a2.Nome); });

            Advogado.AdvogadoHolder holder = new Advogado.AdvogadoHolder();
            holder.advogados = new List<Advogado>();
            holder.total = advogados.Count;

            advogados.ForEach(delegate (Entity.Advogado advogado1)
            {
                bool desistencia = false;
                bool marcado = false;

                if (Request["id"] != "")
                {
                    Entity.Audiencia.Convite convite = new Business.Audiencia.Convite().ConsultarConvite(Request["id"], advogado1.IDAdvogado);
                    desistencia = convite.Status_ID == 2 || convite.Status_ID == 5;
                    marcado = convite.IDConvite > 0;
                }

                if (!desistencia)
                {
                    holder.advogados.Add(new Advogado()
                    {
                        nome = advogado1.Nome,
                        oab = advogado1.Oab,
                        seccional = advogado1.Seccional,
                        apresentacao = advogado1.Apresentacao,
                        curtidas = advogado1.Curtidas,
                        indicacoes = advogado1.Indicacoes,
                        marcado = marcado,
                        id = advogado1.IDAdvogado
                    });
                }
            });

            if (Request["id"] != null && Request["id"] != "")
            {
                List<Entity.Audiencia.Convite> convites = new Business.Audiencia.Convite().ListarConvite(Request["id"]);
                convites.ForEach(delegate (Entity.Audiencia.Convite convite)
                {
                    if (convite.Status_ID != 2 && convite.Status_ID != 5)
                    {
                        holder.advogados.Add(new Advogado()
                        {
                            nome = convite.Nome,
                            oab = convite.Oab,
                            seccional = convite.Seccional,
                            apresentacao = convite.Apresentacao,
                            curtidas = convite.Curtidas,
                            indicacoes = convite.Indicacoes,
                            marcado = true,
                            id = convite.Advogado_ID
                        });
                    }
                });
            }

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Advogado
        {
            public string nome { get; set; }
            public string oab { get; set; }
            public string seccional { get; set; }
            public string apresentacao { get; set; }
            public int curtidas { get; set; }
            public int indicacoes { get; set; }
            public bool marcado { get; set; }
            public int id { get; set; }

            public class AdvogadoHolder : Error
            {
                public List<Advogado> advogados { get; set; }
                public int total { get; set; }
            }
        }
    }
}