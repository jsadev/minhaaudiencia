﻿namespace minhaaudiencia
{
    using System;
    using System.Web.Script.Serialization;

    public partial class api_ofertas_contratar : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Error error = new Error();
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["convite"] == null)
            {
                Error error = new Error();
                error.code = "21";
                error.error = true;
                error.message = "Parâmetro convite está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Entity.Retorno ret = new Business.Audiencia.Convite().AlterarConvite(Request["convite"], "Status_ID", "3");
            if (!ret.Status)
            {
                Error error = new Error();
                error.code = "7";
                error.error = true;
                error.message = ret.Erro;

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Entity.Audiencia.Convite convite = new Business.Audiencia.Convite().ConsultarConvite(Request["convite"]);
            Entity.Advogado advogado2 = new Business.Advogado().ConsultarAdvogado(convite.Advogado_ID);

            try
            {
                Util.Email oEmail = new Util.Email(advogado2.Email, "", "Contratação efetivada.", Util.Email.METODO.HTML);
                oEmail.AudienciaContratada(advogado2);
                oEmail.Enviar();
            }
            catch { }

            Response.Write(new JavaScriptSerializer().Serialize(new Error()));
            return;
        }
    }
}