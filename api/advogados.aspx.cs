﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    public partial class api_advogados : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Error error = new Error();
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            List<Entity.Advogado> advogados = (Request["uid"] == null) ? new Business.Advogado().ListarAdvogado() : new Business.Advogado().ListarAdvogado(Request["uid"]);
            advogados.Sort(delegate (Entity.Advogado a1, Entity.Advogado a2) { return a1.Nome.CompareTo(a2.Nome); });

            Advogado.AdvogadoHolder holder = new Advogado.AdvogadoHolder();
            holder.advogados = new List<Advogado>();
            holder.total = advogados.Count;

            advogados.ForEach(delegate (Entity.Advogado advogado1)
            {
                holder.advogados.Add(new Advogado()
                {
                    nome = advogado1.Nome,
                    oab = advogado1.Oab,
                    seccional = advogado1.Seccional,
                    apresentacao = advogado1.Apresentacao,
                    curtidas = advogado1.Curtidas,
                    indicacoes = advogado1.Indicacoes,
                    id = advogado1.IDAdvogado
                });
            });

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Advogado
        {
            public string nome { get; set; }
            public string oab { get; set; }
            public string seccional { get; set; }
            public string apresentacao { get; set; }
            public int curtidas { get; set; }
            public int indicacoes { get; set; }
            public int id { get; set; }

            public class AdvogadoHolder : Error
            {
                public List<Advogado> advogados { get; set; }
                public int total { get; set; }
            }
        }
    }
}