﻿namespace minhaaudiencia
{
    using System;
    using System.Web.Script.Serialization;

    public partial class api_convites_aceitar : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Error error = new Error();
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["convite"] == null)
            {
                Error error = new Error();
                error.code = "21";
                error.error = true;
                error.message = "Parâmetro convite está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Entity.Retorno ret = new Business.Audiencia.Convite().AlterarConvite(Request["convite"], "Status_ID", "4");
            if (!ret.Status)
            {
                Error error = new Error();
                error.code = "7";
                error.error = true;
                error.message = ret.Erro;

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Response.Write(new JavaScriptSerializer().Serialize(new Error()));
            return;
        }
    }
}