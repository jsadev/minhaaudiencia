﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;
    using System.Linq;

    public partial class api_ofertas : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Error error = new Error();
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            List<Entity.Audiencia> audiencias = new List<Entity.Audiencia>();
            DateTime firstDay;
            DateTime lastDay;

            switch (Request["filtro"])
            {
                case "hoje":
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado, DateTime.Now, DateTime.Now);
                    break;
                case "amanha":
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado, DateTime.Now.AddDays(1), DateTime.Now.AddDays(1));
                    break;
                case "ontem":
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
                    break;
                case "semana":
                    firstDay = DateTime.Now.AddDays((int)DateTime.Now.DayOfWeek * -1);
                    lastDay = firstDay.AddDays(6);
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado, firstDay, lastDay);
                    break;
                case "ano":
                    firstDay = new DateTime(Convert.ToInt32(Request["ano"]), Convert.ToInt32(Request["mes"]), 1);
                    lastDay = firstDay.AddMonths(1).AddDays(-1);
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado, firstDay, lastDay);
                    break;
                case "mes":
                    firstDay = new DateTime(Convert.ToInt32(Request["ano"]), Convert.ToInt32(Request["mes"]), 1);
                    lastDay = firstDay.AddMonths(1).AddDays(-1);
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado, firstDay, lastDay);
                    break;
                default:
                    audiencias = new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado);
                    break;
            }

            Holder holder = new Holder();
            holder.datas = new List<Data>();
            holder.anos = new List<int>();
            holder.total = audiencias.Count;

            audiencias.ForEach(delegate (Entity.Audiencia audiencia)
            {
                string _data = (audiencia.Data.Date == DateTime.Now.Date) ? "Hoje" : audiencia.Data.ToString("dd/MM/yyyy");

                if (!holder.datas.Exists(delegate (Data d) { return d.data == _data; }))
                    holder.datas.Add(new Data() { data = _data, audiencias = new List<Audiencia>() });

                List<Audiencia.Convite> convites = new List<Audiencia.Convite>();
                List<Audiencia.Desistencia> desistencias = new List<Audiencia.Desistencia>();

                List<Entity.Audiencia.Convite> advogados = new Business.Audiencia.Convite().ListarConvite(audiencia.IDAudiencia);
                advogados.ForEach(delegate (Entity.Audiencia.Convite convite)
                {
                    convites.Add(new Audiencia.Convite()
                    {
                        nome = convite.Nome,
                        oab = convite.Oab,
                        seccional = convite.Seccional,
                        endereco = convite.Endereco,
                        bairro = convite.Bairro,
                        estado = convite.Estado,
                        telefone = convite.Telefone,
                        celular = convite.Celular,
                        email = convite.Email,
                        site = convite.Site,
                        apresentacao = convite.Apresentacao,
                        status = convite.Status_ID,
                        curtidas = convite.Curtidas,
                        indicacoes = convite.Indicacoes,
                        curti = convite.Curti,
                        indico = convite.Indico,
                        id = convite.IDConvite
                    });

                    if (convite.Status_ID == 5)
                    {
                        desistencias.Add(new Audiencia.Desistencia()
                        {
                            nome = convite.Nome,
                            motivo = convite.Motivo
                        });
                    }
                });

                Data data = holder.datas.Find(delegate (Data d) { return d.data == _data; });
                data.audiencias.Add(new Audiencia()
                {
                    data = audiencia.Data.ToString("dd/MM/yyyy"),
                    hora = audiencia.Data.ToString("HH:mm"),
                    materia = audiencia.Materia,
                    area = new Business.Area().ConsultarArea(audiencia.Area_ID).Nome,
                    id = audiencia.IDAudiencia,
                    vencido = audiencia.Data < DateTime.Now,
                    status = audiencia.Status,
                    convites = convites,
                    desistencias = desistencias
                });
            });

            new Business.Audiencia().ListarAudienciaOferta(advogado.IDAdvogado).ForEach(delegate (Entity.Audiencia audiencia)
            {
                if (!holder.anos.Exists(delegate (int a) { return a == audiencia.Data.Year; }))
                    holder.anos.Add(audiencia.Data.Year);
            });

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Holder
        {
            public List<Data> datas { get; set; }
            public List<int> anos { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string data { get; set; }
            public List<Audiencia> audiencias { get; set; }
        }

        public class Audiencia
        {
            public string data { get; set; }
            public string hora { get; set; }
            public string materia { get; set; }
            public string area { get; set; }
            public int id { get; set; }
            public bool vencido { get; set; }
            public int status { get; set; }
            public List<Convite> convites { get; set; }
            public List<Desistencia> desistencias { get; set; }

            public class Convite
            {
                public string nome { get; set; }
                public string oab { get; set; }
                public string seccional { get; set; }
                public string endereco { get; set; }
                public string bairro { get; set; }
                public string estado { get; set; }
                public string telefone { get; set; }
                public string celular { get; set; }
                public string email { get; set; }
                public string site { get; set; }
                public string apresentacao { get; set; }
                public int status { get; set; }
                public int curtidas { get; set; }
                public int indicacoes { get; set; }
                public bool curti { get; set; }
                public bool indico { get; set; }
                public int id { get; set; }
            }

            public class Desistencia
            {
                public string nome { get; set; }
                public string motivo { get; set; }
            }
        }
    }
}