﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    public partial class api_cidades : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Entity.Cidade> cidades = new Business.Cidade().ListarCidade(Request["area"].ToString().Split(','));

            Cidade.CidadeHolder holder = new Cidade.CidadeHolder();
            holder.cidades = new List<Cidade>();
            holder.total = cidades.Count;

            cidades.ForEach(delegate (Entity.Cidade cidade)
            {
                holder.cidades.Add(new Cidade()
                {
                    nome = cidade.Nome,
                    estado = cidade.Estado,
                    id = cidade.IDCidade
                });
            });

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Cidade
        {
            public string nome { get; set; }
            public string estado { get; set; }
            public int id { get; set; }

            public class CidadeHolder : Error
            {
                public List<Cidade> cidades { get; set; }
                public int total { get; set; }
            }
        }
    }
}