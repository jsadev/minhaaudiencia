﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    public partial class api_foruns : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Entity.Forum> foruns = new List<Entity.Forum>();

            if (Request["cidade"] != null && Request["area"] != null)
                foruns = new Business.Forum().ListarForum(Request["cidade"], Request["area"].ToString().Split(','));
            else if (Request["cidade"] != null)
                foruns = new Business.Forum().ListarForumCidade(Request["cidade"]);

            Forum.ForumHolder holder = new Forum.ForumHolder();
            holder.foruns = new List<Forum>();
            holder.total = foruns.Count;

            foruns.ForEach(delegate (Entity.Forum forum)
            {
                holder.foruns.Add(new Forum()
                {
                    nome = forum.Nome,
                    endereco = forum.Endereco,
                    bairro = forum.Bairro,
                    cidade = forum.Cidade,
                    estado = forum.Estado,
                    cep = forum.Cep,
                    id = forum.IDForum
                });
            });

            Response.Write(new JavaScriptSerializer().Serialize(holder));
        }

        public class Forum
        {
            public string nome { get; set; }
            public string endereco { get; set; }
            public string bairro { get; set; }
            public string cidade { get; set; }
            public string estado { get; set; }
            public string cep { get; set; }
            public int id { get; set; }

            public class ForumHolder : Error
            {
                public List<Forum> foruns { get; set; }
                public int total { get; set; }
            }
        }
    }
}