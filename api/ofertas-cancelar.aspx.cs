﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    public partial class api_ofertas_cancelar : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (advogado.IDAdvogado == 0)
            {
                Error error = new Error();
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["audiencia"] == null)
            {
                Error error = new Error();
                error.code = "21";
                error.error = true;
                error.message = "Parâmetro audiencia está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["motivo"] == null)
            {
                Error error = new Error();
                error.code = "21";
                error.error = true;
                error.message = "Parâmetro motivo está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            Entity.Retorno ret = new Business.Audiencia().CancelarAudiencia(Request["audiencia"], Request["motivo"]);
            if (!ret.Status)
            {
                Error error = new Error();
                error.code = "7";
                error.error = true;
                error.message = ret.Erro;

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            List<Entity.Audiencia.Convite> convites = new Business.Audiencia.Convite().ListarConvite(Request["audiencia"]);
            convites.ForEach(delegate (Entity.Audiencia.Convite convite)
            {
                ret = new Business.Audiencia.Convite().AlterarConvite(convite.IDConvite, "Status_ID", "6");
                if (!ret.Status)
                {
                    Error error = new Error();
                    error.code = "7";
                    error.error = true;
                    error.message = ret.Erro;

                    Response.Write(new JavaScriptSerializer().Serialize(error));
                    return;
                }
            });

            Response.Write(new JavaScriptSerializer().Serialize(new Error()));
            return;
        }
    }
}