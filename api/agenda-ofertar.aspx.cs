﻿namespace minhaaudiencia
{
    using System;
    using System.Web.Script.Serialization;

    public partial class api_agenda_ofertar : BaseApi
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Error error = new Error();

            if (advogado.IDAdvogado == 0)
            {
                error.code = "13";
                error.error = true;
                error.message = "Falha de autenticação";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["audiencia"] == null)
            {
                error.code = "7";
                error.error = true;
                error.message = "Parâmetro audiencia está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            if (Request["advogados"] == null)
            {
                error.code = "7";
                error.error = true;
                error.message = "Parâmetro advogados está faltando.";

                Response.Write(new JavaScriptSerializer().Serialize(error));
                return;
            }

            int IDAudiencia = Convert.ToInt32(Request["audiencia"]);
            string[] Advogados = Request["advogados"].Split(',');

            foreach (string IDAdvogado in Advogados)
            {
                Entity.Audiencia.Convite convite = new Entity.Audiencia.Convite();
                convite.Audiencia_ID = IDAudiencia;
                convite.Advogado_ID = Convert.ToInt32(IDAdvogado);
                convite.Status_ID = 0;

                // Adiciona o convite no banco de dados.
                Entity.Retorno ret = new Business.Audiencia.Convite().InserirConvite(convite);
                if (!ret.Status)
                {
                    error.code = "5";
                    error.error = true;
                    error.message = ret.Erro;

                    Response.Write(new JavaScriptSerializer().Serialize(error));
                    return;
                }

                // Consulta os dados do convidado e envia um e-mail de notificação.
                Entity.Advogado convidado = new Business.Advogado().ConsultarAdvogado(Convert.ToInt32(IDAdvogado));

                try
                {
                    Util.Email oEmail = new Util.Email(convidado.Email, "", "Convite para nova audiência", Util.Email.METODO.HTML);
                    oEmail.AudienciaConvite(convidado);
                    oEmail.Enviar();
                }
                catch { }
            }

            Response.Write(new JavaScriptSerializer().Serialize(error));
        }
    }
}