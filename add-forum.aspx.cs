﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class add_forum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void MontarForuns()
        {
            List<Entity.Forum> foruns = new Business.Forum().ListarForumCidade(Request["c"]);

            foruns.ForEach(delegate (Entity.Forum forum)
            {
                Response.Write("<div class='col-sm-3'>\n");
                Response.Write("<input type='checkbox' name='chkForum' class='ace' value='" + forum.IDForum + "' />\n");
                Response.Write("<span class='lbl'>&nbsp;" + forum.Nome + "</span>\n");
                Response.Write("</div>\n");
            });
        }
    }
}