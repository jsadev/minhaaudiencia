﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alterar-senha.aspx.cs" Inherits="minhaaudiencia.minha_conta_alterar_senha" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>Minha Conta</li>
    <li class="active">Alterar Senha</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Alterar Senha</h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Senha</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtSenha" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="20" placeholder="senha" TextMode="Password" />
                        <asp:Label ID="lblSenha" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2">Confirme a Senha</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtConfirmarSenha" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="20" placeholder="senha" TextMode="Password" />
                        <asp:Label ID="lblConfirmarSenha" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i>Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>