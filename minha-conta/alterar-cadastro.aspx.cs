﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class minha_conta_alterar_cadastro : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                new Util.Estado(ddlEstado);
                new Util.Estado(ddlSeccional);
                MontarPlanos();
                MontarAreas();

                PreencherCampos();
            }
        }

        private void MontarPlanos()
        {
            List<Entity.Plano> planos = new Admin.Plano().ListarPlano();
            rptPlano.DataSource = planos;
            rptPlano.DataBind();
        }

        public string PlanoChecked(object Valor)
        {
            Entity.Advogado advogado = (Entity.Advogado)Session[Constante.Sessions.LOGIN];

            if (Valor.ToString() == advogado.Plano_ID.ToString())
                return "checked='checked'";
            else
                return "";
        }

        public string Plano(object Valor)
        {
            if (Convert.ToDecimal(Valor) == 0)
                return "Grátis";
            else
                return "Apenas " + Convert.ToDecimal(Valor).ToString("c") + " por mês";
        }

        private void MontarAreas()
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add(new ListItem("Selecione a Área de Atuação", "0"));
            new Admin.Area().ListarArea().ForEach(delegate (Entity.Area area)
            {
                ddlArea.Items.Add(new ListItem(area.Nome, area.IDArea.ToString()));
            });
        }

        private void PreencherCampos()
        {
            Entity.Advogado advogado = (Entity.Advogado)Session[Constante.Sessions.LOGIN];

            txtNome.Text = advogado.Nome;
            txtCpf.Text = advogado.Cpf;
            txtOab.Text = advogado.Oab;
            if (advogado.Seccional.isFilled())
                ddlSeccional.Items.FindByValue(advogado.Seccional).Selected = true;
            txtEndereco.Text = advogado.Endereco;
            txtBairro.Text = advogado.Bairro;
            txtCidade.Text = advogado.Cidade;
            if (advogado.Estado.isFilled())
                ddlEstado.Items.FindByValue(advogado.Estado).Selected = true;
            txtCep.Text = advogado.Cep;
            txtTelefone.Text = advogado.Telefone;
            txtCelular.Text = advogado.Celular;
            txtSite.Text = advogado.Site;
            txtEmail.Text = advogado.Email;
            txtApresentacao.Text = advogado.Apresentacao;
            txtValorMinimo.Text = advogado.ValorMinimo.ToString("n2");
            radNotificacao1.Checked = (advogado.Notificacao == 1);
            radNotificacao2.Checked = (advogado.Notificacao == 2);
            chkAtivo.Checked = advogado.Ativo;

            // áreas
            rptArea.DataSource = new Admin.Advogado.Area().ListarArea(advogado.IDAdvogado);
            rptArea.DataBind();

            // fóruns
            rptForum.DataSource = new Admin.Advogado.Forum().ListarForum(advogado.IDAdvogado);
            rptForum.DataBind();
        }

        public string IsForum(object Forum_ID)
        {
            List<Entity.Advogado.Forum> foruns = new Admin.Advogado.Forum().ListarForum(advogado.IDAdvogado);

            if (foruns.Exists(delegate (Entity.Advogado.Forum f) { return f.IDForum.ToString() == Forum_ID.ToString(); }))
                return " checked='checked'";

            return "";
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Cpf = txtCpf.Text.ToPrepare();
            string Oab = txtOab.Text.ToPrepare();
            string Seccional = ddlSeccional.SelectedValue;
            string Endereco = txtEndereco.Text.ToPrepare();
            string Bairro = txtBairro.Text.ToPrepare();
            string Cidade = txtCidade.Text.ToPrepare();
            string Estado = ddlEstado.SelectedValue;
            string Cep = txtCep.Text.ToPrepare();
            string Telefone = txtTelefone.Text.ToPrepare();
            string Celular = txtCelular.Text.ToPrepare();
            string Site = txtSite.Text.ToPrepare();
            string Email = txtEmail.Text.ToPrepare();
            string Apresentacao = txtApresentacao.Text.ToPrepare();
            string Plano = Request.Form["radPlano"];
            string ValorMinimo = txtValorMinimo.Text.ToPrepare();
            int Notificacao = (radNotificacao1.Checked) ? 1 : (radNotificacao2.Checked) ? 2 : 0;
            bool Ativo = chkAtivo.Checked;
            bool Validar = true;

            LimparLabel();

            if (!Nome.isFilled())
            {
                Validar = false;
                lblNome.Text = "- campo obrigatório";
                txtNome.BackColor = Color.LightYellow;
            }
            if (!Cpf.isFilled())
            {
                Validar = false;
                lblCpf.Text = "- campo obrigatório";
                txtCpf.BackColor = Color.LightYellow;
            }
            if (!Oab.isFilled())
            {
                Validar = false;
                lblOab.Text = "- campo obrigatório";
                txtOab.BackColor = Color.LightYellow;
            }
            if (ddlSeccional.SelectedIndex == 0)
            {
                Validar = false;
                lblSeccional.Text = "- selecione a seccional";
                ddlSeccional.BackColor = Color.LightYellow;
            }
            if (!Endereco.isFilled())
            {
                Validar = false;
                lblEndereco.Text = "- campo obrigatório";
                txtEndereco.BackColor = Color.LightYellow;
            }
            if (!Bairro.isFilled())
            {
                Validar = false;
                lblBairro.Text = "- campo obrigatório";
                txtBairro.BackColor = Color.LightYellow;
            }
            if (!Cidade.isFilled())
            {
                Validar = false;
                lblCidade.Text = "- campo obrigatório";
                txtCidade.BackColor = Color.LightYellow;
            }
            if (ddlEstado.SelectedIndex == 0)
            {
                Validar = false;
                lblEstado.Text = "- campo obrigatório";
                ddlEstado.BackColor = Color.LightYellow;
            }
            if (!Cep.isFilled())
            {
                Validar = false;
                lblCep.Text = "- campo obrigatório";
                txtCep.BackColor = Color.LightYellow;
            }
            if (!ValorMinimo.isFilled())
            {
                Validar = false;
                lblValorMinimo.Text = "- campo obrigatório";
                txtValorMinimo.BackColor = Color.LightYellow;
            }
            if (!Plano.isFilled())
            {
                Validar = false;
                lblPlano.Text = "- selecione o plano";
            }

            if (Validar)
            {
                Entity.Advogado advogado = new Entity.Advogado();
                advogado.Nome = Nome;
                advogado.Cpf = Cpf.RemoveMask();
                advogado.Oab = Oab;
                advogado.Seccional = Seccional;
                advogado.Endereco = Endereco;
                advogado.Bairro = Bairro;
                advogado.Cidade = Cidade;
                advogado.Estado = Estado;
                advogado.Cep = Cep.RemoveMask();
                advogado.Telefone = Telefone;
                advogado.Celular = Celular;
                advogado.Site = Site;
                advogado.Email = Email;
                advogado.Apresentacao = Apresentacao;
                advogado.Plano_ID = Convert.ToInt32(Plano);
                advogado.CidadeAtuacao = "";
                advogado.ValorMinimo = Convert.ToDecimal(ValorMinimo);
                advogado.Notificacao = Notificacao;
                advogado.Ativo = Ativo;

                advogado.IDAdvogado = ((Entity.Advogado)Session[Constante.Sessions.LOGIN]).IDAdvogado;
                Entity.Retorno ret = new Admin.Advogado().AlterarAdvogado(advogado);
                if (!ret.Status)
                {
                    Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                    return;
                }

                // altera áreas
                new Admin.Advogado.Area().ExcluirArea(advogado.IDAdvogado);
                if (Request.Form["chkArea"] != null)
                {
                    string[] Areas = Request.Form["chkArea"].Split(',');
                    foreach (string IDArea in Areas)
                        new Admin.Advogado.Area().InserirArea(advogado.IDAdvogado, IDArea);
                }

                // altera fóruns
                new Admin.Advogado.Forum().ExcluirForum(advogado.IDAdvogado);
                if (Request.Form["chkForum"] != null)
                {
                    string[] Foruns = Request.Form["chkForum"].Split(',');
                    foreach (string IDForum in Foruns)
                        new Admin.Advogado.Forum().InserirForum(advogado.IDAdvogado, IDForum);
                }

                Session[Constante.Sessions.LOGIN] = advogado;

                Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Dados alterado com sucesso.");
                PreencherCampos();
            }
        }

        protected void lnkForum_Command(object sender, CommandEventArgs e)
        {
            Entity.Retorno ret = new Business.Advogado.Forum().ExcluirForum(((Entity.Advogado)Session[Constante.Sessions.LOGIN]).IDAdvogado, e.CommandArgument);
            if (!ret.Status)
            {
                Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                return;
            }

            PreencherCampos();
        }
    }
}