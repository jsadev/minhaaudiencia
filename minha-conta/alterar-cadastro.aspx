﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alterar-cadastro.aspx.cs" Inherits="minhaaudiencia.minha_conta_alterar_cadastro" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <link href="../assets/css/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" />
    <link href="../assets/css/jquery-impromptu.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>Minha Conta</li>
    <li class="active">Alterar Cadastro</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Alterar Cadastro</h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nome Completo</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtNome" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="100" placeholder="insira o nome completo"></asp:TextBox>
                        <asp:Label ID="lblNome" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">CPF</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCpf" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-cpf" MaxLength="14" placeholder="(apenas números)"></asp:TextBox>
                        <asp:Label ID="lblCpf" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">OAB</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtOab" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="6" placeholder="(apenas números)"></asp:TextBox>
                        <asp:Label ID="lblOab" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Seccional</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlSeccional" runat="server"></asp:DropDownList>
                        <asp:Label ID="lblSeccional" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1">Endereço (Rua, Número e Complemento)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtEndereco" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                        <asp:Label ID="lblEndereco" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Bairro</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtBairro" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50" placeholder="insira o nome completo"></asp:TextBox>
                        <asp:Label ID="lblBairro" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Cidade</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCidade" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="50" placeholder="insira o nome completo"></asp:TextBox>
                        <asp:Label ID="lblCidade" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Estado</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <asp:Label ID="lblEstado" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-4">CEP</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtCep" runat="server" CssClass="input-medium input-mask-cep form-control" MaxLength="9" placeholder="99999-999"></asp:TextBox>
                        <asp:Label ID="lblCep" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-2">Telefone<small class="text-warning"></small> </label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="ace-icon fa fa-phone"></i></span>
                            <asp:TextBox ID="txtTelefone" runat="server" CssClass="input-mask-phone col-xs-10 col-sm-5" placeholder="+99 (99) 9999-9999"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblTelefone" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-mask-2">Celular<small class="text-warning">(preferencialmente com WhatsApp)</small> </label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="ace-icon fa fa-phone"></i></span>
                            <asp:TextBox ID="txtCelular" runat="server" CssClass="input-mask-cell col-xs-10 col-sm-5" placeholder="+99 (99) 9999-9999"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblCelular" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Site ou Perfil de Redes Socias(opcional)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtSite" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="200"></asp:TextBox>
                        <asp:Label ID="lblSite" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Email</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="col-xs-10 col-sm-5" placeholder="email@email.com" MaxLength="100" ReadOnly="true" />
                        <asp:Label ID="lblEmail" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-11">Apresentação</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtApresentacao" runat="server" CssClass="autosize-transition form-control col-xs-10 col-sm-5" rows="7" TextMode="MultiLine" placeholder="Faça uma pequena apresentação sobre o seu perfil e qualificações profissionais"></asp:TextBox>
                        <asp:Label ID="lblApresentacao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>

                <!-- start Planos -->
                <hr />
                <div class="form-group">
                    <h4 class="col-sm-3 control-label no-padding-right bolder blue">Escolha um Plano</h4>
                    <div class="col-sm-9">
                        <asp:Label ID="lblPlano" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        <asp:Repeater ID="rptPlano" runat="server">
                            <ItemTemplate>
                                <hr />
                                <div class="radio radio-primary">
                                    <input id="rad<%# Eval("IDPlano") %>" type="radio" name="radPlano" value="<%# Eval("IDPlano") %>" <%# PlanoChecked(Eval("IDPlano")) %> />
                                    <label class="lbl" for="rad<%# Eval("IDPlano") %>"> <%# Plano(Eval("Valor")) %>
                                </div>
                                <small>Oferecer Audiências e usar como Agenda</small></span>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!-- end Planos -->

                <!-- start Áreas de Atuação -->
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Selecione a(s) Área de Autuação na(s) qual(is) está disposto a fazer Audiências</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlArea" runat="server" CssClass="chosen-select" style="width: auto"></asp:DropDownList>
                        <button id="btnArea" type="button" class="btn btn-info btn-white btn-sm">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div id="areas" class="col-sm-9">
                        <asp:Repeater ID="rptArea" runat="server">
                            <ItemTemplate>
                                <input id="chkArea<%# Eval("IDArea") %>" name="chkArea" type="checkbox" class="ace" value="<%# Eval("IDArea") %>" checked="checked" />
                                <label for="chkArea<%# Eval("IDArea") %>" class="lbl"> <%# Eval("Nome") %></label>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!-- end Áreas de Atuação -->

                <!-- start Fóruns -->
                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Selecione os Fóruns que Atende</label>
                    <div id="foruns" class="col-sm-9">
                        <div class="col-sm-6" style="border: solid #abbac3 1px; padding: 0 !important; border-radius: 5px">
                            <div class="col-sm-10" style="padding-top: 8px !important">
                                Selecione os Fóruns que Atende
                            </div>
                            <button id="btnCidade" type="button" class="col-sm-2 btn btn-sm">
                                <i class="fa fa-plus fa-lg"></i>
                            </button>
                        </div>
                        <div class="col-lg-12" style="margin-top: 15px;">
                            <asp:Repeater ID="rptForum" runat="server">
                                <ItemTemplate>
                                    <input id="chkForum<%# Eval("IDForum") %>" type="checkbox" name="chkForum" class="ace hidden" value="<%# Eval("IDForum") %>" <%# IsForum(Eval("IDForum")) %> />
                                    <label class="lbl col-sm-3 hidden" for="chkForum<%# Eval("IDForum") %>"> <%# Eval("Nome") %></label>
                                    <span class="badge badge-info">
                                        <%# Eval("Nome") %>
                                        <asp:LinkButton ID="lnkForum" runat="server" CommandArgument='<%# Eval("IDForum") %>' OnCommand="lnkForum_Command"><i class="fa fa-times" style="color: #fff"></i></asp:LinkButton>
                                    </span>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <!-- end Fóruns -->

                <hr />
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Informe o Valor Mínimo para receber ofertas</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtValorMinimo" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="Informe o Valor Mínimo para receber ofertas"></asp:TextBox>
                        <asp:Label ID="lblValorMinimo" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Como deseja receber as Notificações?</label>
                    <div class="col-sm-9">
                        <div class="radio radio-primary">
                            <asp:RadioButton ID="radNotificacao1" runat="server" GroupName="radNotificacao" />
                            <label class="lbl" for="conteudo_radNotificacao1"> A Cada interação</label>
                        </div>
                        <div class="radio radio-primary">
                            <asp:RadioButton ID="radNotificacao2" runat="server" GroupName="radNotificacao" />
                            <label class="lbl" for="conteudo_radNotificacao2"> Resumo diário</label>
                        </div>
                        <asp:Label ID="lblNotificacao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        <asp:CheckBox ID="chkAtivo" runat="server" />
						<label class="lbl" for="conteudo_chkAtivo"> Ativo</label>
                        <asp:Label ID="lblAtivo" runat="server" CssClass="help-inline text-danger"></asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i>Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="../assets/js/jquery.maskedinput.min.js"></script>
    <script src="../assets/js/jquery.maskMoney.min.js"></script>
    <script src="../assets/js/interno/config.js"></script>
    <script src="../assets/js/jquery-impromptu.js"></script>
    <script type="text/javascript">
        var root = '<%=ConfigurationManager.AppSettings["Url"]%>';
        var uid = '<%=Request["uid"]%>';

        var fs = [];

        jQuery(function () {
            $('.input-mask-cpf').mask('999.999.999-99');
            $('.input-mask-cep').mask('99999-999');
            $('.input-mask-phone').mask('+99 (99) 9999-9999');
            $('.input-mask-cell').mask('+99 (99) 99999-9999');
            $('.input-mask-money').maskMoney({ thousands: '.', decimal: ',' });

            $('#btnArea').click(function () {
                var area_id = $('#conteudo_ddlArea').val();
                var text = $('#conteudo_ddlArea option:selected').text();

                if (area_id == 0) {
                    alert('Selecione a Área antes de adicionar.');
                    return;
                }

                var areas = $('#areas');

                if (areas.html().indexOf('chkArea' + area_id) == -1) {
                    var html = '<input id="chkArea' + area_id + '" name="chkArea" type="checkbox" class="ace" value="' + area_id + '" checked="checked" />\n';
                    html += '<label for="chkArea' + area_id + '" class="lbl"> ' + text + '</label>\n';

                    areas.append(html);
                }
            });

            $('#btnCidade').click(function () {
                var as = [];
                $('input[name="chkArea"]').each(function () {
                    if ($(this).prop("checked"))
                        as.push($(this).val());
                });

                $.ajax({
                    url: Url + "/api/cidades.aspx",
                    data: { area: as.toString() },
                    success: function (data) {
                        data = eval('(' + data + ')');
                        if (data.error) {
                            alert(data.message);
                            return;
                        }

                        var html = '<div class="col-sm-12">\n';
                        html += '<div class="form-group">\n';
                        html += '<select name="ddlCidade" class="" style="width: auto">\n';
                        html += '<option value="">Cidade</option>\n';
                        for (i = 0; i < data.total; i++) {
                            var cidade = data.cidades[i];
                            html += '<option value="' + cidade.id + '">' + cidade.nome + '</option>\n';
                        }
                        html += '</select>\n';
                        html += '</div>\n';
                        html += '</div>\n';

                        html += '<div id="cidades" class="col-sm-12 m-t-sm" style="border: solid #eee 1px; border-radius: 2px">\n';
                        html += '</div>\n';

                        $.prompt(html, {
                            title: "Fóruns que Atende",
                            buttons: { "OK": true, "Cancelar": false },
                            submit: function (e, v, m, f) {
                                if (v) {
                                    parent.$('#foruns').html('');
                                    parent.$('#labForum').html('');

                                    $.ajax({
                                        url: Url + '/api/forum-inserir.aspx',
                                        data: { advogado_id: <%=advogado.IDAdvogado%>, foruns: fs.toString() },
                                        success: function (data) {
                                            data = eval('(' + data + ')');
                                            if (data.error) {
                                                alert(data.message);
                                                return;
                                            }

                                            window.location = window.location.toString();
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            switch (xhr.status) {
                                                case 404:
                                                    alert('Página não encontrada.');
                                                    break;
                                                default:
                                                    alert(thrownError);
                                                    break;
                                            }
                                        }
                                    });

                                    $.prompt.close();
                                }
                            }
                        });

                        $('select[name="ddlCidade"]').change(function () {
                            $.ajax({
                                url: Url + "/api/foruns",
                                data: { cidade: $(this).val(), area: as.toString() },
                                success: function (data) {
                                    data = eval('(' + data + ')');
                                    if (data.error) {
                                        alert(data.message);
                                        return;
                                    }

                                    html = '<div class="form-group">\n';
                                    for (i = 0; i < data.total; i++) {
                                        var forum = data.foruns[i];

                                        html += '<div class="col-sm-6">\n';
                                        html += '<div class="checkbox checkbox-inline">\n';
                                        if (CheckCookie(fs, forum.id))
                                            html += '<input id="chkF' + forum.id + '" type="checkbox" name="chkForum1" value="' + forum.id + '" checked="checked" />\n';
                                        else
                                            html += '<input id="chkF' + forum.id + '" type="checkbox" name="chkForum1" value="' + forum.id + '" />\n';
                                        html += '<label for="chkF' + forum.id + '">' + forum.nome + '</label>\n';
                                        html += '</div>\n';
                                        html += '</div>\n';
                                    }
                                    html += '</div>\n';

                                    $('#cidades').html(html);

                                    $('input[name="chkForum1"]').click(function () {
                                        var id = $(this).val();
                                        var name = $('label[for="chkF' + id + '"]').html()

                                        if ($(this).prop("checked")) {
                                            fs.push(id);
                                        }
                                        else {
                                            var g = [];
                                            for (i = 0; i < fs.length; i++) {
                                                if (fs[i] != $(this).val())
                                                    g.push(fs[i]);
                                            }
                                            fs = g;
                                        }
                                    });
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    switch (xhr.status) {
                                        case 404:
                                            alert('Página não encontrada.');
                                            break;
                                        default:
                                            alert(thrownError);
                                            break;
                                    }
                                }
                            });
                        });
                    },
                    error: function (xhr, ajaxOptions, throwError) {
                        switch (xhr.status) {
                            case 404:
                                alert('Página não encontrada.');
                                break;
                            default:
                                alert(thrownError);
                                break;
                        }
                    }
                });
            });
        });

        function CheckCookie(array, valor) {
            for (j = 0; j < array.length; j++) {
                if (array[j] == valor || array[j][0] == valor)
                    return true;
            }

            return false;
        }
    </script>
</asp:Content>