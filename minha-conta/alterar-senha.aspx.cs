﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class minha_conta_alterar_senha : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Senha = txtSenha.Text.ToPrepare();
            string RepetirSenha = txtConfirmarSenha.Text.ToPrepare();
            bool Validar = true;

            if (Validar)
            {
                Entity.Advogado advogado = new Entity.Advogado();
                advogado.Senha = Senha;

                advogado.IDAdvogado = ((Entity.Advogado)Session[Constante.Sessions.LOGIN]).IDAdvogado;
                Entity.Retorno ret = new Admin.Advogado().AlterarSenha(advogado);
                if (!ret.Status)
                {
                    Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                    return;
                }

                Session[Constante.Sessions.LOGIN] = advogado;
                Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Senha alterada com sucesso.");
            }
        }
    }
}