﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class agendas_registro : BasePage
    {
        private DateTime minDate = DateTime.MinValue;
        private decimal minDecimal = decimal.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        private void PreencherCampos()
        {
            Entity.Audiencia audiencia = new Business.Audiencia().ConsultarAudiencia(Request["uid"]);
            Entity.Area area = new Admin.Area().ConsultarArea(audiencia.Area_ID);
            Entity.Forum forum = new Admin.Forum().ConsultarForum(audiencia.Forum_ID);
            Entity.Vara vara = new Admin.Vara().ConsultarVara(audiencia.Vara_ID);

            lblMateria.Text = audiencia.Materia;
            lblArea.Text = area.Nome;
            lblCidade.Text = audiencia.Cidade;
            lblForum.Text = forum.Nome;
            lblVara.Text = vara.Nome;
            lblData.Text = audiencia.Data.ToString("dd/MM/yyyy HH:mm");
            lblParteRepresenta.Text = audiencia.ParteRepresenta;
            lblParteAudiencia.Text = audiencia.ParteAudiencia;
            lblValor.Text = audiencia.Valor.ToString("c");
            lblDescricao.Text = audiencia.Descricao;
        }
    }
}