﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="minhaaudiencia.agendas_registro" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        input[type="text"] {
            border: none;
            background-color: #fff !important;
        }
    </style>
</asp:Content>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>Agenda</li>
    <li class="active">Audiência</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Audiência</h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Matéria da Audiência</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblMateria" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Área de Atuação</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblArea" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Cidade</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblCidade" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Fórum</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblForum" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Vara</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblVara" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="date-timepicker1">Data e Horário</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblData" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Parte que Representa</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblParteRepresenta" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Partes da Audiência</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblParteAudiencia" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Valor a Pagar</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblValor" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-11">Descrição (opcional)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="lblDescricao" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="window.location = 'listagem.aspx'"><i class="ace-icon fa fa-undo"></i> Voltar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>