﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class agendas_listagem : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MontarListagem();
        }

        private void MontarListagem()
        {
            List<Entity.Audiencia> audiencias = new Business.Audiencia().ListarAudienciaAgenda(advogado.IDAdvogado);

            rptListagem.DataSource = audiencias;
            rptListagem.DataBind();
        }
    }
}