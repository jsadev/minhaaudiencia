﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="listagem.aspx.cs" Inherits="minhaaudiencia.agendas_listagem" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li class="active">Agenda</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <!--<div class="page-header">
        <h1>Audiências</h1>
    </div>-->
    <div class="bg-filtros">
            <div class="col-xs-2">
                <!--Filtro Ano -->
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle">2016 <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">2016</a></li>
                        <li><a href="#">2015</a></li>
                        <li><a href="#">2014</a></li>
                        <li><a href="#">2013</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-4">
                <!--Filtro Mês -->
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Novembro <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Janeiro</a></li>
                        <li><a href="#">Fevereiro</a></li>
                        <li><a href="#">Março</a></li>
                        <li><a href="#">Abril</a></li>
                        <li><a href="#">Maio</a></li>
                        <li><a href="#">Junho</a></li>
                        <li><a href="#">Julho</a></li>
                        <li><a href="#">Agosto</a></li>
                        <li><a href="#">Setembro</a></li>
                        <li><a href="#">Outubro</a></li>
                        <li><a href="#">Novembro</a></li>
                        <li><a href="#">Dezembro</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-2">
                <!--Filtro Hoje -->
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Hoje <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Semana</a></li>
                        <li><a href="#">Ontem</a></li>
                        <li><a href="#">Hoje</a></li>
                        <li><a href="#">Amanhã</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-2">
                <!--Filtro Todos -->
                <div class="btn-group">
                    <button type="button" class="btn btn-grey">Todos</button>
                </div> 
            </div>
            <div class="col-xs-2">           
                <!-- Adicionar Agenda -->
                <button type="button" class="btn btn-info" title="Nova Agenda" onclick="window.location = '../agenda/registro.aspx'"><i class="fa fa-plus"></i></button>
            </div>
    </div>
    
    <!-- Quando não tem Agenda Cadastrada -->
    <div class="ofertas col-xs-12">
    	<div class="sem-audiencia">
        <p>Não há itens na Agenda.</p>
        </div>
    </div>
    
    
    <!-- Quando tem Agenda Cadastrada -->
    <!-- Titulo, que deve exibir o nome do Filtro que foi selecionado -->
    <div class="col-xs-12">
    	<h3>Hoje</h3>
    </div>
    <!-- Agenda Passada, sem Avaliação -->
    <div class="ofertas col-xs-12">
    	<div class="col-xs-4">
        	<div class="hora-passada">08:20</div>
        </div>
        <div class="col-xs-8">
        	<div class="tit-item"><a href="#modal-aud1">Título da Audiência</a></div>
            <div class="nat-item">Natureza</div>
            <div class="adv-item"><a href="#modal-adv1">por Nome do Contratante</a></div>
            <ul class="aval">
            	<li><a href="" class="ico-curti-off"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                <li><a href="" class="ico-recomendo-off"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                <li class="aval-off">avalie</li>
            </ul>
        </div>
    </div>
    <!-- Modal Audiência -->
    <div class="modalDialog col-xs-12" id="modal-aud1">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <h3>Detalhes da Audiência</h3>
            <ul class="resumo-adv">
                <li><big>Título da Audiência</big></li>
                <li>Natureza</li>
                <li>Comarca: <strong>XXX</strong></li>
                <li>Fórum: <strong>XXX</strong></li>
                <li>Parte que Representa: <strong>XXX</strong></li>
                <li>Partes da Audiência: <strong>XXX</strong></li>
                <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                <li>&nbsp;</li>
                <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
            </ul>
            <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>
    <!-- Modal Contratante -->
    <div class="modalDialog col-xs-12" id="modal-adv1">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <h3>Dados do Contratante</h3>
            <ul class="resumo-adv">
                <li><big>Nome do Contratante</big></li>
                <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i> 2
                </li>
                <li>OAB: 111111-SP</li>
                <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
            </ul>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>
    <!-- Agenda Passada, com Avaliação -->
    <div class="ofertas col-xs-12">
    	<div class="col-xs-4">
        	<div class="hora-passada">10:20</div>
        </div>
        <div class="col-xs-8">
        	<div class="tit-item"><a href="#modal-aud2">Título da Audiência</a></div>
            <div class="nat-item">Natureza</div>
            <div class="adv-item"><a href="#modal-adv2">por Nome do Contratante</a></div>
            <ul class="aval">
            	<li><a href="" class="ico-curti-on"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                <li><a href="" class="ico-recomendo-on"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                <li class="aval-on">avaliado</li>
            </ul>
        </div>
    </div>
    <!-- Modal Audiência -->
    <div class="modalDialog col-xs-12" id="modal-aud2">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <h3>Detalhes da Audiência</h3>
            <ul class="resumo-adv">
                <li><big>Título da Audiência</big></li>
                <li>Natureza</li>
                <li>Comarca: <strong>XXX</strong></li>
                <li>Fórum: <strong>XXX</strong></li>
                <li>Parte que Representa: <strong>XXX</strong></li>
                <li>Partes da Audiência: <strong>XXX</strong></li>
                <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                <li>&nbsp;</li>
                <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
            </ul>
            <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>
    <!-- Modal Contratante -->
    <div class="modalDialog col-xs-12" id="modal-adv2">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <h3>Dados do Contratante</h3>
            <ul class="resumo-adv">
                <li><big>Nome do Contratante</big></li>
                <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i> 2
                </li>
                <li>OAB: 111111-SP</li>
                <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
            </ul>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>

    <!-- Agenda Fechada, Contratado -->
    <div class="ofertas col-xs-12">
    	<div class="col-xs-4">
        	<div class="hora-futura">16:20</div>
        </div>
        <div class="col-xs-8">
        	<div class="tit-item"><a href="#modal-aud3">Título da Audiência</a></div>
            <div class="nat-item">Natureza</div>
            <div class="adv-item"><a href="#modal-adv3">por Nome do Contratante</a></div>
            <div class="contratado">Contratado <i class="fa fa-circle" aria-hidden="true"></i></div>
        </div>
    </div>
    <!-- Modal Audiência -->
    <div class="modalDialog col-xs-12" id="modal-aud3">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <h3>Detalhes da Audiência</h3>
            <ul class="resumo-adv">
                <li><big>Título da Audiência</big></li>
                <li>Natureza</li>
                <li>Comarca: <strong>XXX</strong></li>
                <li>Fórum: <strong>XXX</strong></li>
                <li>Parte que Representa: <strong>XXX</strong></li>
                <li>Partes da Audiência: <strong>XXX</strong></li>
                <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                <li>&nbsp;</li>
                <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
            </ul>
            <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>
    <!-- Modal Contratante -->
    <div class="modalDialog col-xs-12" id="modal-adv3">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <h3>Dados do Contratante</h3>
            <ul class="resumo-adv">
                <li><big>Nome do Contratante</big></li>
                <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i> 2
                </li>
                <li>OAB: 111111-SP</li>
                <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
            </ul>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>
    
    
    
    
    
    
    

    <div class="row" style="display:none;">
        <div class="col-xs-12">
            <button type="button" class="btn btn-success" onclick="window.location = '../audiencias/registro.aspx'"><i class="fa fa-plus"></i> Cadastrar</button>
            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header">Listagem de Audiências</div>
            <div>
                <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Fórum</th>
                            <th>Solicitante/Advogado</th>
                            <th>Período</th>
                            <th>Área de Atuação</th>
                            <th>Contato</th>
                            <th>Breve Descrição</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptListagem" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><a href="registro.aspx?uid=<%# Eval("IDAudiencia") %>"><%# Eval("ForumNome") %></a></td>
                                    <td><a href="registro.aspx?uid=<%# Eval("IDAudiencia") %>"><%# Eval("AdvogadoNome") %></a></td>
                                    <td><a href="registro.aspx?uid=<%# Eval("IDAudiencia") %>"><%# Convert.ToDateTime(Eval("Data")).ToString("dd/MM/yyyy HH:mm") %></a></td>
                                    <td><a href="registro.aspx?uid=<%# Eval("IDAudiencia") %>"><%# Eval("AreaNome") %></a></td>
                                    <td><a href="registro.aspx?uid=<%# Eval("IDAudiencia") %>"><%# Eval("Telefone") %></a></td>
                                    <td><a href="registro.aspx?uid=<%# Eval("IDAudiencia") %>"><%# Eval("Descricao") %></a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <!-- dataTables -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/jquery.dataTables.bootstrap.js"></script>
    <script src="../assets/js/dataTables/extensions/TableTools/js/dataTables.tableTools.js"></script>
    <script src="../assets/js/dataTables/extensions/ColVis/js/dataTables.colVis.js"></script>

    <script src="../assets/js/date-time/bootstrap-datepicker.min.js"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function ($) {
            $('.data-picker').datepicker({
                format: 'dd/mm/yyyy'
            });

            //initiate dataTables plugin
            var oTable1 =
            $('#dynamic-table')
            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
            .dataTable({
                bAutoWidth: false,
                "aoColumns": [
                  null, null, null, null, null, null,
                ],
                "aaSorting": [],
            });
            //oTable1.fnAdjustColumnSizing();

            //TableTools settings
            TableTools.classes.container = "btn-group btn-overlap";
            TableTools.classes.print = {
                "body": "DTTT_Print",
                "info": "tableTools-alert gritter-item-wrapper gritter-info gritter-center white",
                "message": "tableTools-print-navbar"
            }

            //initiate TableTools extension
            var tableTools_obj = new $.fn.dataTable.TableTools(oTable1, {
                "sSwfPath": "../assets/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf", //in Ace demo ../assets will be replaced by correct assets path

                "sRowSelector": "td:not(:last-child)",
                "sRowSelect": "multi",
                "fnRowSelected": function (row) {
                    //check checkbox when row is selected
                    try { $(row).find('input[type=checkbox]').get(0).checked = true }
                    catch (e) { }
                },
                "fnRowDeselected": function (row) {
                    //uncheck checkbox
                    try { $(row).find('input[type=checkbox]').get(0).checked = false }
                    catch (e) { }
                },

                "sSelectedClass": "success",
                "aButtons": [
                    {
                        "sExtends": "copy",
                        "sToolTip": "Copy to clipboard",
                        "sButtonClass": "btn btn-white btn-primary btn-bold",
                        "sButtonText": "<i class='fa fa-copy bigger-110 pink'></i>",
                        "fnComplete": function () {
                            this.fnInfo('<h3 class="no-margin-top smaller">Table copied</h3>\
									<p>Copied '+ (oTable1.fnSettings().fnRecordsTotal()) + ' row(s) to the clipboard.</p>',
                                1500
                            );
                        }
                    },

                    {
                        "sExtends": "csv",
                        "sToolTip": "Export to CSV",
                        "sButtonClass": "btn btn-white btn-primary  btn-bold",
                        "sButtonText": "<i class='fa fa-file-excel-o bigger-110 green'></i>"
                    },

                    {
                        "sExtends": "pdf",
                        "sToolTip": "Export to PDF",
                        "sButtonClass": "btn btn-white btn-primary  btn-bold",
                        "sButtonText": "<i class='fa fa-file-pdf-o bigger-110 red'></i>"
                    },

                    {
                        "sExtends": "print",
                        "sToolTip": "Print view",
                        "sButtonClass": "btn btn-white btn-primary  btn-bold",
                        "sButtonText": "<i class='fa fa-print bigger-110 grey'></i>",

                        "sMessage": "<div class='navbar navbar-default'><div class='navbar-header pull-left'><a class='navbar-brand' href='#'><small>Optional Navbar &amp; Text</small></a></div></div>",

                        "sInfo": "<h3 class='no-margin-top'>Print view</h3>\
									  <p>Please use your browser's print function to\
									  print this table.\
									  <br />Press <b>escape</b> when finished.</p>",
                    }
                ]
            });
            //we put a container before our table and append TableTools element to it
            $(tableTools_obj.fnContainer()).appendTo($('.tableTools-container'));

            //also add tooltips to table tools buttons
            //addding tooltips directly to "A" buttons results in buttons disappearing (weired! don't know why!)
            //so we add tooltips to the "DIV" child after it becomes inserted
            //flash objects inside table tools buttons are inserted with some delay (100ms) (for some reason)
            setTimeout(function () {
                $(tableTools_obj.fnContainer()).find('a.DTTT_button').each(function () {
                    var div = $(this).find('> div');
                    if (div.length > 0) div.tooltip({ container: 'body' });
                    else $(this).tooltip({ container: 'body' });
                });
            }, 200);

            //ColVis extension
            var colvis = new $.fn.dataTable.ColVis(oTable1, {
                "buttonText": "<i class='fa fa-search'></i>",
                "aiExclude": [0, 6],
                "bShowAll": true,
                //"bRestore": true,
                "sAlign": "right",
                "fnLabel": function (i, title, th) {
                    return $(th).text();//remove icons, etc
                }
            });

            //style it
            $(colvis.button()).addClass('btn-group').find('button').addClass('btn btn-white btn-info btn-bold')

            //and append it to our table tools btn-group, also add tooltip
            $(colvis.button())
            .prependTo('.tableTools-container .btn-group')
            .attr('title', 'Show/hide columns').tooltip({ container: 'body' });

            //and make the list, buttons and checkboxed Ace-like
            $(colvis.dom.collection)
            .addClass('dropdown-menu dropdown-light dropdown-caret dropdown-caret-right')
            .find('li').wrapInner('<a href="javascript:void(0)" />') //'A' tag is required for better styling
            .find('input[type=checkbox]').addClass('ace').next().addClass('lbl padding-8');

            /////////////////////////////////
            //table checkboxes
            $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

            //select/deselect all rows according to table header checkbox
            $('#dynamic-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
                var th_checked = this.checked;//checkbox inside "TH" table header

                $(this).closest('table').find('tbody > tr').each(function () {
                    var row = this;
                    if (th_checked) tableTools_obj.fnSelect(row);
                    else tableTools_obj.fnDeselect(row);
                });
            });

            //select/deselect a row when the checkbox is checked/unchecked
            $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
                var row = $(this).closest('tr').get(0);
                if (!this.checked) tableTools_obj.fnSelect(row);
                else tableTools_obj.fnDeselect($(this).closest('tr').get(0));
            });

            $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                e.preventDefault();
            });

            //And for the first simple table, which doesn't have TableTools or dataTables
            //select/deselect all rows according to table header checkbox
            var active_class = 'active';
            $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
                var th_checked = this.checked;//checkbox inside "TH" table header

                $(this).closest('table').find('tbody > tr').each(function () {
                    var row = this;
                    if (th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                    else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                });
            });

            //select/deselect a row when the checkbox is checked/unchecked
            $('#simple-table').on('click', 'td input[type=checkbox]', function () {
                var $row = $(this).closest('tr');
                if (this.checked) $row.addClass(active_class);
                else $row.removeClass(active_class);
            });

            /********************************/
            //add tooltip for small view action buttons in dropdown menu
            $('[data-rel="tooltip"]').tooltip({ placement: tooltip_placement });

            //tooltip placement on right or left
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                //var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
                return 'left';
            }

        })
    </script>
</asp:Content>