/** VARA */
CREATE TABLE dbo.ma_vara (
	IDVara int not null identity not for replication primary key,
	Nome varchar(50) not null,
	DataInclusao datetime not null default getdate()
)
GO