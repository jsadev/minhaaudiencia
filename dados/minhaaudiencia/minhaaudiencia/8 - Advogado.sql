/** ADVOGADO */
CREATE TABLE dbo.ma_advogado (
	IDAdvogado int not null identity not for replication primary key,
	Nome varchar(100) not null,
	Tipo char(2) not null,
	Cpf varchar(15) not null,
	Oab varchar(6) not null,
	Seccional varchar(50) null,
	Endereco varchar(200) not null,
	Bairro varchar(50) not null,
	Cidade varchar(50) not null,
	Estado char(2) not null,
	Cep varchar(8) not null,
	Telefone varchar(25) null,
	Celular varchar(25) null,
	Site varchar(200) null,
	Email varchar(100) null,
	Senha varchar(50) not null,
	Apresentacao text null,
	Plano_ID int not null,
	CidadeAtuacao varchar(50) null,
	ValorMinimo money not null,
	Notificacao int not null,
	Ativo bit not null default 0,
	Guid varchar(36) null,
	DataInclusao datetime not null default getdate(),
	foreign key (Plano_ID) references dbo.ma_plano (IDPlano)
)
GO

-- F�runs
CREATE TABLE dbo.ma_advogadoforum (
	Advogado_ID int not null,
	Forum_ID int not null,
	foreign key (Advogado_ID) references dbo.ma_advogado (IDAdvogado) on delete cascade,
	foreign key (Forum_ID) references dbo.ma_forum (IDForum) on delete cascade
)
GO

-- �reas de Atua��o
CREATE TABLE dbo.ma_advogadoarea (
	Advogado_ID int not null,
	Area_ID int not null,
	foreign key (Advogado_ID) references dbo.ma_advogado (IDAdvogado) on delete cascade,
	foreign key (Area_ID) references dbo.ma_area (IDArea) on delete cascade
)
GO