/** F�RUM */
CREATE TABLE dbo.ma_forum (
	IDForum int not null identity not for replication primary key,
	Nome varchar(50) not null,
	Endereco varchar(200) not null,
	Bairro varchar(50) not null,
	Cidade varchar(50) not null,
	Estado char(2) not null,
	Cep varchar(8) not null,
	DataInclusao datetime not null default getdate()
)
GO

CREATE TABLE dbo.ma_forumarea (
	Forum_ID int not null,
	Area_ID int not null,
	foreign key (Forum_ID) references dbo.ma_forum (IDForum) on delete cascade,
	foreign key (Area_ID) references dbo.ma_area (IDArea) on delete cascade
)
GO

CREATE TABLE dbo.ma_forumcidade (
	Forum_ID int not null,
	Cidade_ID int not null,
	foreign key (Forum_ID) references dbo.ma_forum (IDForum) on delete cascade,
	foreign key (Cidade_ID) references dbo.ma_cidade (IDCidade) on delete cascade
)
GO

CREATE TABLE dbo.ma_forumvara (
	Forum_ID int not null,
	Vara_ID int not null,
	foreign key (Forum_ID) references dbo.ma_forum (IDForum) on delete cascade,
	foreign key (Vara_ID) references dbo.ma_vara (IDVara) on delete cascade
)
GO