/** AUDI�NCIA */
CREATE TABLE dbo.ma_audiencia (
	IDAudiencia int not null identity not for replication primary key,
	Advogado_ID int not null,
	Processo varchar(50) not null,
	Materia varchar(100) not null,
	Area_ID int not null,
	Cidade varchar(50) not null,
	Forum_ID int not null,
	Vara_ID int not null,
	Data datetime not null,
	ParteRepresenta varchar(100) not null,
	ParteAudiencia varchar(100) not null,
	Valor money not null,
	Descricao text null,
	Curti bit not null default 0,
	Indico bit not null default 0,
	Cancelamento text null,
	DataInclusao datetime not null default getdate(),
	foreign key (Advogado_ID) references dbo.ma_advogado (IDAdvogado),
	foreign key (Area_ID) references dbo.ma_area (IDArea),
	foreign key (Forum_ID) references dbo.ma_forum (IDForum),
	foreign key (Vara_ID) references dbo.ma_vara (IDVara)
)
GO

CREATE TABLE dbo.ma_convite (
	IDConvite int not null identity not for replication primary key,
	Audiencia_ID int not null,
	Advogado_ID int not null,
	Status_ID int not null,
	Motivo text null,
	Curti bit not null default 0,
	Indico bit not null default 0,
	DataInclusao datetime not null default getdate(),
	foreign key (Audiencia_ID) references ma_audiencia (IDAudiencia),
	foreign key (Advogado_ID) references ma_advogado (IDAdvogado),
	foreign key (Status_ID) references ma_status (IDStatus)
)
GO