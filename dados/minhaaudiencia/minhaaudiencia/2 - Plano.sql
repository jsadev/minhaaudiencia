/** PLANO */
CREATE TABLE dbo.ma_plano (
	IDPlano int not null identity not for replication primary key,
	Nome varchar(50) not null,
	Valor money not null,
	DataInclusao datetime not null default getdate()
)
GO

INSERT INTO dbo.ma_plano (Nome,Valor) VALUES ('Gr�tis', 0.00)
INSERT INTO dbo.ma_plano (Nome,Valor) VALUES ('Plano 1', 12.99)
GO