/** PROCEDURES */

CREATE PROCEDURE dbo.ma_proc_pagina
	@IDPagina int
AS
BEGIN

	with pagina (Nome, Url, Icone, parentId, Ordem, Tipo, Nivel, IDPagina)
	as
	(
		SELECT Nome, Url, Icone, parentId, Ordem, Tipo, Nivel, IDPagina FROM dbo.ma_pagina WHERE IDPagina = @IDPagina
		UNION ALL
		SELECT p.Nome, p.Url, p.Icone, p.parentId, p.Ordem, p.Tipo, p.Nivel, p.IDPagina FROM dbo.ma_pagina p INNER JOIN pagina p2 ON p2.parentId = p.IDPagina
	)
	SELECT * FROM pagina

END