/** STATUS */
CREATE TABLE dbo.ma_status (
	IDStatus int not null primary key,
	Nome varchar(50) not null
)
GO

INSERT INTO ma_status VALUES (0, 'Pendente')
INSERT INTO ma_status VALUES (1, 'Disponível')
INSERT INTO ma_status VALUES (2, 'Indisponível')
INSERT INTO ma_status VALUES (3, 'Aceito')
INSERT INTO ma_status VALUES (4, 'Desistência')
INSERT INTO ma_status VALUES (5, 'Contratado')
INSERT INTO ma_status VALUES (6, 'Cancelado')
GO