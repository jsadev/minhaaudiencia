/** Admin */

-- Usu�rio
CREATE TABLE dbo.ma_usuario (
	IDUsuario int not null identity not for replication primary key,
	Nome varchar(50) not null,
	Email varchar(100) not null,
	Senha varchar(50) not null,
	Ativo bit not null default 0,
	Locked bit not null default 0,
	DataInclusao datetime not null default getdate()
)
GO

INSERT INTO dbo.ma_usuario (Nome,Email,Senha,Ativo,Locked) VALUES ('Administrador','admin@admin.com','5B-42-E4-D7-7E-9B-E2-52-71-A6-BF-63-60-65-A7-FE',1,1)
GO