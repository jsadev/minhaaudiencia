﻿<%@ Page Title="Convites | Minha Audiència" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="convites.aspx.cs" Inherits="minhaaudiencia.apps_convites" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li class="active">Convites</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <!--<div class="page-header">
        <h1>Audiências</h1>
    </div>-->
    <div class="bg-filtros">
        <div class="col-xs-2">
            <!--Filtro Todos -->
            <div class="btn-group">
                <button type="button" class="btn btn-grey" onclick="listar({ })">Todos</button>
            </div>
        </div>
        <div class="col-xs-2">
            <!--Filtro Hoje -->
            <div class="btn-group">
                <button id="btnDays" data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Hoje <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="return days('semana')">Semana</a></li>
                    <li><a href="#" onclick="return days('ontem')">Ontem</a></li>
                    <li><a href="#" onclick="return days('hoje')">Hoje</a></li>
                    <li><a href="#" onclick="return days('amanha')">Amanhã</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2">
            <!--Filtro Ano -->
            <div class="btn-group">
                <button id="btnYears" data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><%=DateTime.Now.Year%> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul id="ddlAno" class="dropdown-menu">
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <!--Filtro Mês -->
            <div class="btn-group">
                <button id="btnMonths" data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><%=DateTime.Now.ToString("MMMM")%> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="return months(1)">Janeiro</a></li>
                    <li><a href="#" onclick="return months(2)">Fevereiro</a></li>
                    <li><a href="#" onclick="return months(3)">Março</a></li>
                    <li><a href="#" onclick="return months(4)">Abril</a></li>
                    <li><a href="#" onclick="return months(5)">Maio</a></li>
                    <li><a href="#" onclick="return months(6)">Junho</a></li>
                    <li><a href="#" onclick="return months(7)">Julho</a></li>
                    <li><a href="#" onclick="return months(8)">Agosto</a></li>
                    <li><a href="#" onclick="return months(9)">Setembro</a></li>
                    <li><a href="#" onclick="return months(10)">Outubro</a></li>
                    <li><a href="#" onclick="return months(11)">Novembro</a></li>
                    <li><a href="#" onclick="return months(12)">Dezembro</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2">
            <!-- Adicionar Agenda -->
            <button type="button" class="btn btn-info" title="Nova Audiência" onclick="window.location = '<%=ConfigurationManager.AppSettings["Url"]%>/nova-audiencia'"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div id="resultados">
    </div>

    <div class="hidden">
        <!-- Quando tem Convites -->
        <!-- Titulo, que deve exibir o nome do Filtro que foi selecionado -->
        <div class="col-xs-12">
            <h3>Hoje</h3>
        </div>

        <!-- Convite Passado, com Avaliação -->
        <div class="convites col-xs-12">
            <div class="col-xs-4">
                <div class="hora-passada">09:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud1">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv1">por Nome do Contratante</a></div>
                <ul class="aval">
                    <li><a href="" class="ico-curti-on"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                    <li><a href="" class="ico-recomendo-on"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                    <li class="aval-on">avaliado</li>
                </ul>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud1">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv1">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>


        <!-- Convite Passada, sem Aceite -->
        <div class="convites col-xs-12">
            <div class="col-xs-4">
                <div class="hora-passada">08:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud2">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv2">por Nome do Contratante</a></div>
                <ul class="aval">
                    <li><a href="" class="ico-curti-on">ESTOU DISPONÍVEL</a></li>
                    <li><a href="" class="ico-curti-on">INDISPONÍVEL</a></li>
                </ul>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud2">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv2">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>

        <!-- Convite Futuro, sem Aceite -->
        <div class="convites col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">08:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud3">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv3">por Nome do Contratante</a></div>
                <div class="col-xs-12">
                    <div class="contrata-pend">aguardando confirmação <i class="fa fa-circle" aria-hidden="true"></i></div>
                </div>
                <div class="col-xs-12">
                    <ul class="aval">
                        <li><a href="#modal-desist3" class="ico-curti-on">DESISTIR</a></li>
                        <li><a href="#modal-conf3" class="ico-curti-on">CONFIRMAR</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Confirmação -->
        <div class="modalDialog col-xs-12" id="modal-conf3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Confirme sua Contratação<br />
                    <span>Você foi selecionado para uma audiência, confirme sua contratação</span>
                </h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                <div class="col-xs-4"><a href="#modal-conf-aceite-ok3" class="close">ACEITAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Confirmação Sucesso -->
        <div class="modalDialog col-xs-12" id="modal-conf-aceite-ok3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <ul class="resumo-adv">
                    <li><big>ACEITE enviado com Sucesso.</big></li>
                    <li>Aguarde o Contato do Contratante.</li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
                <div class="col-xs-4"></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Desistência -->
        <div class="modalDialog col-xs-12" id="modal-desist3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <ul class="resumo-adv">
                    <li><big>Desistir</big></li>
                    <li>Informe o motivo de sua desistência:</li>
                    <li><textarea name="txtMotivo" placeholder="insira o motivo"></textarea></li>
                </ul>
                <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                <div class="col-xs-8"><a href="#modal-conf-desist3" class="close" onclick="return confDesistencia()">CONFIRMAR DESISTÊNCIA</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Confirmação da Desistência -->
        <div class="modalDialog col-xs-12" id="modal-conf-desist3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <ul class="resumo-adv">
                    <li><big>Tem certeza que deseja DESISTIR da AUDIÊNCIA?</big></li>
                </ul>
                <div class="col-xs-4"><a href="#close" class="close">CANCELAR</a></div>
                <div class="col-xs-8"><a href="#modal-conf-desist-ok3" class="close">CONFIRMAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Confirmação da Desistência Sucesso -->
        <div class="modalDialog col-xs-12" id="modal-conf-desist-ok3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <ul class="resumo-adv">
                    <li><big>DESISTÊNCIA confirmada com Sucesso.</big></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
                <div class="col-xs-4"></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>

        <!-- Convite Aberto, Aceito -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">16:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud3">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv3">por Nome do Contratante</a></div>
                <div class="contratado">aguardando confirmação <i class="fa fa-circle" aria-hidden="true"></i></div>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>

        <div class="col-xs-12">
            <h3>Amanhã</h3>
        </div>
        <!-- Convite Futuro, sem Aceite -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">08:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud4">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv4">por Nome do Contratante</a></div>
                <ul class="aval">
                    <li><a href="" class="ico-curti-on">ESTOU DISPONÍVEL</a></li>
                    <li><a href="" class="ico-curti-on">INDISPONÍVEL</a></li>
                </ul>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud4">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv4">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script type="text/javascript">
        var mes = <%=DateTime.Now.Month%>;
        var ano = <%=DateTime.Now.Year%>;
    </script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/config.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1000000, 9999999)%>"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/convites.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1000000, 9999999)%>"></script>
</asp:Content>