﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Linq;

    public partial class apps_agenda : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                MontarAudiencias();
        }

        public void MontarAudiencias()
        {
            List<Entity.Audiencia> audiencias = new Business.Audiencia().ListarAudienciaAgenda(advogado.IDAdvogado);
            audiencias = audiencias.Where(a => a.Status == -1).ToList();

            rptAudiencia.DataSource = audiencias;
            rptAudiencia.DataBind();
        }

        public string MontarAdvogados(object IDAudiencia, object Area_ID, object Cidade_ID, object Data, object Valor)
        {
            string html = "";

            List<Entity.Advogado> advogados = new Business.Advogado().ListarAdvogado(advogado.IDAdvogado, Area_ID, Cidade_ID, Convert.ToDateTime(Data), Convert.ToDecimal(Valor));
            foreach (Entity.Advogado adv in advogados)
            {
                html += "<div class='col-xs-12 adv-aceites'>\n";
                html += "<div class='col-xs-1'>\n";
                html += "<div class='checkbox'>\n";
                html += "<input type='checkbox' id='chkAdvogado-" + IDAudiencia + "-" + adv.IDAdvogado + "' name='chkAdvogado' value='" + adv.IDAdvogado + "' />\n";
                html += "<label for='chkAdvogado-" + IDAudiencia + "-" + adv.IDAdvogado + "'></label>\n";
                html += "</div>\n";
                html += "</div>\n";
                html += "<div class='col-xs-11'>\n";
                html += "<ul class='resumo-adv-oferta'>\n";
                html += "<li><big><a href='#modal-adv-" + IDAudiencia + "-" + adv.IDAdvogado + "'>" + adv.Nome + "</a></big></li>\n";
                html += "<li>Avaliação \n";
                html += "<i class='fa fa-thumbs-o-up' aria-hidden='true'></i>" + adv.Curtidas + "\n";
                html += "<i class='fa fa-hand-o-right' aria-hidden='true'></i>" + adv.Indicacoes + "\n";
                html += "</li>\n";
                html += "</ul>\n";
                html += "</div>\n";
                html += "</div>\n";
            }

            if (advogados.Count == 0)
            {
                html += "<div class='col-xs-12 adv-aceites'>\n";
                html += "<div class='col-xs-1'>\n";
                html += "<div class='checkbox'>\n";
                html += "</div>\n";
                html += "</div>\n";
                html += "<div class='col-xs-11'>\n";
                html += "<ul class='resumo-adv-oferta'>\n";
                html += "<li>SEU RESULTADO NÃO RETORNOU NENHUM ADVOGADO.</li>\n";
                html += "</ul>\n";
                html += "</div>\n";
                html += "</div>\n";
            }


            html += "<div class='col-xs-12'>&nbsp;</div>\n";
            html += "<div class='col-xs-12'>\n";
            html += "<div class='col-xs-4'></div>\n";
            html += "<div class='col-xs-4'><a href='#close' class='close'>CANCELAR</a></div>\n";
            if (advogados.Count > 0)
                html += "<div class='col-xs-4'><a href='#' class='close' onclick='return enviar()'>ENVIAR</a></div>\n";
            else
                html += "<div class='col-xs-4'><a href='/ofertas/" + IDAudiencia + "/alterar-audiencia' class='close'>EDITAR VALOR</a></div>\n";
            html += "</div>\n";

            return html;
        }

        public string MontarAdvogados2(object IDAudiencia, object Area_ID, object Cidade_ID, object Data, object Valor)
        {
            string html2 = "";

            List<Entity.Advogado> advogados = new Business.Advogado().ListarAdvogado(advogado.IDAdvogado, Area_ID, Cidade_ID, Convert.ToDateTime(Data), Convert.ToDecimal(Valor));
            foreach (Entity.Advogado adv in advogados)
            {
                html2 += "<div class='modalDialog col-xs-12' id='modal-adv-" + IDAudiencia + "-" + adv.IDAdvogado + "'>\n";
                html2 += "<div class='col-xs-12 col-sm-3 col-md-4'></div>\n";
                html2 += "<div class='col-xs-12 col-sm-6 col-md-4 pop-adv'>\n";
                html2 += "<h3>Dados do Advogado</h3>\n";
                html2 += "<ul class='resumo-adv'>\n";
                html2 += "<li><big>" + adv.Nome + "</big></li>\n";
                html2 += "<li>Avaliação \n";
                html2 += "<i class='fa fa-thumbs-o-up' aria-hidden='true'></i> " + adv.Curtidas + "\n";
                html2 += "<i class='fa fa-hand-o-right' aria-hidden='true'></i> " + adv.Indicacoes + "\n";
                html2 += "</li>\n";
                html2 += "<li>OAB: " + adv.Oab + "-" + adv.Seccional + "</li>\n";
                html2 += "<li><strong>Apresentação</strong></li>\n";
                html2 += "<li>" + adv.Apresentacao + "</li>\n";
                html2 += "</ul>\n";
                html2 += "<div class='col-xs-4'></div>\n";
                html2 += "<div class='col-xs-4'><a href='#ofertar" + IDAudiencia + "' class='close'>VOLTAR</a></div>\n";
                html2 += "<div class='col-xs-4'></div>\n";
                html2 += "</div>\n";
                html2 += "<div class='col-xs-12 col-sm-3 col-md-4'></div>\n";
                html2 += "</div>\n";
            }

            return html2;
        }
    }
}