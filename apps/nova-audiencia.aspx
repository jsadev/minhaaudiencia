﻿<%@ Page Title="Nova Audiência | Minha Audiência" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="nova-audiencia.aspx.cs" Inherits="minhaaudiencia.apps_nova_audiencia" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .pop-adv div a {
            float: none !important;
        }
    </style>
</asp:Content>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>Audiências</li>
    <li class="active"><%=Titulo%></li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1><%=Titulo%></h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nº do Processo</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtProcesso" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-processo"></asp:TextBox>
                        <asp:Label ID="lblProcessor" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Matéria da Audiência</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtMateria" runat="server" CssClass="col-xs-10 col-sm-5"></asp:TextBox>
                        <asp:Label ID="lblMateria" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Área de Atuação</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlArea" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" AutoPostBack="true" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lblArea" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Cidade</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlCidade" runat="server" CssClass="col-xs-10 col-sm-5" AutoPostBack="true" OnSelectedIndexChanged="ddlCidade_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lblCidade" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Fórum</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlForum" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" AutoPostBack="true" OnSelectedIndexChanged="ddlForum_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lblForum" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Vara</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlVara" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <asp:Label ID="lblVara" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="date-timepicker1">Data e Horário</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-clock-o bigger-110"></i></span>
                            <asp:TextBox ID="txtData" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-datetime"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblData" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Parte que Representa</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtParteRepresenta" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="100"></asp:TextBox>
                        <asp:Label ID="lblParteRepresenta" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Partes da Audiência</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlParteAudiencia" runat="server">
                            <asp:ListItem>Selecione</asp:ListItem>
                            <asp:ListItem>Autor</asp:ListItem>
                            <asp:ListItem>Réu</asp:ListItem>
                            <asp:ListItem>Terceiro</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblParteAudiencia" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Valor a Pagar</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtValor" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="R$ 0,00"></asp:TextBox>
                        <asp:Label ID="lblValor" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-11">Descrição (opcional)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtDescricao" runat="server" CssClass="autosize-transition form-control col-xs-10 col-sm-5" Rows="7" TextMode="MultiLine" placeholder="Informação que entende relevante para o Advogado"></asp:TextBox>
                        <asp:Label ID="lblDescricao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <h4 class="col-sm-3 control-label no-padding-right bolder blue">Convide os Advogados</h4>
                    <div class="col-sm-9">
                        <button id="btnAdvogado" runat="server" type="button" class="ColVis_Button ColVis_MasterButton btn btn-white btn-info btn-bold" onclick="location.href='#modal-adv'">
                            <span><i class="fa fa-search"></i></span>
                        </button>
                        <div id="advogados">
                        </div>
                    </div>
                </div>
                
                <!-- Modal Advogados -->
                <div class="modalDialog col-xs-12" id="modal-adv">
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                        <h3>Selecione os Advogados</h3>

                        <div class="col-xs-12 adv-aceites">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <input type="checkbox" id="chkAdvogadoTodos" />
                                    <label for="chkAdvogadoTodos">Selecionar todos</label>
                                </div>
                            </div>
                        </div>
                        <div id="advs">
                        </div>

                        <asp:Repeater ID="rptAdvogado" runat="server">
                            <ItemTemplate>
                                <div class="col-xs-12 adv-aceites">
                                    <div class="col-xs-1">
                                        <div class="checkbox">
                                            <input type="checkbox" id="chkAdvogado<%# Eval("IDAdvogado") %>" name="chkAdvogado" value="<%# Eval("IDAdvogado") %>" <%# Checked(Eval("IDAdvogado")) %> />
                                            <label for="chkAdvogado<%# Eval("IDAdvogado") %>"></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-11">
                                        <ul class="resumo-adv-oferta">
                                            <li><big><%# Eval("Nome") %></big></li>
                                            <li>Avaliação 
                                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                                                <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                            <div class="col-xs-4"><a href="#concluir" class="close">CONCLUIR</a></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                </div>



                <div class="modal-footer">
                    <button type="button" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i>Salvar</button>
                </div>

                <div class="modalDialog col-xs-12" id="modal-canc">
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                        <ul class="resumo-adv">
                            <li><big>Cancelar</big></li>
                            <li>Informe o motivo do cancelamento:</li>
                            <li>
                                <textarea name="txtMotivo" placeholder="insira o motivo"></textarea></li>
                        </ul>
                        <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                        <div class="col-xs-8"><a href="#modal-conf-canc" class="close">CONFIRMAR CANCELAMENTO</a></div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                </div>

                <div class="modalDialog col-xs-12" id="modal-conf-canc">
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                        <ul class="resumo-adv">
                            <li><big>Tem certeza que deseja CANCELAR a AUDIÊNCIA?</big></li>
                        </ul>
                        <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                        <div class="col-xs-8"><a href="#modal-conf-desist-ok" class="close" onclick="cancelar()">CONFIRMAR</a></div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                </div>

                <div class="modalDialog col-xs-12" id="modal-conf-desist-ok">
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                        <ul class="resumo-adv">
                            <li><big>CANCELAMENTO confirmado com Sucesso.</big></li>
                        </ul>
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
                        <div class="col-xs-4"></div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="adv-detail"></div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/config.js"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/jquery.maskedinput.min.js"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/jquery.maskMoney.min.js"></script>

    <script type="text/javascript">
        var json;

        jQuery(function () {
            $('.input-mask-processo').mask("9999999-99.9999.9.99.9999");
            $('.input-mask-datetime').mask("99/99/9999 99:99");
            $('.input-mask-money').maskMoney({ thousands: '.', decimal: ',' });

            $('#chkAdvogadoTodos').click(function () {
                var checked = $(this).prop('checked');
                $('input[name="chkAdvogado"]').each(function () {
                    $(this).prop("checked", checked);
                });
            });

            $.ajax({
                url: Url + '/api/advogados',
                data: { uid: <%=advogado.IDAdvogado%> },
                success: function (data) {
                    data = eval('(' + data + ')');

                    if (data.error) {
                        alert(data.message);
                        return;
                    }

                    json = data;

                    <% if (RouteData.Values["id"] != null) { %>
                    advs();
                    <% } %>
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    switch (xhr.status) {
                        case 404:
                            alert('Página não encontrada.');
                            break;
                        default:
                            alert(thrownError);
                            break;
                    }
                }
            });

            $('a[href="#concluir"]').click(function () {
                advs();
                return;
            });

            $('#conteudo_ddlArea, #conteudo_ddlCidade, #conteudo_txtData, #conteudo_txtValor').change(function() {
                carregarAdvogados();
            });

            <% if (RouteData.Values["id"] != null) { %>
            carregarAdvogados();
            <% } %>
        });

        function carregarAdvogados() {
            var area = $('#conteudo_ddlArea').val();
            var cidade = $('#conteudo_ddlCidade').val();
            var data = $('#conteudo_txtData').val();
            var valor = $('#conteudo_txtValor').val();

            $.ajax({
                url: Url + "/api/ofertas-advogados.aspx",
                data: { area : area, cidade : cidade, data : data, valor : valor, id : '<%=RouteData.Values["id"]%>' },
                success:function(d) {
                    d = eval('(' + d + ')');

                    if (d.error) {
                        alert(data.message);
                        return;
                    }

                    var html = '';
                    var html2 = '';
                    for(i = 0; i < d.total; i++) {
                        var advogado = d.advogados[i];

                        html += '<div class="col-xs-12 adv-aceites">\n';
                        html += '<div class="col-xs-1">\n';
                        html += '<div class="checkbox">\n';
                        html += '<input type="checkbox" id="chkAdvogado' + advogado.id + '" name="chkAdvogado" value="' + advogado.id + '" '+(advogado.marcado ? ' checked="checked"' : '')+' />\n';
                        html += '<label for="chkAdvogado' + advogado.id + '"></label>\n';
                        html += '</div>\n';
                        html += '</div>\n';
                        html += '<div class="col-xs-11">\n';
                        html += '<ul class="resumo-adv-oferta">\n';
                        html += '<li><big><a href="#modal-adv' + advogado.id + '">' + advogado.nome + '</a></big></li>\n';
                        html += '<li>Avaliação \n';
                        html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' + advogado.curtidas + '\n';
                        html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i>' + advogado.indicacoes + '\n';
                        html += '</li>\n';
                        html += '</ul>\n';
                        html += '</div>\n';
                        html += '</div>\n';

                        html2 += '<div class="modalDialog col-xs-12" id="modal-adv' + advogado.id + '">\n';
                        html2 += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html2 += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                        html2 += '<h3>Dados do Advogado</h3>\n';
                        html2 += '<ul class="resumo-adv">\n';
                        html2 += '<li><big>' + advogado.nome + '</big></li>\n';
                        html2 += '<li>Avaliação \n';
                        html2 += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                        html2 += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                        html2 += '</li>\n';
                        html2 += '<li>OAB: ' + advogado.oab + '-' + advogado.seccional + '</li>\n';
                        html2 += '<li><strong>Apresentação</strong></li>\n';
                        html2 += '<li>' + advogado.apresentacao + '</li>\n';
                        html2 += '</ul>\n';
                        html2 += '<div class="col-xs-4"></div>\n';
                        html2 += '<div class="col-xs-4"><a href="#modal-adv" class="close">VOLTAR</a></div>\n';
                        html2 += '<div class="col-xs-4"></div>\n';
                        html2 += '</div>\n';
                        html2 += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html2 += '</div>\n';
                    }

                    $('#advs').html(html);
                    $('#adv-detail').html(html2);

                    <% if (RouteData.Values["id"] != null) { %>
                    advs();
                    <% } %>
                },
                error: function(xhr,ajaxOptions,thrownError) {
                    switch (xhr.status) {
                        case 404:
                            alert('Página não encontrada.');
                            break;
                        default:
                            alert(thrownError);
                            break;
                    }
                }
            });
        }

        var advs = function() {
            $('#advogados').html('');
            var n = 0;

            $('input[name="chkAdvogado"]').each(function () {
                if($(this).prop("checked")) {
                    var sel = selectedAdv($(this).val());
                    n++;

                    var html = '<span class="badge badge-info badge-outline m-md p-lg">';
                    html += sel.nome + ' ';
                    html += '<input type="hidden" name="chkConvite" value="' + sel.id + '" />';
                    html += '</span>';

                    $('#advogados').append(html);
                }
            });

            if (n > 0)
                $('#conteudo_btnSalvar').html('<i class="ace-icon fa fa-check"></i>Enviar Convite');
            else
                $('#conteudo_btnSalvar').html('<i class="ace-icon fa fa-check"></i>Salvar');
        };

        var selectedAdv = function(id) {
            var selected;
            $.each(json.advogados, function (i, v) {
                if (v.id == id)
                    selected = v;
            });
            return selected;
        };

        function cancelar() {
            $.ajax({
                url: Url + '/api/ofertas/cancelar',
                data: { audiencia: '<%=RouteData.Values["id"]%>', motivo: $('textarea[name="txtMotivo"]').val() },
                success: function(data) {
                    data = eval('(' + data + ')');

                    if (data.error) {
                        alert(data.message);
                        return;
                    }

                    window.location = window.location.toString();
                },
                error:function(xhr,ajaxOptions, thrownError) {
                    switch (xhr.status) {
                        case 404:
                            alert('Página não encontrada.');
                            break;
                        default:
                            alert(thrownError);
                            break;
                    }
                }
            });
        }
    </script>
</asp:Content>