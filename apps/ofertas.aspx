﻿<%@ Page Title="Ofertas | Minha Audiência" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ofertas.aspx.cs" Inherits="minhaaudiencia.apps_ofertas" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <link href="<%=ConfigurationManager.AppSettings["Url"]%>/assets/css/jquery-ui-1.12.1.custom.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li class="active">Ofertas</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <!--<div class="page-header">
        <h1>Audiências</h1>
    </div>-->
    <div class="bg-filtros">
        <div class="col-xs-2">
            <!--Filtro Todos -->
            <div class="btn-group">
                <button type="button" class="btn btn-grey" onclick="listar({ })">Todos</button>
            </div>
        </div>
        <div class="col-xs-2">
            <!--Filtro Hoje -->
            <div class="btn-group">
                <button id="btnDays" data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Hoje <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="return days('semana')">Semana</a></li>
                    <li><a href="#" onclick="return days('ontem')">Ontem</a></li>
                    <li><a href="#" onclick="return days('hoje')">Hoje</a></li>
                    <li><a href="#" onclick="return days('amanha')">Amanhã</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2">
            <!--Filtro Ano -->
            <div class="btn-group">
                <button id="btnYears" data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><%=DateTime.Now.Year%> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul id="ddlAno" class="dropdown-menu">
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <!--Filtro Mês -->
            <div class="btn-group">
                <button id="btnMonths" data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><%=DateTime.Now.ToString("MMMM")%> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="return months(1)">Janeiro</a></li>
                    <li><a href="#" onclick="return months(2)">Fevereiro</a></li>
                    <li><a href="#" onclick="return months(3)">Março</a></li>
                    <li><a href="#" onclick="return months(4)">Abril</a></li>
                    <li><a href="#" onclick="return months(5)">Maio</a></li>
                    <li><a href="#" onclick="return months(6)">Junho</a></li>
                    <li><a href="#" onclick="return months(7)">Julho</a></li>
                    <li><a href="#" onclick="return months(8)">Agosto</a></li>
                    <li><a href="#" onclick="return months(9)">Setembro</a></li>
                    <li><a href="#" onclick="return months(10)">Outubro</a></li>
                    <li><a href="#" onclick="return months(11)">Novembro</a></li>
                    <li><a href="#" onclick="return months(12)">Dezembro</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2">
            <!-- Adicionar Audiência -->
            <button type="button" class="btn btn-info" title="Nova Audiência" onclick="window.location = '<%=ConfigurationManager.AppSettings["Url"]%>/nova-audiencia'"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div id="resultado">
    </div>

    <div class="hidden">
        <!-- Quando tem Audiências Cadastradas -->
        <!-- Titulo, que deve exibir o nome do Filtro que foi selecionado -->
        <div class="col-xs-12">
            <h3>Hoje</h3>
        </div>
        <!-- Oferta Passada, sem Avaliação -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-passada">08:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="../audiencias/registro.aspx">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv1">Nome do Advogado</a></div>
                <ul class="aval">
                    <li><a href="" class="ico-curti-off"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                    <li><a href="" class="ico-recomendo-off"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                    <li class="aval-off">avalie</li>
                </ul>
            </div>
        </div>
        <!-- Modal Advogado Contratado -->
        <div class="modalDialog col-xs-12" id="modal-adv1">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Advogado</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Advogado</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li><strong>Apresentação</strong></li>
                    <li>Parturient dolor mus tempor. Mus, magnis! Vel porta in, aenean egestas? Ac nunc magna vut nec et, integer dapibus sit scelerisque purus hac vel, duis.</li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                <div class="col-xs-4"><a href="#" class="close" style="display: none;">CONTRATAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Oferta Passada, com Avaliação -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-passada">10:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="../audiencias/registro.aspx">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv2">Nome do Advogado</a></div>
                <ul class="aval">
                    <li><a href="" class="ico-curti-on"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                    <li><a href="" class="ico-recomendo-on"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                    <li class="aval-on">avaliado</li>
                </ul>
            </div>
        </div>
        <!-- Modal Advogado Contratado -->
        <div class="modalDialog col-xs-12" id="modal-adv2">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Advogado</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Advogado</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li><strong>Apresentação</strong></li>
                    <li>Parturient dolor mus tempor. Mus, magnis! Vel porta in, aenean egestas? Ac nunc magna vut nec et, integer dapibus sit scelerisque purus hac vel, duis.</li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                <div class="col-xs-4"><a href="#" class="close" style="display: none;">CONTRATAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>

        <!-- Oferta Aberta, Contratação Pendente, Com Aceites -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">13:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv3">5 aceites</a></div>
                <div class="contrata-pend">Contratação Pendente <i class="fa fa-circle" aria-hidden="true"></i></div>
            </div>
        </div>
        <!-- Modal Advogados que Aceitaram a Oferta -->
        <div class="modalDialog col-xs-12" id="modal-adv3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Convites Aceitos</h3>
                <div class="col-xs-12">
                    <h4>Hoje</h4>
                </div>
                <div class="ofertas col-xs-12 adv-aceites">
                    <div class="col-xs-4">
                        <div class="hora-passada">08:20</div>
                    </div>
                    <div class="col-xs-8">
                        <div class="tit-item">Título da Audiência</div>
                        <div class="nat-item">Natureza</div>
                    </div>
                </div>
                <div class="col-xs-12 adv-aceites">
                    <div class="col-xs-1">
                        <input type="checkbox" />
                    </div>
                    <div class="col-xs-11">
                        <ul class="resumo-adv-oferta">
                            <li><big>Nome do Advogado</big></li>
                            <li>Avaliação 
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 adv-aceites">
                    <div class="col-xs-1">
                        <input type="checkbox" />
                    </div>
                    <div class="col-xs-11">
                        <ul class="resumo-adv-oferta">
                            <li><big>Nome do Advogado</big></li>
                            <li>Avaliação 
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 adv-aceites">
                    <div class="col-xs-1">
                        <input type="checkbox" />
                    </div>
                    <div class="col-xs-11">
                        <ul class="resumo-adv-oferta">
                            <li><big>Nome do Advogado</big></li>
                            <li>Avaliação 
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 adv-aceites">
                    <div class="col-xs-1">
                        <input type="checkbox" />
                    </div>
                    <div class="col-xs-11">
                        <ul class="resumo-adv-oferta">
                            <li><big>Nome do Advogado</big></li>
                            <li>Avaliação 
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 adv-aceites">
                    <div class="col-xs-1">
                        <input type="checkbox" />
                    </div>
                    <div class="col-xs-11">
                        <ul class="resumo-adv-oferta">
                            <li><big>Nome do Advogado</big></li>
                            <li>Avaliação 
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                            <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="col-xs-12">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                    <div class="col-xs-4"><a href="#" class="close">CONTRATAR</a></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>

        <!-- Oferta Aberta, Contratação Pendente, Sem Aceites -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">15:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="../audiencias/registro.aspx">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#">0 aceites</a></div>
                <div class="aguard-resp">Aguardando Respostas <i class="fa fa-circle" aria-hidden="true"></i></div>
            </div>
        </div>

        <!-- Oferta Fechada, Contratado -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">16:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="../audiencias/registro.aspx">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv">Nome do Advogado</a></div>
                <div class="contratado">Contratado <i class="fa fa-circle" aria-hidden="true"></i></div>
            </div>
        </div>
        <div class="modalDialog col-xs-12" id="modal-adv">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Advogado</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Advogado</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li><strong>Apresentação</strong></li>
                    <li>Parturient dolor mus tempor. Mus, magnis! Vel porta in, aenean egestas? Ac nunc magna vut nec et, integer dapibus sit scelerisque purus hac vel, duis.</li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                <div class="col-xs-4"></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script type="text/javascript">
        var mes = <%=DateTime.Now.Month%>;
        var ano = <%=DateTime.Now.Year%>;
    </script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/config.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1000000, 9999999)%>"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/ofertas.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1000000, 9999999)%>"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/jquery-ui-1.12.1.custom.min.js"></script>
    <script>
        $(function () {
            $(document).tooltip({
                items: "img, [data-value], [title]",
                content: function () {
                    var element = $(this);
                    if (element.is("[data-value]")) {
                        var text = element.text();
                        var value = element.attr("data-value");
                        return "<p>" + value + "</p>";
                    }
                },
                position: {
                    my: "right top",
                    at: "right+5 bottom+5",
                    collision: "none"
                }
            });
        });
  </script>
</asp:Content>