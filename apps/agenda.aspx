﻿<%@ Page Title="Agenda | Minha Audiência" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="agenda.aspx.cs" Inherits="minhaaudiencia.apps_agenda" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <link href="<%=ConfigurationManager.AppSettings["Url"]%>/assets/css/jquery-ui-1.12.1.custom.min.css" rel="stylesheet" />
    <style type="text/css">
        .pop-adv div a {
            float: none !important;
        }
    </style>
</asp:Content>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li class="active">Agenda</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <div class="bg-filtros">
        <div class="col-xs-2">
            <!--Filtro Todos -->
            <div class="btn-group">
                <button type="button" class="btn btn-grey" onclick="listar({ })">Todos</button>
            </div>
        </div>
        <div class="col-xs-2">
            <!--Filtro Hoje -->
            <div class="btn-group">
                <button id="btnDays" data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Hoje <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="return days('semana')">Semana</a></li>
                    <li><a href="#" onclick="return days('ontem')">Ontem</a></li>
                    <li><a href="#" onclick="return days('hoje')">Hoje</a></li>
                    <li><a href="#" onclick="return days('amanha')">Amanhã</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2">
            <!--Filtro Ano -->
            <div class="btn-group">
                <button id="btnYears" data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><%=DateTime.Now.Year%> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul id="ddlAno" class="dropdown-menu">
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <!--Filtro Mês -->
            <div class="btn-group">
                <button id="btnMonths" data-toggle="dropdown" class="btn btn-grey dropdown-toggle"><%=DateTime.Now.ToString("MMMM")%> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="return months(1)">Janeiro</a></li>
                    <li><a href="#" onclick="return months(2)">Fevereiro</a></li>
                    <li><a href="#" onclick="return months(3)">Março</a></li>
                    <li><a href="#" onclick="return months(4)">Abril</a></li>
                    <li><a href="#" onclick="return months(5)">Maio</a></li>
                    <li><a href="#" onclick="return months(6)">Junho</a></li>
                    <li><a href="#" onclick="return months(7)">Julho</a></li>
                    <li><a href="#" onclick="return months(8)">Agosto</a></li>
                    <li><a href="#" onclick="return months(9)">Setembro</a></li>
                    <li><a href="#" onclick="return months(10)">Outubro</a></li>
                    <li><a href="#" onclick="return months(11)">Novembro</a></li>
                    <li><a href="#" onclick="return months(12)">Dezembro</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-2">
            <!-- Adicionar Agenda -->
            <button type="button" class="btn btn-info" title="Nova Audiência" onclick="window.location = '<%=ConfigurationManager.AppSettings["Url"]%>/nova-audiencia'"><i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div id="resultados">
    </div>

    <!-- Modal Advogados -->
    <asp:Repeater ID="rptAudiencia" runat="server">
        <ItemTemplate>
            <div class="modalDialog col-xs-12" id="ofertar<%# Eval("IDAudiencia") %>">
                <div class="col-xs-12 col-sm-3 col-md-4"></div>
                <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                    <h3>Selecione os Advogados</h3>

                    <div class="col-xs-12 adv-aceites">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <input type="checkbox" id="chkAdvogadoTodos" />
                                <label for="chkAdvogadoTodos">Selecionar todos</label>
                            </div>
                        </div>
                    </div>

                    <%# MontarAdvogados(Eval("IDAudiencia"), Eval("Area_ID"), Eval("Cidade_ID"), Eval("Data"), Eval("Valor")) %>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-4"></div>
            </div>

            <div id="adv-detail">
                <%# MontarAdvogados2(Eval("IDAudiencia"), Eval("Area_ID"), Eval("Cidade_ID"), Eval("Data"), Eval("Valor")) %>
            </div>
        </ItemTemplate>
    </asp:Repeater>

    <div class="hidden">
        <!-- Quando tem Agenda Cadastrada -->
        <!-- Titulo, que deve exibir o nome do Filtro que foi selecionado -->
        <div class="col-xs-12">
            <h3>Hoje</h3>
        </div>
        <!-- Agenda Passada, sem Avaliação -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-passada">08:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud1">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv1">por Nome do Contratante</a></div>
                <ul class="aval">
                    <li><a href="#" class="ico-curti-off"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="ico-recomendo-off"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                    <li class="aval-off">avalie</li>
                </ul>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud1">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv1">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        
        <!-- Agenda Passada, com Avaliação -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-passada">10:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud2">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv2">por Nome do Contratante</a></div>
                <ul class="aval">
                    <li><a href="" class="ico-curti-on"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>
                    <li><a href="" class="ico-recomendo-on"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>
                    <li class="aval-on">avaliado</li>
                </ul>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud2">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv2">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>

        <!-- Agenda Fechada, Contratado -->
        <div class="ofertas col-xs-12">
            <div class="col-xs-4">
                <div class="hora-futura">16:20</div>
            </div>
            <div class="col-xs-8">
                <div class="tit-item"><a href="#modal-aud3">Título da Audiência</a></div>
                <div class="nat-item">Natureza</div>
                <div class="adv-item"><a href="#modal-adv3">por Nome do Contratante</a></div>
                <div class="contratado">Contratado <i class="fa fa-circle" aria-hidden="true"></i></div>
            </div>
        </div>
        <!-- Modal Audiência -->
        <div class="modalDialog col-xs-12" id="modal-aud3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Detalhes da Audiência</h3>
                <ul class="resumo-adv">
                    <li><big>Título da Audiência</big></li>
                    <li>Natureza</li>
                    <li>Comarca: <strong>XXX</strong></li>
                    <li>Fórum: <strong>XXX</strong></li>
                    <li>Parte que Representa: <strong>XXX</strong></li>
                    <li>Partes da Audiência: <strong>XXX</strong></li>
                    <li>Valor a Receber: <strong>R$ 0,00</strong></li>
                    <li>Descrição: <strong>Informação que entende ser relevante para o Advogado</strong></li>
                    <li>&nbsp;</li>
                    <li>Local: <strong>Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</strong></li>
                </ul>
                <div class="data-hora">Data e Horário: 17/12/2016 às 13:00hs</div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
        <!-- Modal Contratante -->
        <div class="modalDialog col-xs-12" id="modal-adv3">
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                <h3>Dados do Contratante</h3>
                <ul class="resumo-adv">
                    <li><big>Nome do Contratante</big></li>
                    <li>Avaliação 
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                    </li>
                    <li>OAB: 111111-SP</li>
                    <li>Endereço: Rua Heinz Heirich, 123 - AP 121 - Vila Olímpia - SP</li>
                    <li>Telefone: <a href="tel:+551199880099">(11) 9988.0099</a></li>
                    <li>Celular: <a href="tel:+551199880099">(11) 9988.0099</a> com WhatsApp</li>
                    <li>Email: <a href="mailto:email@email.com">email@email.com</a></li>
                    <li>Site: <a href="http://www.advogado.com.br" target="_blank">www.advogado.com.br</a></li>
                </ul>
                <div class="col-xs-4"></div>
                <div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-4"></div>
        </div>
    </div>

    <div class="modalDialog col-xs-12" id="modal-canc">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <ul class="resumo-adv">
                <li><big>Cancelar</big></li>
                <li>Informe o motivo do cancelamento:</li>
                <li><textarea name="txtMotivo" placeholder="insira o motivo"></textarea></li>
            </ul>
            <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
            <div class="col-xs-8"><a href="#modal-conf-canc" class="close" onclick="return confCancelar()">CONFIRMAR CANCELAMENTO</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>

    <div class="modalDialog col-xs-12" id="modal-conf-canc">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <ul class="resumo-adv">
                <li><big>Tem certeza que deseja CANCELAR a AUDIÊNCIA?</big></li>
            </ul>
            <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
            <div class="col-xs-8"><a href="#modal-conf-desist-ok" class="close" onclick="cancelando()">CONFIRMAR</a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>

    <div class="modalDialog col-xs-12" id="modal-conf-desist-ok">
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
        <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
            <ul class="resumo-adv">
                <li><big>CANCELAMENTO confirmado com Sucesso.</big></li>
            </ul>
            <div class="col-xs-4"></div>
            <div class="col-xs-4"><a href="<%=Request.Url.AbsoluteUri%>" class="close">FECHAR</a></div>
            <div class="col-xs-4"></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-4"></div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script type="text/javascript">
        var mes = <%=DateTime.Now.Month%>;
        var ano = <%=DateTime.Now.Year%>;
    </script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/config.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1000000, 9999999)%>"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/agenda.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1000000, 9999999)%>"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/jquery-ui-1.12.1.custom.min.js"></script>
    <script>
        $(function () {
            $(document).tooltip({
                items: "img, [data-value], [title]",
                content: function () {
                    var element = $(this);
                    if (element.is("[data-value]")) {
                        var text = element.text();
                        var value = element.attr("data-value");
                        return "<p>" + value + "</p>";
                    }
                },
                position: {
                    my: "right top",
                    at: "right+5 bottom+5",
                    collision: "none"
                }
            });
        });
  </script>
</asp:Content>