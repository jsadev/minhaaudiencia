﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using System.Drawing;

    public partial class apps_alterar_audiencia : BasePage
    {
        private DateTime minDate = DateTime.MinValue;
        private decimal minDecimal = decimal.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MontarAreas();
                MontarCidades();
                MontarAdvogados();

                if (Request["uid"] != null)
                    PreencherCampos();
            }
        }

        private void MontarAreas()
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add("Área de Atuação");
            new Admin.Area().ListarArea().ForEach(delegate (Entity.Area area)
            {
                ddlArea.Items.Add(new ListItem(area.Nome, area.IDArea.ToString()));
            });
        }

        private void MontarCidades()
        {
            ddlCidade.Items.Clear();
            ddlCidade.Items.Add("Selecione a Cidade");
            new Admin.Cidade().ListarCidade().ForEach(delegate (Entity.Cidade cidade)
            {
                ddlCidade.Items.Add(new ListItem(cidade.Nome, cidade.IDCidade.ToString()));
            });
        }

        private void MontarForuns()
        {
            ddlForum.Items.Clear();
            ddlForum.Items.Add("Fórum");
            new Admin.Forum().ListarForumCidade(ddlArea.SelectedValue, ddlCidade.SelectedValue).ForEach(delegate (Entity.Forum forum)
            {
                ddlForum.Items.Add(new ListItem(forum.Nome, forum.IDForum.ToString()));
            });
        }

        private void MontarVaras()
        {
            ddlVara.Items.Clear();
            ddlVara.Items.Add("Vara");
            new Admin.Vara().ListarVara(ddlForum.SelectedValue).ForEach(delegate (Entity.Vara vara)
            {
                ddlVara.Items.Add(new ListItem(vara.Nome, vara.IDVara.ToString()));
            });
        }

        private void MontarAdvogados()
        {
            rptAdvogado.DataSource = new Business.Advogado().ListarAdvogado(advogado.IDAdvogado);
            rptAdvogado.DataBind();
        }

        public void Migalha()
        {
            if (Request["uid"] == null)
                Response.Write("Nova Audiência");
            else
                Response.Write("Alterar Audiência");
        }

        private void PreencherCampos()
        {
            Entity.Audiencia audiencia = new Admin.Audiencia().ConsultarAudiencia(Request["uid"]);

            txtMateria.Text = audiencia.Materia;
            ddlArea.Items.FindByValue(audiencia.Area_ID.ToString()).Selected = true;
            ddlCidade.Items.FindByValue(audiencia.Cidade_ID.ToString()).Selected = true;
            ddlForum.Items.FindByValue(audiencia.Forum_ID.ToString()).Selected = true;
            ddlVara.Items.FindByValue(audiencia.Vara_ID.ToString()).Selected = true;
            txtData.Text = audiencia.Data.ToString("dd/MM/yyyy HH:mm");
            txtParteRepresenta.Text = audiencia.ParteRepresenta;
            ddlParteAudiencia.Items.FindByValue(audiencia.ParteAudiencia).Selected = true;
            txtValor.Text = audiencia.Valor.ToString("n2");
            txtDescricao.Text = audiencia.Descricao;

            Response.Write(">> " + (audiencia.Data - DateTime.Now).Hours);
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Materia = txtMateria.Text.ToPrepare();
            string Area_ID = ddlArea.SelectedValue;
            string Cidade_ID = ddlCidade.SelectedValue;
            string Forum_ID = ddlForum.SelectedValue;
            string Vara_ID = ddlVara.SelectedValue;
            string Data = txtData.Text.ToPrepare();
            string ParteRepresenta = txtParteRepresenta.Text.ToPrepare();
            string ParteAudiencia = ddlParteAudiencia.SelectedValue;
            string Valor = txtValor.Text.ToPrepare();
            string Descricao = txtDescricao.Text.ToPrepare();
            bool Validar = true;

            LimparLabel();

            if (!DateTime.TryParse(Data, out minDate))
            {
                Validar = false;
                lblData.Text = "- data inválida";
                txtData.BackColor = Color.LightYellow;
            }
            if (Data.isFilled() && Convert.ToDateTime(Data) < DateTime.Now)
            {
                Validar = false;
                lblData.Text = "- data deve ser futura";
                txtData.BackColor = Color.LightYellow;
            }
            if (ddlParteAudiencia.SelectedIndex == 0)
            {
                Validar = false;
                lblParteAudiencia.Text = "- campo obrigatório";
                ddlParteAudiencia.BackColor = Color.LightYellow;
            }
            if (!Area_ID.isFilled())
            {
                Validar = false;
                lblArea.Text = "- campo obrigatório";
                ddlArea.BackColor = Color.LightYellow;
            }
            if (!Cidade_ID.isFilled())
            {
                Validar = false;
                lblCidade.Text = "- campo obrigatório";
                ddlCidade.BackColor = Color.LightYellow;
            }
            if (!Forum_ID.isFilled())
            {
                Validar = false;
                lblForum.Text = "- campo obrigatório";
                ddlForum.BackColor = Color.LightYellow;
            }
            if (!Vara_ID.isFilled())
            {
                Validar = false;
                lblVara.Text = "- campo obrigatório";
                ddlVara.BackColor = Color.LightYellow;
            }
            if (!Valor.isFilled())
            {
                Validar = false;
                lblValor.Text = "- campo obrigatório";
                txtValor.BackColor = Color.LightYellow;
            }
            if (Valor.isFilled() && !decimal.TryParse(Valor, out minDecimal))
            {
                Validar = false;
                lblValor.Text = "- valor inválido";
                txtValor.BackColor = Color.LightYellow;
            }

            if (Validar)
            {
                Entity.Audiencia audiencia = new Entity.Audiencia();
                audiencia.Advogado_ID = advogado.IDAdvogado;
                audiencia.Materia = Materia;
                audiencia.Area_ID = Convert.ToInt32(Area_ID);
                audiencia.Cidade_ID = Convert.ToInt32(Cidade_ID);
                audiencia.Forum_ID = Convert.ToInt32(Forum_ID);
                audiencia.Vara_ID = Convert.ToInt32(Vara_ID);
                audiencia.Data = Convert.ToDateTime(Data);
                audiencia.ParteRepresenta = ParteRepresenta;
                audiencia.ParteAudiencia = ParteAudiencia;
                audiencia.Valor = Convert.ToDecimal(Valor);
                audiencia.Descricao = Descricao;

                Entity.Retorno ret = new Business.Audiencia().InserirAudiencia(audiencia);
                if (!ret.Status)
                {
                    Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                    return;
                }

                // envia convite
                audiencia.IDAudiencia = ret.Identity;
                if (Request.Form["chkAdvogado"] != null)
                {
                    string[] Advogados = Request.Form["chkAdvogado"].Split(',');
                    foreach (string IDAdvogado in Advogados)
                    {
                        Entity.Audiencia.Convite convite = new Entity.Audiencia.Convite();
                        convite.Audiencia_ID = audiencia.IDAudiencia;
                        convite.Advogado_ID = Convert.ToInt32(IDAdvogado);
                        convite.Status_ID = 0;

                        ret = new Business.Audiencia.Convite().InserirConvite(convite);
                        if (!ret.Status)
                        {
                            Mensagem(pnlError, ico, labMsg, STATUS.ERROR, ret.Erro);
                            return;
                        }

                        Entity.Advogado convidado = new Business.Advogado().ConsultarAdvogado(Convert.ToInt32(IDAdvogado));

                        Util.Email oEmail = new Util.Email(convidado.Email, "", "Convite para nova audiência", Util.Email.METODO.HTML);
                        oEmail.AudienciaConvite(convidado);
                        oEmail.Enviar();
                    }
                }

                Mensagem(pnlError, ico, labMsg, STATUS.SUCCESS, "Audiência cadastrada com sucesso.");
                LimparCampos();
            }
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex > 0 && ddlCidade.SelectedIndex > 0)
                MontarForuns();
        }

        protected void ddlCidade_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex > 0 && ddlCidade.SelectedIndex > 0)
                MontarForuns();
        }

        protected void ddlForum_SelectedIndexChanged(object sender, EventArgs e)
        {
            MontarVaras();
        }
    }
}