﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alterar-audiencia.aspx.cs" Inherits="minhaaudiencia.apps_alterar_audiencia" %>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li>Audiências</li>
    <li class="active">Cadastro de Audiência</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button>
        <i id="ico" runat="server"></i>
        <asp:Label ID="labMsg" runat="server"></asp:Label>
        <br />
    </asp:Panel>

    <div class="page-header">
        <h1>Cadastro de Audiência</h1>
    </div>
    <!-- /.page-header -->
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="form-horizontal">
                <!-- #section:elements.form -->
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Matéria da Audiência</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtMateria" runat="server" CssClass="col-xs-10 col-sm-5"></asp:TextBox>
                        <asp:Label ID="lblMateria" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Área de Atuação</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlArea" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" AutoPostBack="true" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lblArea" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Cidade</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlCidade" runat="server" CssClass="col-xs-10 col-sm-5" AutoPostBack="true" OnSelectedIndexChanged="ddlCidade_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lblCidade" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Fórum</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlForum" runat="server" CssClass="chosen-select col-xs-10 col-sm-5" AutoPostBack="true" OnSelectedIndexChanged="ddlForum_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lblForum" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Vara</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlVara" runat="server" CssClass="chosen-select col-xs-10 col-sm-5"></asp:DropDownList>
                        <asp:Label ID="lblVara" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="date-timepicker1">Data e Horário</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-clock-o bigger-110"></i></span>
                            <asp:TextBox ID="txtData" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-datetime"></asp:TextBox>
                        </div>
                        <asp:Label ID="lblData" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Parte que Representa</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtParteRepresenta" runat="server" CssClass="col-xs-10 col-sm-5" MaxLength="100"></asp:TextBox>
                        <asp:Label ID="lblParteRepresenta" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Partes da Audiência</label>
                    <div class="col-sm-9">
                        <asp:DropDownList ID="ddlParteAudiencia" runat="server">
                            <asp:ListItem>Selecione</asp:ListItem>
                            <asp:ListItem>Autor</asp:ListItem>
                            <asp:ListItem>Réu</asp:ListItem>
                            <asp:ListItem>Terceiro</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="lblParteAudiencia" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Valor a Pagar</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtValor" runat="server" CssClass="col-xs-10 col-sm-5 input-mask-money" placeholder="R$ 0,00"></asp:TextBox>
                        <asp:Label ID="lblValor" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-11">Descrição (opcional)</label>
                    <div class="col-sm-9">
                        <asp:TextBox ID="txtDescricao" runat="server" CssClass="autosize-transition form-control col-xs-10 col-sm-5" Rows="7" TextMode="MultiLine" placeholder="Informação que entende relevante para o Advogado"></asp:TextBox>
                        <asp:Label ID="lblDescricao" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <h4 class="col-sm-3 control-label no-padding-right bolder blue">Convide os Advogados</h4>
                    <button type="button" style="margin-left: 12px;" class="ColVis_Button ColVis_MasterButton btn btn-white btn-info btn-bold" onclick="location.href='#modal-adv'">
                        <span><i class="fa fa-search"></i></span>
                    </button>
                </div>
                <!-- Modal Advogados -->
                <div class="modalDialog col-xs-12" id="modal-adv">
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4 pop-adv">
                        <h3>Selecione os Advogados</h3>

                        <div class="col-xs-12 adv-aceites">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <input type="checkbox" id="chkAdvogadoTodos" />
                                    <label for="chkAdvogadoTodos">Selecionar todos</label>
                                </div>
                            </div>
                        </div>

                        <asp:Repeater ID="rptAdvogado" runat="server">
                            <ItemTemplate>
                                <div class="col-xs-12 adv-aceites">
                                    <div class="col-xs-1">
                                        <div class="checkbox">
                                            <input type="checkbox" id="chkAdvogado<%# Eval("IDAdvogado") %>" name="chkAdvogado" value="<%# Eval("IDAdvogado") %>" />
                                            <label for="chkAdvogado<%# Eval("IDAdvogado") %>"></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-11">
                                        <ul class="resumo-adv-oferta">
                                            <li><big><%# Eval("Nome") %></big></li>
                                            <li>Avaliação 
                                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2
                                                <i class="fa fa-hand-o-right" aria-hidden="true"></i>2
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>
                            <div class="col-xs-4"><a href="#" class="close" runat="server" onserverclick="btnSalvar_ServerClick">ENVIAR CONVITE</a></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-4"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onserverclick="btnSalvar_ServerClick"><i class="ace-icon fa fa-check"></i>Salvar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/jquery.maskedinput.min.js"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/jquery.maskMoney.min.js"></script>

    <script type="text/javascript">
        jQuery(function () {
            $('.input-mask-datetime').mask("99/99/9999 99:99");
            $('.input-mask-money').maskMoney({ thousands: '.', decimal: ',' });

            $('#chkAdvogadoTodos').click(function () {
                var checked = $(this).prop('checked');
                $('input[name="chkAdvogado"]').each(function () {
                    $(this).prop("checked", checked);
                });
            });
        });
    </script>
</asp:Content>