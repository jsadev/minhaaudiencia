﻿<%@ Page Title="Advogados | Minha Audiência" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="advogados.aspx.cs" Inherits="minhaaudiencia.apps_advogados" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
</asp:Content>

<asp:Content ID="migalha" runat="server" ContentPlaceHolderID="migalha">
    <li class="active">Advogados</li>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <!--<div class="page-header">
        <h1>Audiências</h1>
    </div>-->
    <div class="bg-filtros">
        <div class="col-xs-4">
            <!--Filtro Fóruns -->
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Fóruns <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Fórum 1</a></li>
                    <li><a href="#">Fórum 2</a></li>
                    <li><a href="#">Fórum 3</a></li>
                    <li><a href="#">Fórum 4</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <!--Filtro Avaliação -->
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-grey dropdown-toggle">Avaliação <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Maior</a></li>
                    <li><a href="#">Menor</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4"></div>
    </div>

    <div class="col-xs-12">&nbsp;</div>

    <div class="col-xs-12">
        <div class="col-xs-6"></div>
        <div class="col-xs-6">avaliação</div>
    </div>

    <!-- Lista de Advogados -->
    <div id="resultado"></div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/config.js"></script>
    <script src="<%=ConfigurationManager.AppSettings["Url"]%>/assets/js/interno/advogados.js"></script>
</asp:Content>