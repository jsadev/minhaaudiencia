﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using System.Net.Mail;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace minhaaudiencia.Util
{
    public class Email
    {
        public enum METODO { HTML, TEXT };

        private string Destinatario;
        private string Copia;
        private string Assunto;
        private StringBuilder Corpo;
        private METODO Metodo;
        private List<Attachment> attachs;

        private bool Teste = true;
        private string Url = "http://minhaaudiencia.testedigital.com.br";

        public Email() { }

        /// <summary>
        /// Dispara o e-mail utilizando um modelo pré-formatado
        /// </summary>
        /// <param name="Destinatario">Endereço de e-mail do destinatário.</param>
        /// <param name="Copia">Endereço de e-mail de quem vai receber uma cópia da mensagem.</param>
        /// <param name="Assunto">Assunto do e-mail.</param>
        /// <param name="Metodo">Método de envio. HTML ou TEXTO.</param>
        public Email(string Destinatario, string Copia, string Assunto, METODO Metodo)
        {
            this.Destinatario = Destinatario;
            this.Copia = Copia;
            this.Assunto = Assunto;
            this.Metodo = Metodo;
        }

        /// <summary>
        /// Dispara o e-mail utilizando um modelo dinâmico
        /// </summary>
        /// <param name="Destinatario">Endereço de e-mail do destinatário.</param>
        /// <param name="Copia">Endereço de e-mail de quem vai receber uma cópia da mensagem.</param>
        /// <param name="Assunto">Assunto do e-mail.</param>
        /// <param name="Mensagem">Corpo do e-mail.</param>
        /// <param name="Metodo">Método de envio. HTML ou TEXTO.</param>
        public Email(string Destinatario, string Copia, string Assunto, string Mensagem, METODO Metodo)
        {
            this.Corpo = new StringBuilder();

            this.Destinatario = Destinatario;
            this.Copia = Copia;
            this.Assunto = Assunto;
            this.Corpo.Append(Mensagem);
            this.Metodo = Metodo;
        }

        /// <summary>
        /// Anexa um arquivo ao e-mail
        /// </summary>
        /// <param name="Arquivo"></param>
        /// <returns></returns>
        public Email Anexar(string Arquivo)
        {
            attachs = new List<Attachment>();
            attachs.Add(new Attachment(Arquivo));
            return this;
        }

        /// <summary>
        /// Anexa vários arquivos ao e-mail
        /// </summary>
        /// <param name="Arquivos"></param>
        /// <returns></returns>
        public Email Anexar(string[] Arquivos)
        {
            attachs = new List<Attachment>();
            foreach (string Arquivo in Arquivos)
                attachs.Add(new Attachment(Arquivo));
            return this;
        }

        /// <summary>
        /// Envia o e-mail
        /// </summary>
        public void Enviar()
        {
            MailMessage oEmail = new MailMessage();
            oEmail.From = new MailAddress("naoresponder@minhaaudiencia.com.br", "Minha Audiência");
            oEmail.Priority = MailPriority.Normal;
            oEmail.IsBodyHtml = (Metodo == METODO.HTML);
            oEmail.Subject = Assunto;
            oEmail.Body = Corpo.ToString();

            SmtpClient oEnviar = new SmtpClient();
            oEnviar.Timeout = 30000;

            if (Teste)
            {
                oEmail.To.Add(Destinatario);
                oEnviar.Host = "smtp.gmail.com"; //DIGITE AQUI O NOME DO SERVIDOR DE SMTP QUE VOCÊ IRA UTILIZAR
                oEnviar.Credentials = new System.Net.NetworkCredential("janailson.silva@gmail.com", "elibclgaumppmato");
                oEnviar.EnableSsl = true;

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            }
            else
            {
                oEmail.To.Add(Destinatario);
                if (!String.IsNullOrEmpty(Copia)) oEmail.Bcc.Add(Copia);

                oEnviar.Host = "smtp.minhaaudiencia.com.br"; //DIGITE AQUI O NOME DO SERVIDOR DE SMTP QUE VOCÊ IRA UTILIZAR
                oEnviar.EnableSsl = false;
            }

            if (attachs != null)
            {
                foreach (Attachment attach in attachs)
                    oEmail.Attachments.Add(attach);
            }

            oEnviar.Send(oEmail);
            oEmail.Dispose();
        }

        /// <summary>
        /// Modelo de e-mail para recuperação de senha
        /// </summary>
        /// <param name="Guid"></param>
        /// <returns></returns>
        public Email LembrarSenha(string Guid)
        {
            Corpo = new StringBuilder();
            Corpo.Append("<p>Você solicitou a redifinação de senha.</p>\n");
            Corpo.Append("<p>Clique no link abaixo para redefinir ou copie e cole o link no navegador.<br /><a href='" + Url + "/recuperar-senha?h=" + Guid + "' target='_blank'>" + Url + "/recuperar-senha?h=" + Guid + "</a></p>\n");

            return this;
        }

        /// <summary>
        /// Modelo de e-mail para convite de audiência
        /// </summary>
        /// <param name="advogado"></param>
        /// <returns></returns>
        public Email AudienciaConvite(Entity.Advogado advogado)
        {
            Corpo = new StringBuilder();
            Corpo.Append("<p>Olá,</p>\n");
            Corpo.Append("<p>Você recebeu um convite para uma nova audiência.<br />\n");
            Corpo.Append("Clique no link abaixo para visualizar os convite(s) recebido(s) ou copie e cole o link no navegador.</p>\n");
            Corpo.Append("<p><a href='" + Url + "/convites' target='_blank'>" + Url + "/convites</a></p>\n");
            Corpo.Append("<p>--<br />\n");
            Corpo.Append("Atenciosamente,<br />\n");
            Corpo.Append("Equipe Minha Audiência</p>\n");

            return this;
        }

        /// <summary>
        /// Modelo de e-mail para indisponibilidade para a audiência
        /// </summary>
        /// <param name="advogado"></param>
        /// <returns></returns>
        public Email ConviteIndisponivel(Entity.Advogado advogado)
        {
            Corpo = new StringBuilder();
            Corpo.Append("<p>Convite, indisponível.</p>\n");

            return this;
        }

        /// <summary>
        /// Modelo de e-mail para disponibilidade para a audiência
        /// </summary>
        /// <param name="advogado"></param>
        /// <returns></returns>
        public Email ConviteDisponivel(Entity.Advogado advogado)
        {
            Corpo = new StringBuilder();
            Corpo.Append("<p>Olá,</p>\n");
            Corpo.Append("<p>O advogado que convidou esta disponível!<br />\n");
            Corpo.Append("Clique no link abaixo para efetuar a contratação ou copie e cole o link no navegador.</p>\n");
            Corpo.Append("<p><a href='" + Url + "/agenda' target='_blank'>" + Url + "/agenda</a></p>\n");
            Corpo.Append("<p>--<br />\n");
            Corpo.Append("Atenciosamente,<br />\n");
            Corpo.Append("Equipe Minha Audiência</p>\n");

            return this;
        }

        /// <summary>
        /// Modelo de e-mail para contratação de audiência
        /// </summary>
        /// <param name="advogado"></param>
        /// <returns></returns>
        public Email AudienciaContratada(Entity.Advogado advogado)
        {
            Corpo = new StringBuilder();
            Corpo.Append("<p>Olá,</p>\n");
            Corpo.Append("<p>Você foi contratado para a audiência.<br />\n");
            Corpo.Append("Clique no link abaixo para visualizar sua agenda ou copie e cole o link no navegador.</p>\n");
            Corpo.Append("<p><a href='" + Url + "/agenda' target='_blank'>" + Url + "/agenda</a></p>\n");
            Corpo.Append("<p>--<br />\n");
            Corpo.Append("Atenciosamente,<br />\n");
            Corpo.Append("Equipe Minha Audiência</p>\n");

            return this;
        }
    }
}