﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace minhaaudiencia.Util
{
    public class Util
    {
        /// <summary>
        /// Remove todos os caracteres especiais da sentença e substitui o espaço por um caractere pré definido.
        /// </summary>
        /// <param name="Expressao">Expressão que terá os caracteres especiais removidos.</param>
        /// <param name="Espaco">Caractere que substituirá o espaço em branco.</param>
        /// <returns></returns>
        public string RemoverCaractereEspecial(object Expressao, string Espaco)
        {
            string[] entrada = new string[13];
            string[] saida = new string[13];

            entrada[0] = "áãàâä";
            saida[0] = "a";
            entrada[1] = "éèêë";
            saida[1] = "e";
            entrada[2] = "íìîï";
            saida[2] = "i";
            entrada[3] = "óõòôö";
            saida[3] = "o";
            entrada[4] = "úùûü";
            saida[4] = "u";
            entrada[5] = "ç";
            saida[5] = "c";
            entrada[6] = "ÁÃÀÂÄ";
            saida[6] = "A";
            entrada[7] = "ÉÈÊË";
            saida[7] = "E";
            entrada[8] = "ÍÌÎÏ";
            saida[8] = "I";
            entrada[9] = "ÓÕÒÔÖ";
            saida[9] = "O";
            entrada[10] = "ÚÙÛÜ";
            saida[10] = "U";
            entrada[11] = "Ç";
            saida[11] = "C";
            entrada[12] = "'=/*+)(*&¨%$#@!\"\\|,<>;:/?´`[{]}~^¹²³£¢¬ªº°§";
            saida[12] = "";

            for (int i = 0; i < entrada.Length; i++)
            {
                for (int ii = 0; ii < entrada[i].Length; ii++)
                {
                    string e = entrada[i].Substring(ii, 1);
                    string s = saida[i];
                    Expressao = Expressao.ToString().Replace(e, s);
                }
            }

            return Expressao.ToString().ToLower().Replace(" ", Espaco);
        }

        /// <summary>
        /// Validação de e-mail
        /// </summary>
        /// <param name="Email">Endereço de e-mail</param>
        /// <returns></returns>
        public bool ValidarEmail(string Email)
        {
            if (!new Regex(@"^[a-zA-Z0-9][\w\d_.-]+@([\w\d]+\.)+[\w]{2,4}$").IsMatch(Email))
                return false;

            return true;
        }

        /// <summary>
        /// Validação de cpf
        /// </summary>
        /// <param name="cpf">Número do cpf</param>
        /// <returns></returns>
        public bool ValidarCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");

            if (cpf.Length != 11)
                return false;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        /// <summary>
        /// Validação de cnpj
        /// </summary>
        /// <param name="cnpj">Número do cnpj</param>
        /// <returns></returns>
        public bool ValidarCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        /// <summary>
        /// Criptografia MD5
        /// </summary>
        /// <param name="Valor">Expressão a ser criptografada.</param>
        /// <returns></returns>
        public string Criptografia(string Valor)
        {
            MD5 oCryp = new MD5CryptoServiceProvider();
            byte[] Result = oCryp.ComputeHash(Encoding.UTF8.GetBytes(Valor));

            return BitConverter.ToString(Result);
        }
    }
}