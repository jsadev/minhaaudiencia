﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class MasterPage : System.Web.UI.MasterPage
    {
        public Entity.Advogado _Advogado = new Entity.Advogado();

        protected void Page_Load(object sender, EventArgs e)
        {
            _Advogado = (Entity.Advogado)Session[Constante.Sessions.LOGIN];
        }

        public void setActive(string Url)
        {
            string uri = Request.Url.AbsolutePath;
            if (uri.Contains(Url))
                Response.Write("class='ativo'");
        }

        protected void lnkSair_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("~/login");
        }
}
}