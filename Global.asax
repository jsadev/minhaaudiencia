﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        // API
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "api/{pagina}", "~/api/{pagina}.aspx", false, null, new System.Web.Routing.RouteValueDictionary { { "pagina", "(ofertas|convites|agenda|advogados|foruns|agenda-ofertar)" } });
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "api/ofertas/{pagina}", "~/api/ofertas-{pagina}.aspx", false, null, new System.Web.Routing.RouteValueDictionary { { "pagina", "(contratar|curti|indico|cancelar)" } });
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "api/convites/{pagina}", "~/api/convites-{pagina}.aspx", false, null, new System.Web.Routing.RouteValueDictionary { { "pagina", "(disponivel|indisponivel|aceitar|desistir|curti|indico)" } });

        // PRINCIPAL
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "{pagina}", "~/{pagina}.aspx", false, null, new System.Web.Routing.RouteValueDictionary { { "pagina", "(login|recuperar-senha|quem-somos|vantagens|termo-de-uso-e-privacidade)" } });

        // APPS
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "{pagina}", "~/apps/{pagina}.aspx", false, null, new System.Web.Routing.RouteValueDictionary { { "pagina", "(ofertas|nova-audiencia|convites|agenda|advogados)" } });
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "ofertas/{id}/alterar-audiencia", "~/apps/nova-audiencia.aspx", false, null);

        // MINHA CONTA
        System.Web.Routing.RouteTable.Routes.MapPageRoute("", "minha-conta/{pagina}", "~/minha-conta/{pagina}.aspx", false, null, new System.Web.Routing.RouteValueDictionary { { "pagina", "(alterar-cadastro|alterar-senha)" } });
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>
