﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class termo_de_uso_e_privacidade : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session[Constante.Sessions.LOGIN] == null)
            Page.MasterPageFile = "~/MasterPageAberta.master";
        else
            Page.MasterPageFile = "~/MasterPage.master";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}