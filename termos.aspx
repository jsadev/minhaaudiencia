﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="termos.aspx.cs" Inherits="minhaaudiencia.termos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="custom.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/css/font-awesome.css" />
    <link rel="stylesheet" href="assets/css/ace.css" />
</head>

<body>
    <form id="form1" runat="server">
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="row">
                        <h1>Termos de Uso e Privacidade</h1>
                        <p>Curabitur vestibulum varius tempor. Duis ligula libero, euismod et sollicitudin lobortis, fermentum efficitur tellus. Vivamus gravida mauris ullamcorper, elementum risus eget, fringilla sem. Integer pharetra ultrices elit, sed bibendum libero euismod et. Etiam at lobortis tellus. Praesent imperdiet massa sit amet massa vestibulum commodo. Proin sed ante urna. Integer nec molestie nulla. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec mauris tortor, gravida eget lorem non, semper iaculis erat. Praesent vulputate urna sit amet gravida efficitur. Pellentesque vitae auctor velit. Nunc eu diam hendrerit dui hendrerit condimentum a eu ante. Nullam eget lacinia elit.</p>
                        <p>Aliquam erat volutpat. Sed bibendum lacus at aliquet ornare. Vestibulum egestas sem sit amet tortor faucibus viverra. Maecenas finibus urna mi, eget sollicitudin nibh accumsan vitae. Nullam ac blandit nisl, imperdiet porttitor nunc. Quisque euismod libero at sem tempor auctor. Etiam dui mi, convallis eget bibendum vel, condimentum id nisl. Cras condimentum ante at iaculis malesuada. Praesent id venenatis velit, at tincidunt est. Morbi et imperdiet dui. Nam ultrices arcu in elit ultrices, in elementum sapien ultrices. Curabitur ultricies elit nunc, quis aliquam eros pharetra quis. Nam ac nulla ac odio malesuada vehicula vitae quis nunc. Morbi ut nibh eros.</p>
                        <p>Maecenas rhoncus mattis blandit. Integer eget elit sagittis, sollicitudin leo quis, lacinia lacus. Duis mi nulla, lacinia eget egestas sit amet, lacinia ut nulla. In hac habitasse platea dictumst. Maecenas id diam auctor, gravida odio id, tempus tortor. Suspendisse potenti. Vivamus et lacinia augue, at auctor nunc. Sed ut feugiat odio. Vestibulum sit amet viverra elit, id aliquet quam. Etiam ullamcorper urna a tristique vestibulum. Nulla consectetur odio quam, quis varius lacus lacinia in. Ut sit amet volutpat sem, ut ultricies arcu.</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

</html>