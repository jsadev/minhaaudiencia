﻿namespace minhaaudiencia
{
    using System;

    public abstract class BaseApi : System.Web.UI.Page
    {
        public Entity.Advogado advogado = new Entity.Advogado();

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[Constante.Sessions.LOGIN] != null)
                advogado = (Entity.Advogado)Session[Constante.Sessions.LOGIN];
        }

        public class Error
        {
            public Error()
            {
            }

            public Error(string code, bool error, string message)
            {
                this.code = code;
                this.error = error;
                this.message = message;
            }

            public string code { get; set; }
            public bool error { get; set; }
            public string message { get; set; }
        }
    }
}