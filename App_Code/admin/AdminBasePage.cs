﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;

public abstract class AdminBasePage : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session[Constante.Sessions.ADM_LOGIN] == null)
            Response.Redirect(ConfigurationManager.AppSettings["Url"] + "/_Admin/login.aspx?u=" + Request.Url.AbsoluteUri);
    }
}