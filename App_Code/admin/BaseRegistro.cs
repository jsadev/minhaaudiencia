﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Drawing;

public abstract class BaseRegistro : AdminBasePage
{
    protected void LimparLabel()
    {
        foreach (Control c in Form.FindControl("conteudo").Controls)
        {
            switch (c.GetType().ToString())
            {
                case "System.Web.UI.WebControls.TextBox":
                    ((TextBox)c).BackColor = Color.Empty;
                    break;
                case "System.Web.UI.WebControls.DropDownList":
                    ((DropDownList)c).BackColor = Color.Empty;
                    break;
                case "System.Web.UI.HtmlControls.HtmlGenericControl":
                    c.Visible = false;
                    break;
                case "System.Web.UI.WebControls.Label":
                    ((Label)c).Text = "";
                    break;
                case "System.Web.UI.WebControls.Panel":
                    c.Visible = false;
                    break;
            }
        }

        foreach (Control c in Form.FindControl("conteudo").FindControl("pnlError").Controls)
        {
            switch (c.GetType().ToString())
            {
                case "System.Web.UI.WebControls.Label":
                    ((Label)c).Text = "";
                    break;
            }
        }
    }

    protected void LimparCampos()
    {
        foreach (Control c in Form.FindControl("conteudo").Controls)
        {
            switch (c.GetType().ToString())
            {
                case "System.Web.UI.WebControls.TextBox":
                    ((TextBox)c).Text = "";
                    break;
                case "System.Web.UI.WebControls.DropDownList":
                    ((DropDownList)c).ClearSelection();
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputCheckBox":
                    ((HtmlInputCheckBox)c).Checked = false;
                    break;
            }
        }
    }

    public enum STATUS { SUCCESS, ERROR, INFO, WARNING }

    protected void Mensagem(Panel pnlError, HtmlGenericControl ico, Label labMsg, STATUS status, string msg)
    {
        switch (status)
        {
            case STATUS.SUCCESS:
                pnlError.Attributes.Add("class", "alert alert-success");
                ico.Attributes.Add("class", "fa fa-check");
                break;
            case STATUS.ERROR:
                pnlError.Attributes.Add("class", "alert alert-error");
                ico.Attributes.Add("class", "fa fa-times");
                break;
        }

        pnlError.Visible = true;
        labMsg.Text = msg;
    }

    protected void Imagem(System.Web.UI.WebControls.Image img, HiddenField hid, string Path, string Arquivo)
    {
        if (!String.IsNullOrEmpty(Arquivo))
        {
            img.Visible = true;
            img.ImageUrl = Path + Arquivo;
            hid.Value = Arquivo;
        }
    }

    protected void Arquivo(System.Web.UI.WebControls.Label lbl, HiddenField hid, string Path, string Arquivo)
    {
        if (!String.IsNullOrEmpty(Arquivo))
        {
            lbl.Text = "<a href='" + Path + Arquivo + "' target='_blank'><i class='fa fa-download'></i> " + Arquivo + "</a>";
            hid.Value = Arquivo;
        }
        else
        {
            lbl.Text = "";
            hid.Value = "";
        }
    }
}