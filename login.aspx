﻿<%@ Page Title="Login - Minha Audiência" Language="C#" MasterPageFile="~/MasterPageAberta.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="minhaaudiencia.login" EnableEventValidation="false" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <link href="assets/css/fancybox/jquery.fancybox.css" rel="stylesheet" />
    <link href="assets/css/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" />
    <link href="assets/css/jquery-impromptu.css" rel="stylesheet" />

    <script src="assets/js/jquery-1.9.1.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/sweetalert.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery-impromptu.js"></script>

    <script type="text/javascript">
        var areas = [
            <% MontarAreas(); %>
        ];

        var as = [];
        var cs = [];
        var fs = [];
    </script>

    <script src="assets/js/interno/config.js"></script>
    <script src="assets/js/interno/login.js?v=<%=new Random(DateTime.Now.Millisecond).Next(1111111, 9999999)%>"></script>
    <script src="assets/js/jquery.maskedinput.min.js"></script>
    <script src="assets/js/jquery.maskMoney.min.js"></script>
    <script src="assets/js/jquery.fancybox.js"></script>
    <script src="assets/js/jquery.cookie.js"></script>
</asp:Content>

<asp:Content ID="conteudo" runat="server" ContentPlaceHolderID="conteudo">
    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>

    <div class="login-container">
        <div class="space-6"></div>
        <div class="position-relative">
            <div id="login-box" class="login-box visible widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header blue lighter bigger">LOGIN</h4>
                        <div class="space-6"></div>
                        <fieldset>
                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <asp:TextBox ID="txtLoginEmail" runat="server" CssClass="form-control input-login" placeholder="Digite o E-mail" required></asp:TextBox>
                                </span>
                            </label>
                            <hr />
                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <asp:TextBox ID="txtLoginSenha" runat="server" CssClass="form-control input-login" placeholder="Digite a Senha" TextMode="Password"></asp:TextBox>
                                </span>
                            </label>

                            <asp:Label ID="labMsg" runat="server" CssClass="text-danger"></asp:Label>

                            <div class="space"></div>
                            <div class="clearfix center">
                                <button type="submit" id="btnEntrar" runat="server" class="width-35 pull-center btn btn-sm btn-primary btn-entrar" onserverclick="btnEntrar_ServerClick">
                                    <span class="bigger-110">ENTRAR</span>
                                </button>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>
                    </div>
                    <!-- /.widget-main -->

                    <div class="toolbar clearfix text-center">
                        <center>
                            <div>
                                <a href="#" data-target="#forgot-box" class="forgot-password-link btn-esqueci">
                                    Esqueci a Senha
                                </a>
                            </div>
                        </center>
                        <div class="space-1"></div>
                    </div>
                    <div class="space-6"></div>

                    <div class="toolbar clearfix" style="background-color: #efefef !important; width: 100%;">
                        <div>
                            <center>
                                <div class="space-6"></div>
                                <div class="space-6"></div>
                                <a href="#" data-target="#signup-box" class="user-signup-link btn-cadastro">
                                    Cadastre-se
                                </a>
                            </center>
                        </div>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.login-box -->

            <div id="forgot-box" class="forgot-box widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header red lighter bigger">
                            <i class="ace-icon fa fa-key"></i>
                            Lembrar Senha
                        </h4>
                        <div class="space-6"></div>
                        <p>
                            Digite o e-mail para receber sua senha.
                        </p>
                        <fieldset>
                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <asp:TextBox ID="txtLembrarEmail" runat="server" CssClass="form-control" placeholder="E-mail" MaxLength="100"></asp:TextBox>
                                    <i class="ace-icon fa fa-envelope"></i>
                                </span>
                            </label>
                            <div class="clearfix">
                                <button type="button" id="btnEnviar" runat="server" class="width-35 pull-right btn btn-sm btn-danger" onserverclick="btnEnviar_ServerClick">
                                    <i class="ace-icon fa fa-lightbulb-o"></i>
                                    <span class="bigger-110">Enviar!</span>
                                </button>
                            </div>
                        </fieldset>
                    </div>
                    <!-- /.widget-main -->

                    <div class="toolbar center">
                        <a href="#" data-target="#login-box" class="back-to-login-link">Voltar para tela de Login
												<i class="ace-icon fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.forgot-box -->

            <div id="signup-box" class="signup-box widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header green lighter bigger">
                            <i class="ace-icon fa fa-users blue"></i>
                            Cadastrar
                        </h4>

                        <div class="space-6"></div>
                        <p>Digite seus dados para começar: </p>
                        <div class="row">
                            <div class="col-xs-12 form-horizontal">
                                <!-- PAGE CONTENT BEGINS -->
                                <div class="form-group">
                                    <label class="col-sm-12 control-label text-left" for="txtNome">Nome Completo</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtNome" runat="server" CssClass="col-sm-12" placeholder="insira o nome completo" MaxLength="100"></asp:TextBox>
                                        <asp:Label ID="lblNome" runat="server" CssClass="text-danger"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="radio radio-inline">
                                            <asp:RadioButton ID="radTipoPF" runat="server" GroupName="radTipo" Checked="true" />
                                            <label for="radTipoPF">Pessoa Física</label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <asp:RadioButton ID="radTipoPJ" runat="server" GroupName="radTipo" />
                                            <label for="radTipoPJ">Pessoa Jurídica</label>
                                        </div>
                                    </div>
                                </div>
                                <div id="dCpf" class="form-group">
                                    <label class="col-sm-12 control-label" for="txtCpf">CPF</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtCpf" runat="server" CssClass="col-sm-12 _cpf" placeholder="(apenas números)" MaxLength="14"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="dCnpj" class="form-group hidden">
                                    <label class="col-sm-12 control-label" for="txtCnpj">CNPJ</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtCnpj" runat="server" CssClass="col-sm-12 _cnpj" placeholder="(apenas números)" MaxLength="19"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtOab">OAB</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtOab" runat="server" CssClass="col-sm-12 _oab" placeholder="(apenas números)" MaxLength="6"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-12 m-t-sm">
                                        <asp:DropDownList ID="ddlSeccional" runat="server" CssClass="chosen-select col-sm-12">
                                            <asp:ListItem Value="">Selecione o Estado</asp:ListItem>
                                            <asp:ListItem Value="AC">Acre</asp:ListItem>
                                            <asp:ListItem Value="AL">Alagoas</asp:ListItem>
                                            <asp:ListItem Value="AM">Amazonas</asp:ListItem>
                                            <asp:ListItem Value="AP">Amapá</asp:ListItem>
                                            <asp:ListItem Value="BA">Bahia</asp:ListItem>
                                            <asp:ListItem Value="CE">Ceará</asp:ListItem>
                                            <asp:ListItem Value="DF">Distrito Federal</asp:ListItem>
                                            <asp:ListItem Value="ES">Espírito Santo</asp:ListItem>
                                            <asp:ListItem Value="GO">Goiás</asp:ListItem>
                                            <asp:ListItem Value="MA">Maranhão</asp:ListItem>
                                            <asp:ListItem Value="MT">Mato Grosso</asp:ListItem>
                                            <asp:ListItem Value="MS">Mato Grosso do Sul</asp:ListItem>
                                            <asp:ListItem Value="MG">Minas Gerais</asp:ListItem>
                                            <asp:ListItem Value="PA">Pará</asp:ListItem>
                                            <asp:ListItem Value="PB">Paraíba</asp:ListItem>
                                            <asp:ListItem Value="PR">Paraná</asp:ListItem>
                                            <asp:ListItem Value="PE">Pernambuco</asp:ListItem>
                                            <asp:ListItem Value="PI">Piauí</asp:ListItem>
                                            <asp:ListItem Value="RJ">Rio de Janeiro</asp:ListItem>
                                            <asp:ListItem Value="RN">Rio Grande do Norte</asp:ListItem>
                                            <asp:ListItem Value="RO">Rondônia</asp:ListItem>
                                            <asp:ListItem Value="RS">Rio Grande do Sul</asp:ListItem>
                                            <asp:ListItem Value="RR">Roraima</asp:ListItem>
                                            <asp:ListItem Value="SC">Santa Catarina</asp:ListItem>
                                            <asp:ListItem Value="SE">Sergipe</asp:ListItem>
                                            <asp:ListItem Value="SP">São Paulo</asp:ListItem>
                                            <asp:ListItem Value="TO">Tocantins</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtCep">CEP</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtCep" runat="server" CssClass="input-medium input-mask-eyescript form-control _cep" placeholder="99999-999" MaxLength="9"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtEndereco">Endereço (Rua, Número e Complemento)</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtEndereco" runat="server" CssClass="col-sm-12" placeholder="" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtBairro">Bairro</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtBairro" runat="server" CssClass="col-sm-12" placeholder="insira o nome completo" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtCidade">Cidade</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtCidade" runat="server" CssClass="col-sm-12" placeholder="insira o nome completo" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="ddlEstado">Estado</label>
                                    <div class="col-sm-12">
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="chosen-select col-sm-12">
                                            <asp:ListItem Value="">Selecione o Estado</asp:ListItem>
                                            <asp:ListItem Value="AC">Acre</asp:ListItem>
                                            <asp:ListItem Value="AL">Alagoas</asp:ListItem>
                                            <asp:ListItem Value="AM">Amazonas</asp:ListItem>
                                            <asp:ListItem Value="AP">Amapá</asp:ListItem>
                                            <asp:ListItem Value="BA">Bahia</asp:ListItem>
                                            <asp:ListItem Value="CE">Ceará</asp:ListItem>
                                            <asp:ListItem Value="DF">Distrito Federal</asp:ListItem>
                                            <asp:ListItem Value="ES">Espírito Santo</asp:ListItem>
                                            <asp:ListItem Value="GO">Goiás</asp:ListItem>
                                            <asp:ListItem Value="MA">Maranhão</asp:ListItem>
                                            <asp:ListItem Value="MT">Mato Grosso</asp:ListItem>
                                            <asp:ListItem Value="MS">Mato Grosso do Sul</asp:ListItem>
                                            <asp:ListItem Value="MG">Minas Gerais</asp:ListItem>
                                            <asp:ListItem Value="PA">Pará</asp:ListItem>
                                            <asp:ListItem Value="PB">Paraíba</asp:ListItem>
                                            <asp:ListItem Value="PR">Paraná</asp:ListItem>
                                            <asp:ListItem Value="PE">Pernambuco</asp:ListItem>
                                            <asp:ListItem Value="PI">Piauí</asp:ListItem>
                                            <asp:ListItem Value="RJ">Rio de Janeiro</asp:ListItem>
                                            <asp:ListItem Value="RN">Rio Grande do Norte</asp:ListItem>
                                            <asp:ListItem Value="RO">Rondônia</asp:ListItem>
                                            <asp:ListItem Value="RS">Rio Grande do Sul</asp:ListItem>
                                            <asp:ListItem Value="RR">Roraima</asp:ListItem>
                                            <asp:ListItem Value="SC">Santa Catarina</asp:ListItem>
                                            <asp:ListItem Value="SE">Sergipe</asp:ListItem>
                                            <asp:ListItem Value="SP">São Paulo</asp:ListItem>
                                            <asp:ListItem Value="TO">Tocantins</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtTelefone">Telefone</label>
                                    <div class="input-group col-sm-12" style="padding-left: 12px !important;">
                                        <span class="input-group-addon"><i class="ace-icon fa fa-phone"></i></span>
                                        <asp:TextBox ID="txtTelefone" runat="server" CssClass="input-mask-phone col-sm-12" placeholder="+99 (99) 9999-9999" MaxLength="19"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtCelular">Celular <small class="text-warning">(preferencialmente com WhatsApp)</small></label>
                                    <div class="input-group col-sm-12" style="padding-left: 12px !important;">
                                        <span class="input-group-addon"><i class="ace-icon fa fa-phone"></i></span>
                                        <asp:TextBox ID="txtCelular" runat="server" CssClass="input-mask-phone col-sm-12" placeholder="+99 (99) 9999-9999" MaxLength="19"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtSite">Site ou Perfil de Redes Socias(opcional)</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtSite" runat="server" CssClass="col-sm-12" placeholder="" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtEmail">Email</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="col-sm-12" placeholder="email@email.com" MaxLength="100"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtSenha">Senha</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtSenha" runat="server" CssClass="col-sm-12" placeholder="senha" MaxLength="50" TextMode="Password"></asp:TextBox>
                                        <span class="help-inline col-sm-12">Instruções de criação de senha</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtConfirmeSenha">Confirme a Senha</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtConfirmeSenha" runat="server" CssClass="col-sm-12" placeholder="senha" MaxLength="50" TextMode="Password"></asp:TextBox>
                                        <span class="help-inline col-sm-12">Instruções de criação de senha</span>
                                    </div>
                                    <asp:Label ID="lblSenha" runat="server" CssClass="text-danger"></asp:Label>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="txtApresentacao">Apresentação</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtApresentacao" runat="server" CssClass="form-control" Rows="3" placeholder="Faça uma pequena apresentação sobre o seu perfil e qualificações profissionais" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <h4 class="col-sm-12 control-label bolder blue">Escolha um Plano</h4>
                                </div>
                                <div class="form-group">
                                    <div class="radio radio-info">
                                        <asp:RadioButton ID="radPlano1" runat="server" GroupName="radPlano" Checked="true" />
                                        <label for="radPlano1">
                                            Grátis<br />
                                            <small>Oferecer Audiências e usar como Agenda</small>
                                        </label>
                                    </div>
                                    <hr />
                                    <div class="radio radio-info">
                                        <asp:RadioButton ID="radPlano2" runat="server" GroupName="radPlano" Enabled="false" />
                                        <label for="radPlano2">
                                            Apenas R$12,99 por mês<br />
                                            <small style="color: blue;">Oferecer e Receber Ofertas de Audiências</small>
                                        </label>
                                    </div>
                                    <hr />
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12" style="border: solid #abbac3 1px; padding: 0 !important; border-radius: 5px">
                                        <div class="col-sm-10" style="padding-top: 8px !important">
                                            <span id="lblArea">Selecione a Área de Atuação</span>
                                        </div>
                                        <button id="btnArea" type="button" class="col-sm-2 btn btn-sm">
                                            <i class="fa fa-plus fa-lg"></i>
                                        </button>
                                    </div>
                                    <div id="labArea" class="col-sm-12 m-t-sm">
                                    </div>
                                </div>
                                <div class="form-group hidden">
                                    <div id="areas" class="col-sm-12">
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <div class="col-sm-12" style="border: solid #abbac3 1px; padding: 0 !important; border-radius: 5px">
                                        <div class="col-sm-10" style="padding-top: 8px !important">
                                            <span id="lblCidade">Selecione os Fóruns que Atende</span>
                                        </div>
                                        <button id="btnCidade" type="button" class="col-sm-2 btn btn-sm">
                                            <i class="fa fa-plus fa-lg"></i>
                                        </button>
                                    </div>
                                    <div id="labForum" class="col-sm-12 m-t-sm">
                                    </div>
                                </div>
                                <div class="form-group hidden">
                                    <div id="foruns" class="col-sm-12">
                                    </div>
                                </div>
                                <!-- /section:custom/checkbox -->
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-12 control-label" for="form-field-1">Informe o Valor Mínimo para receber ofertas</label>
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtValorMinimo" runat="server" CssClass="col-sm-12 _money" placeholder="Informe o Valor Mínimo para receber ofertas" MaxLength="15"></asp:TextBox>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Como deseja receber as Notificações?</label>
                                </div>
                                <div class="form-group">
                                    <div class="radio radio-info">
                                        <asp:RadioButton ID="radNotificacao1" runat="server" GroupName="radNotificacao" Checked="true" />
                                        <label for="radNotificacao1">A Cada interação</label>
                                    </div>
                                    <div class="radio radio-info">
                                        <asp:RadioButton ID="radNotificacao2" runat="server" GroupName="radNotificacao" />
                                        <label for="radNotificacao2">Resumo diário</label>
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <div class="checkbox">
                                        <asp:CheckBox ID="chkTermo" runat="server" />
                                        <label for="chkTermo">Li e concordo com os <a id="termos" href="termos.aspx" data-fancybox-type="iframe">Termos de Uso e Privacidade</a>.</label>
                                    </div>
                                    <asp:Label ID="lblTermo" runat="server" CssClass="text-danger"></asp:Label>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i>Cancelar </button>
                                    <button type="button" id="btnSalvar" runat="server" class="btn btn-sm btn-primary" onclick="return valida();" disabled="disabled" onserverclick="btnSalvar_ServerClick">
                                        <i class="ace-icon fa fa-check"></i>
                                        Salvar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="toolbar center">
                        <a href="#" data-target="#login-box" class="back-to-login-link">
                            <i class="ace-icon fa fa-arrow-left"></i>
                            Voltar para tela de Login
                        </a>
                    </div>
                </div>
                <!-- /.widget-body -->
            </div>
            <!-- /.signup-box -->
        </div>
        <!-- /.position-relative -->
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="footer">
    <script type="text/javascript">
        jQuery(function ($) {
            $('.input-mask-phone')
                .mask("+99 (99) 9999-9999?9")
                .focusout(function (event) {
                    var target, phone, element;
                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    phone = target.value.replace(/\D/g, '');
                    element = $(target);
                    element.unmask();
                    if (phone.length > 12) {
                        element.mask("+99 (99) 99999-999?9");
                    } else {
                        element.mask("+99 (99) 9999-9999?9");
                    }
                });

            $(document).on('click', '.toolbar a[data-target]', function (e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible');//hide others
                $(target).addClass('visible');//show target
            });
        });

        //you don't need this, just used for changing background
        jQuery(function ($) {
            $('#btn-login-dark').on('click', function (e) {
                $('body').attr('class', 'login-layout');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-light').on('click', function (e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-blur').on('click', function (e) {
                $('body').attr('class', 'login-layout blur-login');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'light-blue');

                e.preventDefault();
            });

        });
    </script>
</asp:Content>