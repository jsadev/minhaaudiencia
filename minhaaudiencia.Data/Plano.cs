﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Plano : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Plano
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Plano> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Plano> planos = new List<Entity.Plano>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    planos.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return planos;
        }

        /// <summary>
        /// Consulta um registro da tabela Plano
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Plano Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Plano plano = new Entity.Plano();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    plano = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return plano;
        }

        /// <summary>
        /// Monta o objeto Entity.Plano
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Plano MontarObjeto(SqlDataReader oDr)
        {
            Entity.Plano plano = new Entity.Plano();

            if (Coluna(oDr, "IDPlano")) plano.IDPlano = (int)oDr["IDPlano"];
            if (Coluna(oDr, "Nome")) plano.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "Valor")) plano.Valor = (decimal)oDr["Valor"];
            if (Coluna(oDr, "DataInclusao")) plano.DataInclusao = (DateTime)oDr["DataInclusao"];

            return plano;
        }
    }
}