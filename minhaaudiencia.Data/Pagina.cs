﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Pagina : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Pagina
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Pagina> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Pagina> paginas = new List<Entity.Pagina>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    paginas.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return paginas;
        }

        /// <summary>
        /// Consulta um registro da tabela Pagina
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Pagina Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Pagina pagina = new Entity.Pagina();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    pagina = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return pagina;
        }

        /// <summary>
        /// Monta o objeto Entity.Pagina
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Pagina MontarObjeto(SqlDataReader oDr)
        {
            Entity.Pagina pagina = new Entity.Pagina();

            if (Coluna(oDr, "IDPagina")) pagina.IDPagina = (int)oDr["IDPagina"];
            if (Coluna(oDr, "Nome")) pagina.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "Url")) pagina.Url = oDr["Url"].ToString();
            if (Coluna(oDr, "Icone")) pagina.Icone = oDr["Icone"].ToString();
            if (Coluna(oDr, "parentId")) pagina.parentId = (int)oDr["parentId"];
            if (Coluna(oDr, "Ordem")) pagina.Ordem = (int)oDr["Ordem"];
            if (Coluna(oDr, "Tipo")) pagina.Tipo = (int)oDr["Tipo"];
            if (Coluna(oDr, "Nivel")) pagina.Nivel = (int)oDr["Nivel"];

            return pagina;
        }
    }
}