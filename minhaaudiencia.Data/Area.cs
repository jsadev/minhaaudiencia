﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Area : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Area
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Area> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Area> areas = new List<Entity.Area>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    areas.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return areas;
        }

        /// <summary>
        /// Consulta um registro da tabela Area
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Area Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Area area = new Entity.Area();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    area = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return area;
        }

        /// <summary>
        /// Monta o objeto Entity.Area
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Area MontarObjeto(SqlDataReader oDr)
        {
            Entity.Area area = new Entity.Area();

            if (Coluna(oDr, "IDArea")) area.IDArea = (int)oDr["IDArea"];
            if (Coluna(oDr, "Nome")) area.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "DataInclusao")) area.DataInclusao = (DateTime)oDr["DataInclusao"];

            return area;
        }
    }
}