﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace minhaaudiencia.Data
{
    public class Advogado : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Advogado
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Advogado> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Advogado> advogados = new List<Entity.Advogado>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    advogados.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e, Sql);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return advogados;
        }

        /// <summary>
        /// Consulta um registro da tabela Advogado
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Advogado Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Advogado advogado = new Entity.Advogado();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    advogado = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e, Sql);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return advogado;
        }

        /// <summary>
        /// Monta o objeto Entity.Advogado
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Advogado MontarObjeto(SqlDataReader oDr)
        {
            Entity.Advogado advogado = new Entity.Advogado();

            if (Coluna(oDr, "IDAdvogado")) advogado.IDAdvogado = (int)oDr["IDAdvogado"];
            if (Coluna(oDr, "Nome")) advogado.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "Tipo")) advogado.Tipo = oDr["Tipo"].ToString();
            if (Coluna(oDr, "Cpf")) advogado.Cpf = oDr["Cpf"].ToString();
            if (Coluna(oDr, "Oab")) advogado.Oab = oDr["Oab"].ToString();
            if (Coluna(oDr, "Seccional")) advogado.Seccional = oDr["Seccional"].ToString();
            if (Coluna(oDr, "Endereco")) advogado.Endereco = oDr["Endereco"].ToString();
            if (Coluna(oDr, "Bairro")) advogado.Bairro = oDr["Bairro"].ToString();
            if (Coluna(oDr, "Cidade")) advogado.Cidade = oDr["Cidade"].ToString();
            if (Coluna(oDr, "Estado")) advogado.Estado = oDr["Estado"].ToString();
            if (Coluna(oDr, "Cep")) advogado.Cep = oDr["Cep"].ToString();
            if (Coluna(oDr, "Telefone")) advogado.Telefone = oDr["Telefone"].ToString();
            if (Coluna(oDr, "Celular")) advogado.Celular = oDr["Celular"].ToString();
            if (Coluna(oDr, "Site")) advogado.Site = oDr["Site"].ToString();
            if (Coluna(oDr, "Email")) advogado.Email = oDr["Email"].ToString();
            if (Coluna(oDr, "Senha")) advogado.Senha = oDr["Senha"].ToString();
            if (Coluna(oDr, "Apresentacao")) advogado.Apresentacao = oDr["Apresentacao"].ToString();
            if (Coluna(oDr, "Plano_ID")) advogado.Plano_ID = (int)oDr["Plano_ID"];
            if (Coluna(oDr, "CidadeAtuacao")) advogado.CidadeAtuacao = oDr["CidadeAtuacao"].ToString();
            if (Coluna(oDr, "ValorMinimo")) advogado.ValorMinimo = Convert.ToDecimal(oDr["ValorMinimo"]);
            if (Coluna(oDr, "Notificacao")) advogado.Notificacao = (int)oDr["Notificacao"];
            if (Coluna(oDr, "Ativo")) advogado.Ativo = (bool)oDr["Ativo"];
            if (Coluna(oDr, "DataInclusao")) advogado.DataInclusao = (DateTime)oDr["DataInclusao"];

            if (Coluna(oDr, "Curtidas")) advogado.Curtidas = (int)oDr["Curtidas"];
            if (Coluna(oDr, "Indicacoes")) advogado.Indicacoes = (int)oDr["Indicacoes"];

            return advogado;
        }

        public class Forum : Generica
        {
            /// <summary>
            /// Lista os registros da tabela Forum
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public List<Entity.Advogado.Forum> Listar(string Sql)
            {
                List<Entity.Advogado.Forum> foruns = new List<Entity.Advogado.Forum>();

                new Data.Forum().Listar(Sql).ForEach(delegate (Entity.Forum a)
                {
                    foruns.Add(new Entity.Advogado.Forum()
                    {
                        IDForum = a.IDForum,
                        Nome = a.Nome,
                        Endereco = a.Endereco,
                        Bairro = a.Bairro,
                        Cidade = a.Cidade,
                        Estado = a.Estado,
                        Cep = a.Cep,
                        DataInclusao = a.DataInclusao
                    });
                });

                return foruns;
            }

            /// <summary>
            /// Consulta um registro da tabela Forum
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public Entity.Advogado.Forum Consultar(string Sql)
            {
                Entity.Advogado.Forum forum = new Entity.Advogado.Forum();

                new Data.Forum().Listar(Sql).ForEach(delegate (Entity.Forum a)
                {
                    forum.IDForum = a.IDForum;
                    forum.Nome = a.Nome;
                    forum.Endereco = a.Endereco;
                    forum.Bairro = a.Bairro;
                    forum.Cidade = a.Cidade;
                    forum.Estado = a.Estado;
                    forum.Cep = a.Cep;
                    forum.DataInclusao = a.DataInclusao;
                });

                return forum;
            }
        }

        public class Area : Data.Area
        {
            /// <summary>
            /// Lista os registros da tabela Area
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public List<Entity.Advogado.Area> Listar(string Sql)
            {
                List<Entity.Advogado.Area> areas = new List<Entity.Advogado.Area>();

                new Data.Area().Listar(Sql).ForEach(delegate (Entity.Area a)
                {
                    areas.Add(new Entity.Advogado.Area() { IDArea = a.IDArea, Nome = a.Nome, DataInclusao = a.DataInclusao });
                });

                return areas;
            }

            /// <summary>
            /// Consulta um registro da tabela Area
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public Entity.Advogado.Area Consultar(string Sql)
            {
                Entity.Advogado.Area area = new Entity.Advogado.Area();

                new Data.Area().Listar(Sql).ForEach(delegate (Entity.Area a)
                {
                    area.IDArea = a.IDArea;
                    area.Nome = a.Nome;
                    area.DataInclusao = a.DataInclusao;
                });

                return area;
            }
        }
    }
}