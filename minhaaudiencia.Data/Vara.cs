﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Vara : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Vara
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Vara> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Vara> varas = new List<Entity.Vara>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    varas.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return varas;
        }

        /// <summary>
        /// Consulta um registro da tabela Vara
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Vara Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Vara vara = new Entity.Vara();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    vara = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return vara;
        }

        /// <summary>
        /// Monta o objeto Entity.Vara
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Vara MontarObjeto(SqlDataReader oDr)
        {
            Entity.Vara vara = new Entity.Vara();

            if (Coluna(oDr, "IDVara")) vara.IDVara = (int)oDr["IDVara"];
            if (Coluna(oDr, "Nome")) vara.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "DataInclusao")) vara.DataInclusao = (DateTime)oDr["DataInclusao"];

            return vara;
        }

        public class Forum : Generica
        {
            /// <summary>
            /// Lista os registros da tabela Vara
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public List<Entity.Vara.Forum> Listar(string Sql)
            {
                List<Entity.Vara.Forum> foruns = new List<Entity.Vara.Forum>();

                new Data.Forum().Listar(Sql).ForEach(delegate (Entity.Forum f)
                {
                    foruns.Add(new Entity.Vara.Forum() { IDForum = f.IDForum, Nome = f.Nome, DataInclusao = f.DataInclusao });
                });

                return foruns;
            }

            /// <summary>
            /// Consulta um registro da tabela ma_forumvara
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public Entity.Vara.Forum Consultar(string Sql)
            {
                Entity.Vara.Forum forum = new Entity.Vara.Forum();

                new Data.Forum().Listar(Sql).ForEach(delegate (Entity.Forum f)
                {
                    forum.IDForum = f.IDForum;
                    forum.Nome = f.Nome;
                    forum.DataInclusao = f.DataInclusao;
                });

                return forum;
            }
        }
    }
}