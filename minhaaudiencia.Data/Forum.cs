﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Forum : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Forum
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Forum> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Forum> foruns = new List<Entity.Forum>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    foruns.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return foruns;
        }

        /// <summary>
        /// Consulta um registro da tabela Forum
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Forum Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Forum forum = new Entity.Forum();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    forum = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return forum;
        }

        /// <summary>
        /// Monta o objeto Entity.Forum
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Forum MontarObjeto(SqlDataReader oDr)
        {
            Entity.Forum forum = new Entity.Forum();

            if (Coluna(oDr, "IDForum")) forum.IDForum = (int)oDr["IDForum"];
            if (Coluna(oDr, "Nome")) forum.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "Endereco")) forum.Endereco = oDr["Endereco"].ToString();
            if (Coluna(oDr, "Bairro")) forum.Bairro = oDr["Bairro"].ToString();
            if (Coluna(oDr, "Cidade")) forum.Cidade = oDr["Cidade"].ToString();
            if (Coluna(oDr, "Estado")) forum.Estado = oDr["Estado"].ToString();
            if (Coluna(oDr, "Cep")) forum.Cep = oDr["Cep"].ToString();
            if (Coluna(oDr, "DataInclusao")) forum.DataInclusao = (DateTime)oDr["DataInclusao"];

            return forum;
        }

        public class Area : Generica
        {
            /// <summary>
            /// Lista os registros da tabela Area
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public List<Entity.Forum.Area> Listar(string Sql)
            {
                List<Entity.Forum.Area> areas = new List<Entity.Forum.Area>();

                new Data.Area().Listar(Sql).ForEach(delegate (Entity.Area a)
                {
                    areas.Add(new Entity.Forum.Area() { IDArea = a.IDArea, Nome = a.Nome, DataInclusao = a.DataInclusao });
                });

                return areas;
            }

            /// <summary>
            /// Consulta um registro da tabela Area
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public Entity.Forum.Area Consultar(string Sql)
            {
                Entity.Forum.Area area = new Entity.Forum.Area();

                new Data.Area().Listar(Sql).ForEach(delegate (Entity.Area a)
                {
                    area.IDArea = a.IDArea;
                    area.Nome = a.Nome;
                    area.DataInclusao = a.DataInclusao;
                });

                return area;
            }
        }

        public class Jurisdicao : Generica
        {
            /// <summary>
            /// Lista os registros da tabela ma_forumcidade
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public List<Entity.Forum.Jurisdicao> Listar(string Sql)
            {
                List<Entity.Forum.Jurisdicao> jurisdicoes = new List<Entity.Forum.Jurisdicao>();

                new Data.Cidade().Listar(Sql).ForEach(delegate (Entity.Cidade a)
                {
                    jurisdicoes.Add(new Entity.Forum.Jurisdicao() { IDCidade = a.IDCidade, Nome = a.Nome, Estado = a.Estado, DataInclusao = a.DataInclusao });
                });

                return jurisdicoes;
            }

            /// <summary>
            /// Consulta um registro da tabela ma_forumcidade
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public Entity.Forum.Jurisdicao Consultar(string Sql)
            {
                Entity.Forum.Jurisdicao jurisdicao = new Entity.Forum.Jurisdicao();

                new Data.Cidade().Listar(Sql).ForEach(delegate (Entity.Cidade a)
                {
                    jurisdicao.IDCidade = a.IDCidade;
                    jurisdicao.Nome = a.Nome;
                    jurisdicao.Estado = a.Estado;
                    jurisdicao.DataInclusao = a.DataInclusao;
                });

                return jurisdicao;
            }
        }
    }
}