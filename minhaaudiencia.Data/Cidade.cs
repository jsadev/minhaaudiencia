﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Cidade : Generica
    {
        /// <summary>
        /// Lista os registros da tabela ma_cidade
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Cidade> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Cidade> cidades = new List<Entity.Cidade>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    cidades.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return cidades;
        }

        /// <summary>
        /// Consulta um registro da tabela ma_cidade
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Cidade Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Cidade cidade = new Entity.Cidade();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    cidade = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return cidade;
        }

        /// <summary>
        /// Monta o objeto Entity.Cidade
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Cidade MontarObjeto(SqlDataReader oDr)
        {
            Entity.Cidade cidade = new Entity.Cidade();

            if (Coluna(oDr, "IDCidade")) cidade.IDCidade = (int)oDr["IDCidade"];
            if (Coluna(oDr, "Nome")) cidade.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "Estado")) cidade.Estado = oDr["Estado"].ToString();
            if (Coluna(oDr, "DataInclusao")) cidade.DataInclusao = (DateTime)oDr["DataInclusao"];

            return cidade;
        }
    }
}