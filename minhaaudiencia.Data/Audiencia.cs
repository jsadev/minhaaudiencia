﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Audiencia : Generica
    {
        /// <summary>
        /// Lista os registros da tabela ma_audiencia
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Audiencia> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Audiencia> audiencias = new List<Entity.Audiencia>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    audiencias.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return audiencias;
        }

        /// <summary>
        /// Consulta um registro da tabela ma_audiencia
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Audiencia Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Audiencia audiencia = new Entity.Audiencia();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    audiencia = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return audiencia;
        }

        /// <summary>
        /// Monta o objeto Entity.Audiencia
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Audiencia MontarObjeto(SqlDataReader oDr)
        {
            Entity.Audiencia audiencia = new Entity.Audiencia();

            if (Coluna(oDr, "IDAudiencia")) audiencia.IDAudiencia = (int)oDr["IDAudiencia"];
            if (Coluna(oDr, "Advogado_ID")) audiencia.Advogado_ID = (int)oDr["Advogado_ID"];
            if (Coluna(oDr, "Processo")) audiencia.Processo = oDr["Processo"].ToString();
            if (Coluna(oDr, "Materia")) audiencia.Materia = oDr["Materia"].ToString();
            if (Coluna(oDr, "Area_ID")) audiencia.Area_ID = (int)oDr["Area_ID"];
            if (Coluna(oDr, "Cidade_ID")) audiencia.Cidade_ID = (int)oDr["Cidade_ID"];
            if (Coluna(oDr, "Forum_ID")) audiencia.Forum_ID = (int)oDr["Forum_ID"];
            if (Coluna(oDr, "Vara_ID")) audiencia.Vara_ID = (int)oDr["Vara_ID"];
            if (Coluna(oDr, "Data")) audiencia.Data = (DateTime)oDr["Data"];
            if (Coluna(oDr, "ParteRepresenta")) audiencia.ParteRepresenta = oDr["ParteRepresenta"].ToString();
            if (Coluna(oDr, "ParteAudiencia")) audiencia.ParteAudiencia = oDr["ParteAudiencia"].ToString();
            if (Coluna(oDr, "Valor")) audiencia.Valor = Convert.ToDecimal(oDr["Valor"]);
            if (Coluna(oDr, "Descricao")) audiencia.Descricao = oDr["Descricao"].ToString();
            if (Coluna(oDr, "Curti")) audiencia.Curti = (bool)oDr["Curti"];
            if (Coluna(oDr, "Indico")) audiencia.Indico = (bool)oDr["Indico"];
            if (Coluna(oDr, "Cancelamento")) audiencia.Cancelamento = oDr["Cancelamento"].ToString();
            if (Coluna(oDr, "DataInclusao")) audiencia.DataInclusao = (DateTime)oDr["DataInclusao"];

            if (Coluna(oDr, "ForumNome")) audiencia.ForumNome = oDr["ForumNome"].ToString();
            if (Coluna(oDr, "AreaNome")) audiencia.AreaNome = oDr["AreaNome"].ToString();
            if (Coluna(oDr, "AdvogadoNome")) audiencia.AdvogadoNome = oDr["AdvogadoNome"].ToString();
            if (Coluna(oDr, "Telefone")) audiencia.Telefone = oDr["Telefone"].ToString();

            if (Coluna(oDr, "Status")) audiencia.Status = (int)oDr["Status"];
            if (Coluna(oDr, "Curtidas")) audiencia.Curtidas = (int)oDr["Curtidas"];
            if (Coluna(oDr, "Indicacoes")) audiencia.Indicacoes = (int)oDr["Indicacoes"];

            return audiencia;
        }

        public class Convite : Generica
        {
            /// <summary>
            /// Lista os registros da tabela ma_convite
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public List<Entity.Audiencia.Convite> Listar(string Sql)
            {
                SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
                SqlCommand oComm = new SqlCommand(Sql, oConn);

                SqlDataReader oDr;

                List<Entity.Audiencia.Convite> convites = new List<Entity.Audiencia.Convite>();
                try
                {
                    oConn.Open();
                    oDr = oComm.ExecuteReader();

                    while (oDr.Read())
                        convites.Add(MontarObjeto(oDr));
                }
                catch (Exception e)
                {
                    new Log(e, Sql);
                }
                finally
                {
                    oDr = null;
                    oComm = null;
                    oConn.Close();
                }

                return convites;
            }

            /// <summary>
            /// Consulta um registro da tabela ma_convite
            /// </summary>
            /// <param name="Sql">Síntaxe Sql</param>
            /// <returns></returns>
            public Entity.Audiencia.Convite Consultar(string Sql)
            {
                SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
                SqlCommand oComm = new SqlCommand(Sql, oConn);

                SqlDataReader oDr;

                Entity.Audiencia.Convite convite = new Entity.Audiencia.Convite();
                try
                {
                    oConn.Open();
                    oDr = oComm.ExecuteReader();

                    while (oDr.Read())
                        convite = MontarObjeto(oDr);
                }
                catch (Exception e)
                {
                    new Log(e, Sql);
                }
                finally
                {
                    oDr = null;
                    oComm = null;
                    oConn.Close();
                }

                return convite;
            }

            /// <summary>
            /// Monta o objeto Entity.Audiencia.Convite
            /// </summary>
            /// <param name="oDr">Linha de resultado do banco de dados</param>
            /// <returns></returns>
            private Entity.Audiencia.Convite MontarObjeto(SqlDataReader oDr)
            {
                Entity.Audiencia.Convite convite = new Entity.Audiencia.Convite();

                if (Coluna(oDr, "IDConvite")) convite.IDConvite = (int)oDr["IDConvite"];
                if (Coluna(oDr, "Audiencia_ID")) convite.Audiencia_ID = (int)oDr["Audiencia_ID"];
                if (Coluna(oDr, "Advogado_ID")) convite.Advogado_ID = (int)oDr["Advogado_ID"];
                if (Coluna(oDr, "Status_ID")) convite.Status_ID = (int)oDr["Status_ID"];
                if (Coluna(oDr, "Motivo")) convite.Motivo = oDr["Motivo"].ToString();
                if (Coluna(oDr, "Curti")) convite.Curti = (bool)oDr["Curti"];
                if (Coluna(oDr, "Indico")) convite.Indico = (bool)oDr["Indico"];
                if (Coluna(oDr, "DataInclusao")) convite.DataInclusao = (DateTime)oDr["DataInclusao"];

                if (Coluna(oDr, "Nome")) convite.Nome = oDr["Nome"].ToString();
                if (Coluna(oDr, "Oab")) convite.Oab = oDr["Oab"].ToString();
                if (Coluna(oDr, "Seccional")) convite.Seccional = oDr["Seccional"].ToString();
                if (Coluna(oDr, "Endereco")) convite.Endereco = oDr["Endereco"].ToString();
                if (Coluna(oDr, "Bairro")) convite.Bairro = oDr["Bairro"].ToString();
                if (Coluna(oDr, "Estado")) convite.Estado = oDr["Estado"].ToString();
                if (Coluna(oDr, "Telefone")) convite.Telefone = oDr["Telefone"].ToString();
                if (Coluna(oDr, "Celular")) convite.Celular = oDr["Celular"].ToString();
                if (Coluna(oDr, "Email")) convite.Email = oDr["Email"].ToString();
                if (Coluna(oDr, "Site")) convite.Site = oDr["Site"].ToString();
                if (Coluna(oDr, "Apresentacao")) convite.Apresentacao = oDr["Apresentacao"].ToString();

                return convite;
            }
        }
    }
}