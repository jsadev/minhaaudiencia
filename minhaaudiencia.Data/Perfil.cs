﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Data
{
    public class Perfil : Generica
    {
        /// <summary>
        /// Lista os registros da tabela Perfil
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public List<Entity.Perfil> Listar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            List<Entity.Perfil> perfis = new List<Entity.Perfil>();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    perfis.Add(MontarObjeto(oDr));
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return perfis;
        }

        /// <summary>
        /// Consulta um registro da tabela Perfil
        /// </summary>
        /// <param name="Sql">Síntaxe Sql</param>
        /// <returns></returns>
        public Entity.Perfil Consultar(string Sql)
        {
            SqlConnection oConn = new SqlConnection(oConexao.ConexaoBancoDeDados);
            SqlCommand oComm = new SqlCommand(Sql, oConn);

            SqlDataReader oDr;

            Entity.Perfil perfil = new Entity.Perfil();
            try
            {
                oConn.Open();
                oDr = oComm.ExecuteReader();

                while (oDr.Read())
                    perfil = MontarObjeto(oDr);
            }
            catch (Exception e)
            {
                new Log(e);
            }
            finally
            {
                oDr = null;
                oComm = null;
                oConn.Close();
            }

            return perfil;
        }

        /// <summary>
        /// Monta o objeto Entity.Perfil
        /// </summary>
        /// <param name="oDr">Linha de resultado do banco de dados</param>
        /// <returns></returns>
        private Entity.Perfil MontarObjeto(SqlDataReader oDr)
        {
            Entity.Perfil perfil = new Entity.Perfil();

            if (Coluna(oDr, "IDPerfil")) perfil.IDPerfil = (int)oDr["IDPerfil"];
            if (Coluna(oDr, "Nome")) perfil.Nome = oDr["Nome"].ToString();
            if (Coluna(oDr, "Locked")) perfil.Locked = Convert.ToBoolean(oDr["Locked"]);
            if (Coluna(oDr, "DataInclusao")) perfil.DataInclusao = (DateTime)oDr["DataInclusao"];

            if (Coluna(oDr, "Total")) perfil.Total = Convert.ToInt32(oDr["Total"]);

            return perfil;
        }

        public class Permissao : Generica
        {
        }
    }
}