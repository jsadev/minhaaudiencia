﻿namespace minhaaudiencia
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class recuperar_senha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Entity.Advogado advogado = new Business.Advogado().ConsultarAdvogadoHash(Request["h"]);
            if (advogado.IDAdvogado == 0)
                Response.Redirect("~/login");

            Response.Write(advogado.IDAdvogado);
        }

        protected void btnEntrar_ServerClick(object sender, EventArgs e)
        {
            string Senha = txtSenha.Text.ToPrepare();
            string ConfirmeSenha = txtConfirmeSenha.Text.ToPrepare();

            Entity.Advogado advogado = new Business.Advogado().ConsultarAdvogadoHash(Request["h"]);

            // Alterar a senha
            Entity.Retorno ret = new Business.Advogado().AlterarAdvogado(advogado.IDAdvogado, "Senha", new Util.Util().Criptografia(Senha));
            if (!ret.Status)
            {
                Response.Write(ret.Erro);
                return;
            }

            // Limpa o Guid
            ret = new Business.Advogado().AlterarAdvogado(advogado.IDAdvogado, "Guid", "");
            if (!ret.Status)
            {
                Response.Write(ret.Erro);
                return;
            }

            Response.Redirect("~/login");
        }
    }
}