﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Advogado
    {
        #region SELECT

        public List<Entity.Advogado> ListarAdvogado()
        {
            string Sql = "SELECT D.*, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Indico = 1)) as Indicacoes FROM ma_advogado D";
            return new Data.Advogado().Listar(Sql);
        }

        public Entity.Advogado ConsultarAdvogado(object IDAdvogado)
        {
            string Sql = "SELECT * FROM ma_advogado WHERE IDAdvogado = " + IDAdvogado;
            return new Data.Advogado().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirAdvogado(Entity.Advogado advogado)
        {
            string Sql = "INSERT INTO ma_advogado (Nome,Tipo,Cpf,Oab,Seccional,Endereco,Bairro,Cidade,Estado,Cep,Telefone,Celular,Site,Email,Senha,Apresentacao,Plano_ID,CidadeAtuacao,ValorMinimo,Notificacao,Ativo) VALUES ('" + advogado.Nome + "','" + advogado.Tipo + "','" + advogado.Cpf + "','" + advogado.Oab + "','" + advogado.Seccional + "','" + advogado.Endereco + "','" + advogado.Bairro + "','" + advogado.Cidade + "','" + advogado.Estado + "','" + advogado.Cep + "','" + advogado.Telefone + "','" + advogado.Celular + "','" + advogado.Site + "','" + advogado.Email + "','" + new Util.Util().Criptografia(advogado.Senha) + "','" + advogado.Apresentacao + "','" + advogado.Plano_ID + "','" + advogado.CidadeAtuacao + "','" + advogado.ValorMinimo.ToString().Replace(",", ".") + "','" + advogado.Notificacao + "','" + advogado.Ativo + "')";
            return new Data.Advogado().Inserir(Sql);
        }

        public Entity.Retorno AlterarAdvogado(Entity.Advogado advogado)
        {
            string Sql = "UPDATE ma_advogado SET Nome = '" + advogado.Nome + "', Cpf = '" + advogado.Cpf + "', Oab = '" + advogado.Oab + "', Seccional = '" + advogado.Seccional + "', Endereco = '" + advogado.Endereco + "', Bairro = '" + advogado.Bairro + "', Cidade = '" + advogado.Cidade + "', Estado = '" + advogado.Estado + "', Cep = '" + advogado.Cep + "', Telefone = '" + advogado.Telefone + "', Celular = '" + advogado.Celular + "', Site = '" + advogado.Site + "', Email = '" + advogado.Email + "', Apresentacao = '" + advogado.Apresentacao + "', Plano_ID = " + advogado.Plano_ID + ", CidadeAtuacao = '" + advogado.CidadeAtuacao + "', ValorMinimo = " + advogado.ValorMinimo.ToString().Replace(",", ".") + ", Notificacao = " + advogado.Notificacao + ", Ativo = '" + advogado.Ativo + "' WHERE IDAdvogado = " + advogado.IDAdvogado;
            return new Data.Advogado().Alterar(Sql);
        }

        public Entity.Retorno AlterarSenha(Entity.Advogado advogado)
        {
            string Sql = "UPDATE ma_advogado SET Senha = '" + new Util.Util().Criptografia(advogado.Senha) + "' WHERE IDAdvogado = " + advogado.IDAdvogado;
            return new Data.Advogado().Alterar(Sql);
        }

        public Entity.Retorno ExcluirAdvogado(object IDAdvogado)
        {
            string Sql = "DELETE FROM ma_advogado WHERE IDAdvogado = " + IDAdvogado;
            return new Data.Advogado().Alterar(Sql);
        }

        #endregion

        public class Forum
        {
            #region SELECT

            public List<Entity.Advogado.Forum> ListarForum(object Advogado_ID)
            {
                string Sql = "SELECT * FROM ma_forum WHERE IDForum IN (SELECT Forum_ID FROM ma_advogadoforum WHERE Advogado_ID = " + Advogado_ID + ")";
                return new Data.Advogado.Forum().Listar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirForum(object Advogado_ID, object Forum_ID)
            {
                string Sql = "INSERT INTO ma_advogadoforum (Advogado_ID,Forum_ID) VALUES (" + Advogado_ID + "," + Forum_ID + ")";
                return new Data.Advogado.Forum().Alterar(Sql);
            }

            public Entity.Retorno ExcluirForum(object Advogado_ID)
            {
                string Sql = "DELETE FROM ma_advogadoforum WHERE Advogado_ID = " + Advogado_ID;
                return new Data.Advogado.Forum().Alterar(Sql);
            }

            #endregion
        }

        public class Area
        {
            #region SELECT

            public List<Entity.Advogado.Area> ListarArea(object IDAdvogado)
            {
                string Sql = "SELECT * FROM ma_area WHERE IDArea IN (SELECT Area_ID FROM ma_advogadoarea WHERE Advogado_ID = " + IDAdvogado + ")";
                return new Data.Advogado.Area().Listar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirArea(object Advogado_ID, object Area_ID)
            {
                string Sql = "INSERT INTO ma_advogadoarea (Advogado_ID,Area_ID) VALUES (" + Advogado_ID + "," + Area_ID + ")";
                return new Data.Advogado.Area().Alterar(Sql);
            }

            public Entity.Retorno ExcluirArea(object Advogado_ID)
            {
                string Sql = "DELETE FROM ma_advogadoarea WHERE Advogado_ID = " + Advogado_ID;
                return new Data.Advogado.Area().Alterar(Sql);
            }

            #endregion
        }
    }
}