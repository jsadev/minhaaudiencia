﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Vara
    {
        #region SELECT

        public List<Entity.Vara> ListarVara()
        {
            string Sql = "SELECT * FROM ma_vara";
            return new Data.Vara().Listar(Sql);
        }

        public List<Entity.Vara> ListarVara(object IDForum)
        {
            string Sql = "SELECT * FROM ma_vara WHERE IDVara IN (SELECT Vara_ID FROM ma_forumvara WHERE Forum_ID = " + IDForum + ")";
            return new Data.Vara().Listar(Sql);
        }

        public Entity.Vara ConsultarVara(object IDVara)
        {
            string Sql = "SELECT * FROM ma_vara WHERE IDVara = " + IDVara;
            return new Data.Vara().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirVara(Entity.Vara vara)
        {
            string Sql = "INSERT INTO ma_vara (Nome) VALUES ('" + vara.Nome + "')";
            return new Data.Vara().Inserir(Sql);
        }

        public Entity.Retorno AlterarVara(Entity.Vara vara)
        {
            string Sql = "UPDATE ma_vara SET Nome = '" + vara.Nome + "' WHERE IDVara = " + vara.IDVara;
            return new Data.Vara().Alterar(Sql);
        }

        public Entity.Retorno ExcluirVara(object IDVara)
        {
            string Sql = "DELETE FROM ma_vara WHERE IDVara = " + IDVara;
            return new Data.Vara().Alterar(Sql);
        }

        #endregion

        public class Forum
        {
            #region SELECT

            public List<Entity.Vara.Forum> ListarForum(object Vara_ID)
            {
                string Sql = "SELECT * FROM ma_forum WHERE IDForum IN (SELECT Forum_ID FROM ma_forumvara WHERE Vara_ID = " + Vara_ID + ")";
                return new Data.Vara.Forum().Listar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirForum(object Vara_ID, object Forum_ID)
            {
                string Sql = "INSERT INTO ma_forumvara (Vara_ID,Forum_ID) VALUES ('" + Vara_ID + "','" + Forum_ID + "')";
                return new Data.Vara.Forum().Alterar(Sql);
            }

            public Entity.Retorno ExcluirForum(object Vara_ID)
            {
                string Sql = "DELETE FROM ma_forumvara WHERE Vara_ID = " + Vara_ID;
                return new Data.Vara.Forum().Alterar(Sql);
            }

            public Entity.Retorno ExcluirForum(object Vara_ID, object Forum_ID)
            {
                string Sql = "DELETE FROM ma_forumvara WHERE Vara_ID = " + Vara_ID + " AND Forum_ID = " + Forum_ID;
                return new Data.Vara.Forum().Alterar(Sql);
            }

            #endregion
        }
    }
}