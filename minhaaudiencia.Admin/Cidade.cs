﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Cidade
    {
        #region SELECT

        public List<Entity.Cidade> ListarCidade()
        {
            string Sql = "SELECT * FROM ma_cidade";
            return new Data.Cidade().Listar(Sql);
        }

        public Entity.Cidade ConsultarCidade(object IDCidade)
        {
            string Sql = "SELECT * FROM ma_cidade WHERE IDCidade = " + IDCidade;
            return new Data.Cidade().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirCidade(Entity.Cidade cidade)
        {
            string Sql = "INSERT INTO ma_cidade (Nome,Estado) VALUES ('" + cidade.Nome + "','" + cidade.Estado + "')";
            return new Data.Cidade().Inserir(Sql);
        }

        public Entity.Retorno AlterarCidade(Entity.Cidade cidade)
        {
            string Sql = "UPDATE ma_cidade SET Nome = '" + cidade.Nome + "', Estado = '" + cidade.Estado + "' WHERE IDCidade = " + cidade.IDCidade;
            return new Data.Cidade().Alterar(Sql);
        }

        public Entity.Retorno ExcluirCidade(object IDCidade)
        {
            string Sql = "DELETE FROM ma_cidade WHERE IDCidade =" + IDCidade;
            return new Data.Cidade().Alterar(Sql);
        }

        #endregion
    }
}