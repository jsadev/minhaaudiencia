﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Usuario
    {
        #region SELECT

        public List<Entity.Usuario> ListarUsuario()
        {
            string Sql = "SELECT * FROM ma_usuario";
            return new Data.Usuario().Listar(Sql);
        }

        public Entity.Usuario ConsultarUsuario(object IDUsuario)
        {
            string Sql = "SELECT * FROM ma_usuario WHERE IDUsuario = " + IDUsuario;
            return new Data.Usuario().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirUsuario(Entity.Usuario usuario)
        {
            string Sql = "INSERT INTO ma_usuario (Nome,Email,Senha,Ativo) VALUES ('" + usuario.Nome + "','" + usuario.Email + "','" + new Util.Util().Criptografia(usuario.Senha) + "','" + usuario.Ativo + "')";
            return new Data.Usuario().Inserir(Sql);
        }

        public Entity.Retorno AlterarUsuario(Entity.Usuario usuario)
        {
            string Sql = "UPDATE ma_usuario SET Nome = '" + usuario.Nome + "', Email = '" + usuario.Email + "', Ativo = '" + usuario.Ativo + "' WHERE IDUsuario = " + usuario.IDUsuario;
            return new Data.Usuario().Alterar(Sql);
        }

        public Entity.Retorno AlterarSenha(Entity.Usuario usuario)
        {
            string Sql = "UPDATE ma_usuario SET Senha = '" + new Util.Util().Criptografia(usuario.Senha) + "' WHERE IDUsuario = " + usuario.IDUsuario;
            return new Data.Usuario().Alterar(Sql);
        }

        public Entity.Retorno ExcluirUsuario(object IDUsuario)
        {
            string Sql = "DELETE FROM ma_usuario WHERE IDUsuario =" + IDUsuario;
            return new Data.Usuario().Alterar(Sql);
        }

        #endregion
    }
}