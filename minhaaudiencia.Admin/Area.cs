﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Area
    {
        #region SELECT

        public List<Entity.Area> ListarArea()
        {
            string Sql = "SELECT * FROM ma_area";
            return new Data.Area().Listar(Sql);
        }

        public List<Entity.Area> ListarArea(object IDAdvogado)
        {
            string Sql = "SELECT * FROM ma_area WHERE IDArea IN (SELECT Area_ID FROM ma_advogadoarea WHERE Advogado_ID = " + IDAdvogado + ")";
            return new Data.Area().Listar(Sql);
        }

        public Entity.Area ConsultarArea(object IDArea)
        {
            string Sql = "SELECT * FROM ma_area WHERE IDArea = " + IDArea;
            return new Data.Area().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirArea(Entity.Area area)
        {
            string Sql = "INSERT INTO ma_area (Nome) VALUES ('" + area.Nome + "')";
            return new Data.Area().Inserir(Sql);
        }

        public Entity.Retorno AlterarArea(Entity.Area area)
        {
            string Sql = "UPDATE ma_area SET Nome = '" + area.Nome + "' WHERE IDArea = " + area.IDArea;
            return new Data.Area().Alterar(Sql);
        }

        public Entity.Retorno ExcluirArea(object IDArea)
        {
            string Sql = "DELETE FROM ma_area WHERE IDArea = " + IDArea;
            return new Data.Area().Alterar(Sql);
        }

        #endregion
    }
}