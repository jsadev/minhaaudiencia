﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Plano
    {
        #region SELECT

        public List<Entity.Plano> ListarPlano()
        {
            string Sql = "SELECT * FROM ma_plano";
            return new Data.Plano().Listar(Sql);
        }

        #endregion
    }
}