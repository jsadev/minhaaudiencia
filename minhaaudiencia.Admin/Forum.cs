﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Forum
    {
        #region SELECT

        public List<Entity.Forum> ListarForum()
        {
            string Sql = "SELECT * FROM ma_forum";
            return new Data.Forum().Listar(Sql);
        }

        public List<Entity.Forum> ListarCidade()
        {
            string Sql = "SELECT DISTINCT Cidade FROM ma_forum ORDER BY Cidade";
            return new Data.Forum().Listar(Sql);
        }

        public List<Entity.Forum> ListarForum(string Cidade)
        {
            string Sql = "SELECT * FROM ma_forum WHERE Cidade = '" + Cidade + "'";
            return new Data.Forum().Listar(Sql);
        }

        public List<Entity.Forum> ListarForumCidade(object Area_ID, object Cidade_ID)
        {
            string Sql = "SELECT * FROM ma_forum WHERE IDForum IN (SELECT Forum_ID FROM ma_forumarea WHERE Area_ID IN (" + Area_ID + ")) AND IDForum IN (SELECT Forum_ID FROM ma_forumcidade WHERE Cidade_ID IN (" + Cidade_ID + "))";
            return new Data.Forum().Listar(Sql);
        }

        public Entity.Forum ConsultarForum(object IDForum)
        {
            string Sql = "SELECT * FROM ma_forum WHERE IDForum = " + IDForum;
            return new Data.Forum().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirForum(Entity.Forum forum)
        {
            string Sql = "INSERT INTO ma_forum (Nome,Endereco,Bairro,Cidade,Estado,Cep) VALUES ('" + forum.Nome + "','" + forum.Endereco + "','" + forum.Bairro + "','" + forum.Cidade + "','" + forum.Estado + "','" + forum.Cep + "')";
            return new Data.Forum().Inserir(Sql);
        }

        public Entity.Retorno AlterarForum(Entity.Forum forum)
        {
            string Sql = "UPDATE ma_forum SET Nome = '" + forum.Nome + "', Endereco = '" + forum.Endereco + "', Bairro = '" + forum.Bairro + "', Cidade = '" + forum.Cidade + "', Estado = '" + forum.Estado + "', Cep = '" + forum.Cep + "' WHERE IDForum = " + forum.IDForum;
            return new Data.Forum().Alterar(Sql);
        }

        public Entity.Retorno ExcluirForum(object IDForum)
        {
            string Sql = "DELETE FROM ma_forum WHERE IDForum = " + IDForum;
            return new Data.Forum().Alterar(Sql);
        }

        #endregion

        public class Area
        {
            #region SELECT

            public List<Entity.Forum.Area> ListarArea(object Forum_ID)
            {
                string Sql = "SELECT * FROM ma_area WHERE IDArea IN (SELECT Area_ID FROM ma_forumarea WHERE Forum_ID = " + Forum_ID + ")";
                return new Data.Forum.Area().Listar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirArea(object Forum_ID, object Area_ID)
            {
                string Sql = "INSERT INTO ma_forumarea (Forum_ID,Area_ID) VALUES ('" + Forum_ID + "','" + Area_ID + "')";
                return new Data.Forum.Area().Alterar(Sql);
            }

            public Entity.Retorno ExcluirArea(object Forum_ID)
            {
                string Sql = "DELETE FROM ma_forumarea WHERE Forum_ID = " + Forum_ID;
                return new Data.Forum.Area().Alterar(Sql);
            }

            public Entity.Retorno ExcluirArea(object Forum_ID, object Area_ID)
            {
                string Sql = "DELETE FROM ma_forumarea WHERE Forum_ID = " + Forum_ID + " AND Area_ID = " + Area_ID;
                return new Data.Forum.Area().Alterar(Sql);
            }

            #endregion
        }

        public class Jurisdicao
        {
            #region SELECT

            public List<Entity.Forum.Jurisdicao> ListarJurisdicao(object Forum_ID)
            {
                string Sql = "SELECT * FROM ma_cidade WHERE IDCidade IN (SELECT Cidade_ID FROM ma_forumcidade WHERE Forum_ID = " + Forum_ID + ")";
                return new Data.Forum.Jurisdicao().Listar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirJurisdicao(object Forum_ID, string Cidade_ID)
            {
                string Sql = "INSERT INTO ma_forumcidade (Forum_ID,Cidade_ID) VALUES ('" + Forum_ID + "','" + Cidade_ID + "')";
                return new Data.Forum.Jurisdicao().Alterar(Sql);
            }

            public Entity.Retorno ExcluirJurisdicao(object Forum_ID)
            {
                string Sql = "DELETE FROM ma_forumcidade WHERE Forum_ID = " + Forum_ID;
                return new Data.Forum.Jurisdicao().Alterar(Sql);
            }

            public Entity.Retorno ExcluirJurisdicao(object Forum_ID, string Cidade_ID)
            {
                string Sql = "DELETE FROM ma_forumcidade WHERE Forum_ID = " + Forum_ID + " AND Cidade_ID = '" + Cidade_ID + "'";
                return new Data.Forum.Jurisdicao().Alterar(Sql);
            }

            #endregion
        }
    }
}