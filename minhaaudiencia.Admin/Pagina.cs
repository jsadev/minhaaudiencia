﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Pagina
    {
        #region SELECT

        public List<Entity.Pagina> ListarPagina(int parentId)
        {
            string Sql = "SELECT * FROM ma_pagina WHERE parentId = " + parentId + " ORDER BY Ordem ASC";
            return new Data.Pagina().Listar(Sql);
        }

        public List<Entity.Pagina> ListarPagina(int parentId, int Perfil_ID)
        {
            string Sql = "SELECT * FROM ma_pagina WHERE parentId = " + parentId + " AND IDPagina IN (SELECT Pagina_ID FROM ma_permissao WHERE Perfil_ID = " + Perfil_ID + ") ORDER BY Ordem ASC";
            return new Data.Pagina().Listar(Sql);
        }

        public List<Entity.Pagina> ListarPaginaRecursiva(object IDPagina)
        {
            string Sql = "ma_proc_pagina " + IDPagina;
            return new Data.Pagina().Listar(Sql);
        }

        public Entity.Pagina ConsultarPagina(object IDPagina)
        {
            string Sql = "SELECT * FROM ma_pagina WHERE IDPagina = " + IDPagina;
            return new Data.Pagina().Consultar(Sql);
        }

        #endregion
    }
}