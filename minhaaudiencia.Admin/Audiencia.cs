﻿using System.Collections.Generic;

namespace minhaaudiencia.Admin
{
    public class Audiencia
    {
        #region SELECT

        public List<Entity.Audiencia> ListarAudiencia()
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, ISNULL((SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia), 2) as Status FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID";
            return new Data.Audiencia().Listar(Sql);
        }

        public bool ContemAudiencia(object Advogado_ID)
        {
            string Sql = "SELECT * FROM ma_audiencia WHERE Advogado_ID = " + Advogado_ID;
            return new Data.Audiencia().Consultar(Sql).IDAudiencia > 0;
        }

        public Entity.Audiencia ConsultarAudiencia(object IDAudiencia)
        {
            string Sql = "SELECT * FROM ma_audiencia WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirAudiencia(Entity.Audiencia audiencia)
        {
            string Sql = "INSERT INTO ma_audiencia (Materia,Area_ID,Cidade_ID,Forum_ID,Vara_ID,Data,ParteRepresenta,ParteAudiencia,Valor,Descricao) VALUES ('" + audiencia.Materia + "','" + audiencia.Area_ID + "','" + audiencia.Cidade_ID + "','" + audiencia.Forum_ID + "','" + audiencia.Vara_ID + "'," + new Data.Generica().FormatarData(audiencia.Data) + ",'" + audiencia.ParteRepresenta.ToString().Replace(",", ".") + "','" + audiencia.ParteAudiencia.ToString().Replace(",", ".") + "','" + audiencia.Valor.ToString().Replace(",", ".") + "','" + audiencia.Descricao + "')";
            return new Data.Audiencia().Inserir(Sql);
        }

        public Entity.Retorno AlterarAudiencia(Entity.Audiencia audiencia)
        {
            string Sql = "UPDATE ma_audiencia SET Materia = '" + audiencia.Materia + "', Area_ID = " + audiencia.Area_ID + ", Cidade_ID = " + audiencia.Cidade_ID + ", Forum_ID = " + audiencia.Forum_ID + ", Vara_ID = " + audiencia.Vara_ID + ", Data = " + new Data.Generica().FormatarData(audiencia.Data) + ", ParteRepresenta = " + audiencia.ParteRepresenta.ToString().Replace(",", ".") + ", ParteAudiencia = " + audiencia.ParteAudiencia.ToString().Replace(",", ".") + ", Valor = " + audiencia.Valor.ToString().Replace(",", ".") + ", Descricao = '" + audiencia.Descricao + "' WHERE IDAudiencia = " + audiencia.IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno ExcluirAudiencia(object IDAudiencia)
        {
            string Sql = "DELETE FROM ma_audiencia WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno ExcluirAudienciaAdvogado(object Advogado_ID)
        {
            string Sql = "DELETE FROM ma_audiencia WHERE Advogado_ID = " + Advogado_ID;
            return new Data.Audiencia().Alterar(Sql);
        }

        #endregion

        public class Convite
        {
            #region SELECT

            public bool ContemConvite(object Advogado_ID)
            {
                string Sql = "SELECT * FROM ma_convite WHERE Advogado_ID = " + Advogado_ID;
                return new Data.Audiencia.Convite().Consultar(Sql).IDConvite > 0;
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno ExcluirConvite(object Advogado_ID)
            {
                string Sql = "DELETE FROM ma_convite WHERE Advogado_ID = " + Advogado_ID;
                return new Data.Audiencia.Convite().Alterar(Sql);
            }

            #endregion
        }
    }
}