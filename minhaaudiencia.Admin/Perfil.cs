﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Admin
{
    public class Perfil
    {
        #region SELECT

        public List<Entity.Perfil> ListarPerfil()
        {
            string Sql = "SELECT * FROM ma_perfil";
            return new Data.Perfil().Listar(Sql);
        }

        public Entity.Perfil ConsultarPerfil(object IDPerfil)
        {
            string Sql = "SELECT * FROM ma_perfil WHERE IDPerfil = " + IDPerfil;
            return new Data.Perfil().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirPerfil(Entity.Perfil perfil)
        {
            string Sql = "INSERT INTO ma_perfil (Nome) VALUES ('" + perfil.Nome + "')";
            return new Data.Perfil().Inserir(Sql);
        }

        public Entity.Retorno AlterarPerfil(Entity.Perfil perfil)
        {
            string Sql = "UPDATE ma_perfil SET Nome = '" + perfil.Nome + "' WHERE IDPerfil = " + perfil.IDPerfil;
            return new Data.Perfil().Alterar(Sql);
        }

        public Entity.Retorno ExcluirPerfil(string[] Perfis)
        {
            string Sql = "DELETE FROM ma_perfil WHERE IDPerfil IN (" + String.Join(",", Perfis) + ")";
            return new Data.Perfil().Alterar(Sql);
        }

        #endregion

        public class Permissao
        {
            #region SELECT

            public bool ConsultarPermissao(object Perfil_ID, object Pagina_ID)
            {
                string Sql = "SELECT COUNT(*) as Total FROM ma_permissao WHERE Perfil_ID = " + Perfil_ID + " AND Pagina_ID = " + Pagina_ID;
                return new Data.Perfil().Consultar(Sql).Total > 0;
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirPermissao(object Perfil_ID, object Pagina_ID)
            {
                string Sql = "INSERT INTO ma_permissao (Perfil_ID,Pagina_ID) VALUES (" + Perfil_ID + "," + Pagina_ID + ")";
                return new Data.Perfil().Alterar(Sql);
            }

            public Entity.Retorno ExcluirPermissao(object Perfil_ID)
            {
                string Sql = "DELETE FROM ma_permissao WHERE Perfil_ID = " + Perfil_ID;
                return new Data.Perfil().Alterar(Sql);
            }

            #endregion
        }
    }
}