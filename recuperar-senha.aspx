﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="recuperar-senha.aspx.cs" Inherits="minhaaudiencia.recuperar_senha" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Minha Audiência</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <link rel="stylesheet" href="custom.css" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/css/font-awesome.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- text fonts -->
    <link rel="stylesheet" href="assets/css/ace-fonts.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="assets/css/ace.css" />

    <!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.css" />
		<![endif]-->
    <link rel="stylesheet" href="assets/css/ace-rtl.css" />

    <!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.css" />
		<![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.js"></script>
		<![endif]-->
</head>

<body class="login-layout">
    <form runat="server">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="topo-login">
                        <nav class="navbar navbar-inverse">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#">
                                        <h1><span class="white" id="id-text2">Minha Audiência</span><br /></h1>
                                    </a>
                                </div>
                                <div class="collapse navbar-collapse" id="myNavbar">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="#">Home</a></li>
                                        <li><a href="#">Quem Somos</a></li>
                                        <li><a href="#">Vantagens</a></li>
                                        <li><a href="#">Termos de Uso e Privacidade</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="login-container">
                            <div class="space-6"></div>
                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">REDEFINIR SENHA</h4>
                                            <div class="space-6"></div>
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <asp:TextBox ID="txtSenha" runat="server" CssClass="form-control input-login" placeholder="Digite a Senha" TextMode="Password"></asp:TextBox>
                                                    </span>
                                                </label>
                                                <hr />
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <asp:TextBox ID="txtConfirmeSenha" runat="server" CssClass="form-control input-login" placeholder="Redigite a Senha" TextMode="Password"></asp:TextBox>
                                                    </span>
                                                </label>

                                                <asp:Label ID="labMsg" runat="server" CssClass="text-danger"></asp:Label>

                                                <div class="space"></div>
                                                <div class="clearfix center">
                                                    <button type="submit" id="btnEntrar" runat="server" class="width-35 pull-center btn btn-sm btn-primary btn-entrar" onserverclick="btnEntrar_ServerClick">
                                                        <span class="bigger-110">ENVIAR</span>
                                                    </button>
                                                </div>

                                                <div class="space-4"></div>
                                            </fieldset>
                                        </div>
                                        <!-- /.widget-main -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.login-box -->
                            </div>
                            <!-- /.position-relative -->
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.main-content -->
        </div>
        <!-- /.main-container -->
    </form>


    <!-- basic scripts -->
    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery.js'>" + "<" + "/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
    </script>
    <![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.js'>" + "<" + "/script>");
    </script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function ($) {
            $(document).on('click', '.toolbar a[data-target]', function (e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible');//hide others
                $(target).addClass('visible');//show target
            });
        });

        //you don't need this, just used for changing background
        jQuery(function ($) {
            $('#btn-login-dark').on('click', function (e) {
                $('body').attr('class', 'login-layout');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-light').on('click', function (e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-blur').on('click', function (e) {
                $('body').attr('class', 'login-layout blur-login');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'light-blue');

                e.preventDefault();
            });

        });
    </script>
    <div style="clear: left;"></div>
    <div class="como-funciona">
        <h3>COMO FUNCIONA?</h3>
        <p>Adipiscing hac eu aenean elementum? Rhoncus sed. Diam montes, cursus mus elit pulvinar adipiscing, duis? Parturient integer pid diam scelerisque scelerisque! Nascetur! Sociis in integer sociis rhoncus lectus placerat elementum augue mus! Magnis lectus aenean et tincidunt sociis ac? Lundium dis montes vut turpis et elementum phasellus? Amet aenean non, nisi dis risus lundium! Nascetur lundium phasellus, sit placerat? Magna nisi dapibus nisi. Quis sed sagittis diam dolor nascetur, placerat elementum? Lectus sagittis, integer, ac nec risus. Aliquet magnis, egestas ac aliquet a ac lacus. Turpis montes, rhoncus amet sed ut, dolor tempor scelerisque! Porta? Nunc nisi porta penatibus porta dolor a tristique in! Risus dignissim sagittis phasellus nec placerat? Magnis arcu pulvinar rhoncus dapibus amet sed urna in.</p>
    </div>
    <div style="clear: left;"></div>
    <footer>
        Todos os Direitos Reservados.
    </footer>
</body>

</html>