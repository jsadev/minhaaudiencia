﻿namespace minhaaudiencia
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class login : System.Web.UI.Page
    {
        private decimal minDecimal = decimal.MinValue;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void MontarAreas()
        {
            new Business.Area().ListarArea().ForEach(delegate (Entity.Area area)
            {
                Response.Write("{ \"id\": " + area.IDArea + ", \"nome\": \"" + area.Nome + "\" },\n");
            });
        }

        protected void btnEntrar_ServerClick(object sender, EventArgs e)
        {
            string Email = txtLoginEmail.Text.ToPrepare();
            string Senha = txtLoginSenha.Text.ToPrepare();

            labMsg.Text = "";

            Entity.Advogado advogado = new Business.Login().ConsultarLogin(Email, Senha);
            if (advogado.IDAdvogado == 0)
            {
                labMsg.Text = "E-mail e/ou Senha inválido.";
                return;
            }

            Session[Constante.Sessions.LOGIN] = advogado;

            if (Request["u"] == null)
                Response.Redirect("Default.aspx");
            else
                Response.Redirect(Request["u"]);
        }

        protected void btnSalvar_ServerClick(object sender, EventArgs e)
        {
            string Nome = txtNome.Text.ToPrepare();
            string Tipo = radTipoPF.Checked ? "PF" : radTipoPJ.Checked ? "PJ" : "";
            string Cpf = txtCpf.Text.ToPrepare();
            string Cnpj = txtCnpj.Text.ToPrepare();
            string Oab = txtOab.Text.ToPrepare();
            string Seccional = ddlSeccional.SelectedValue;
            string Endereco = txtEndereco.Text.ToPrepare();
            string Bairro = txtBairro.Text.ToPrepare();
            string Cidade = txtCidade.Text.ToPrepare();
            string Estado = ddlEstado.SelectedValue;
            string Cep = txtCep.Text.ToPrepare();
            string Telefone = txtTelefone.Text.ToPrepare();
            string Celular = txtCelular.Text.ToPrepare();
            string Site = txtSite.Text.ToPrepare();
            string Email = txtEmail.Text.ToPrepare();
            string Senha = txtSenha.Text.ToPrepare();
            string ConfirmeSenha = txtConfirmeSenha.Text.ToPrepare();
            string Apresentacao = txtApresentacao.Text.ToPrepare();
            int Plano = (radPlano1.Checked ? 1 : 2);
            string CidadeAtuacao = "";
            string ValorMinimo = txtValorMinimo.Text.ToPrepare().Replace("R$ ", "");
            int Notificacao = radNotificacao1.Checked ? 1 : radNotificacao2.Checked ? 2 : 0;
            bool Termo = chkTermo.Checked;
            bool Validar = true;

            if (new Business.Login().ConsultarEmail(Email))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Este e-mail já está sendo utilizado.', 'error')", true);
                return;
            }
            if (new Business.Login().ConsultarCpf(Cpf.Replace(".", "").Replace("-", "").Replace("/", "")))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Este " + (Tipo == "PF" ? "CPF" : "CNPJ") + " já está sendo utilizado.', 'error')", true);
                return;
            }
            if (!Nome.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe seu nome completo.', 'error')", true);
                return;
            }
            if (radTipoPF.Checked && !Cpf.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe seu CPF.', 'error')", true);
                return;
            }
            if (radTipoPF.Checked && Cpf.isFilled() && !new Util.Util().ValidarCpf(Cpf))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('CPF inválido.', 'error')", true);
                return;
            }
            if (radTipoPJ.Checked && !Cnpj.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe seu CNPJ.', 'error')", true);
                return;
            }
            if (radTipoPJ.Checked && Cnpj.isFilled() && !new Util.Util().ValidarCnpj(Cnpj))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('CNPJ inválido.', 'error')", true);
                return;
            }
            if (!Oab.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe o OAB.', 'error')", true);
                return;
            }
            if (!Cep.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe o cep.', 'error')", true);
                return;
            }
            if (!Endereco.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe o endereço.', 'error')", true);
                return;
            }
            if (!Bairro.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe o bairro.', 'error')", true);
                return;
            }
            if (!Cidade.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe a cidade.', 'error')", true);
                return;
            }
            if (!Estado.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe o estado.', 'error')", true);
                return;
            }
            if (!Senha.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe a senha.', 'error')", true);
                return;
            }
            if (String.Compare(Senha, ConfirmeSenha, false) != 0)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Confirmação de senha não confere.', 'error')", true);
                return;
            }
            if (!ValorMinimo.isFilled())
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe o valor mínimo.', 'error')", true);
                return;
            }
            if (ValorMinimo.isFilled() && Convert.ToDecimal(ValorMinimo) < 0)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Informe valor mínimo maior ou igual a 0.', 'error')", true);
                return;
            }
            if (Request["chkArea"] == null)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Selecione a Área de Atuação.', 'error')", true);
                return;
            }
            if (Request["chkForum"] == null)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Selecione os Fóruns que Atende.', 'error')", true);
                return;
            }
            if (!Termo)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "error", "showAlert('Necessário concordar com os Termos de Uso e Privacidade.', 'error')", true);
                return;
            }

            if (Validar)
            {
                Entity.Advogado advogado = new Entity.Advogado();
                advogado.Nome = Nome;
                advogado.Tipo = Tipo;
                advogado.Cpf = (Tipo == "PF") ? Cpf.Replace(".", "").Replace("-", "") : Cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
                advogado.Oab = Oab;
                advogado.Seccional = Seccional;
                advogado.Endereco = Endereco;
                advogado.Bairro = Bairro;
                advogado.Cidade = Cidade;
                advogado.Estado = Estado;
                advogado.Cep = Cep.Replace("-", "");
                advogado.Telefone = Telefone;
                advogado.Celular = Celular;
                advogado.Site = Site;
                advogado.Email = Email;
                advogado.Senha = Senha;
                advogado.Apresentacao = Apresentacao;
                advogado.Plano_ID = Plano;
                advogado.CidadeAtuacao = CidadeAtuacao;
                advogado.ValorMinimo = Convert.ToDecimal(ValorMinimo);
                advogado.Notificacao = Notificacao;
                advogado.Ativo = true;

                Entity.Retorno ret = new Admin.Advogado().InserirAdvogado(advogado);
                if (!ret.Status)
                {
                    Response.Write(ret.Erro);
                    return;
                }

                advogado.IDAdvogado = ret.Identity;

                // adiciona área de atuação
                if (Request.Form["chkArea"] != null)
                {
                    string[] Areas = Request.Form["chkArea"].Split(',');
                    foreach (string IDArea in Areas)
                    {
                        ret = new Admin.Advogado.Area().InserirArea(advogado.IDAdvogado, IDArea);
                        if (!ret.Status)
                        {
                            Response.Write(ret.Erro);
                            return;
                        }
                    }
                }

                // adiciona fóruns que atende
                if (Request.Form["chkForum"] != null)
                {
                    string[] Foruns = Request.Form["chkForum"].Split(',');
                    foreach (string IDForum in Foruns)
                    {
                        ret = new Admin.Advogado.Forum().InserirForum(advogado.IDAdvogado, IDForum);
                        if (!ret.Status)
                        {
                            Response.Write(ret.Erro);
                            return;
                        }
                    }
                }

                Session[Constante.Sessions.LOGIN] = advogado;
                if (Request["u"] == null)
                    Page.ClientScript.RegisterStartupScript(GetType(), "redirect", "delete_cookie('areas'); delete_cookie('cidades'); delete_cookie('foruns'); window.location = 'Default.aspx'", true);
                else
                    Page.ClientScript.RegisterStartupScript(GetType(), "redirect", "delete_cookie('areas'); delete_cookie('cidades'); delete_cookie('foruns'); window.location = '" + Request["u"] + "'", true);
            }
        }

        protected void btnEnviar_ServerClick(object sender, EventArgs e)
        {
            string Email = txtLembrarEmail.Text.ToPrepare();

            Entity.Advogado advogado = new Business.Advogado().ConsultarAdvogado(Email);
            if (advogado.IDAdvogado > 0)
            {
                Guid guid = Guid.NewGuid();
                Entity.Retorno ret = new Business.Advogado().AlterarAdvogado(advogado.IDAdvogado, "Guid", guid.ToString());
                if (!ret.Status)
                {
                    Response.Write(ret.Erro);
                    return;
                }

                Util.Email oEmail = new Util.Email(advogado.Email, "", "Recuperação de Senha", Util.Email.METODO.HTML);
                oEmail.LembrarSenha(guid.ToString());
                oEmail.Enviar();

                Response.Redirect("~/login");
            }
            else
            {
                Response.Write("E-mail não cadastrado.");
            }
        }
    }
}