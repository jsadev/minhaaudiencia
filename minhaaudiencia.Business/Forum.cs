﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minhaaudiencia.Business
{
    public class Forum
    {
        #region SELECT

        public List<Entity.Forum> ListarForumCidade(object Cidade_ID)
        {
            string Sql = "SELECT * FROM ma_forum WHERE IDForum IN (SELECT Forum_ID FROM ma_forumcidade WHERE Cidade_ID = " + Cidade_ID + ")";
            return new Data.Forum().Listar(Sql);
        }

        public List<Entity.Forum> ListarForum(object Cidade_ID, string[] Areas)
        {
            string Sql = "SELECT * FROM ma_forum WHERE IDForum IN (SELECT Forum_ID FROM ma_forumarea WHERE Area_ID IN (" + String.Join(",", Areas) + ")) AND IDForum IN (SELECT Forum_ID FROM ma_forumcidade WHERE Cidade_ID IN (" + Cidade_ID + "))";
            return new Data.Forum().Listar(Sql);
        }

        public Entity.Forum ConsultarForum(object IDForum)
        {
            string Sql = "SELECT * FROM ma_forum WHERE IDForum = " + IDForum;
            return new Data.Forum().Consultar(Sql);
        }

        #endregion
    }
}