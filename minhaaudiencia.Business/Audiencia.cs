﻿using System;
using System.Collections.Generic;

namespace minhaaudiencia.Business
{
    public class Audiencia
    {
        #region SELECT

        /** OFERTAS */
        public List<Entity.Audiencia> ListarAudienciaOferta(object IDAdvogado)
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, ISNULL((SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia), 2) as Status FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID WHERE A.Advogado_ID = " + IDAdvogado + " AND A.IDAudiencia IN (SELECT Audiencia_ID FROM ma_convite) ORDER BY Data ASC";
            return new Data.Audiencia().Listar(Sql);
        }

        public List<Entity.Audiencia> ListarAudienciaOferta(object IDAdvogado, DateTime Datai, DateTime Dataf)
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, ISNULL((SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia), 2) as Status FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID WHERE A.Advogado_ID = " + IDAdvogado + " AND A.Data BETWEEN '" + Datai.ToString("yyyy-MM-dd") + " 00:00:00.000' AND '" + Dataf.ToString("yyyy-MM-dd") + " 23:59:59.999' AND A.IDAudiencia IN (SELECT Audiencia_ID FROM ma_convite) ORDER BY Data ASC";
            return new Data.Audiencia().Listar(Sql);
        }

        /** CONVITES */
        public List<Entity.Audiencia> ListarAudienciaConvite(object IDAdvogado)
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, (SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia AND Advogado_ID = " + IDAdvogado + ") as Status, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Indico = 1)) as Indicacoes FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID WHERE A.IDAudiencia IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID = " + IDAdvogado + " AND Status_ID NOT IN (2,5)) AND A.IDAudiencia NOT IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID <> " + IDAdvogado + " AND Status_ID IN (3,4,5)) ORDER BY Data ASC";
            return new Data.Audiencia().Listar(Sql);
        }

        public List<Entity.Audiencia> ListarAudienciaConvite(object IDAdvogado, DateTime Datai, DateTime Dataf)
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, (SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia AND Advogado_ID = " + IDAdvogado + ") as Status, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Indico = 1)) as Indicacoes FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID WHERE A.IDAudiencia IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID = " + IDAdvogado + " AND Status_ID NOT IN (2,5)) AND A.IDAudiencia NOT IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID <> " + IDAdvogado + " AND Status_ID IN (3,4,5)) AND A.Data BETWEEN '" + Datai.ToString("yyyy-MM-dd") + " 00:00:00.000' AND '" + Dataf.ToString("yyyy-MM-dd") + " 23:59:59.999' ORDER BY Data ASC";
            return new Data.Audiencia().Listar(Sql);
        }

        /** AGENDA */
        public List<Entity.Audiencia> ListarAudienciaAgenda(object IDAdvogado)
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, ISNULL(CASE WHEN A.Advogado_ID = " + IDAdvogado + " THEN (SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia) ELSE (SELECT MAX(Status_ID) FROM ma_convite WHERE Audiencia_ID = A.IDAudiencia AND Advogado_ID = " + IDAdvogado + ") END, -1) as Status FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID WHERE ((A.IDAudiencia IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID = " + IDAdvogado + " AND Status_ID IN (4,6)) AND A.IDAudiencia NOT IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID <> " + IDAdvogado + " AND Status_ID IN (3,4,5))) OR Advogado_ID = " + IDAdvogado + ") ORDER BY Data ASC";
            return new Data.Audiencia().Listar(Sql);
        }

        public List<Entity.Audiencia> ListarAudienciaAgenda(object IDAdvogado, DateTime Datai, DateTime Dataf)
        {
            string Sql = "SELECT A.*, B.Nome as ForumNome, C.Nome as AreaNome, D.Nome as AdvogadoNome, D.Telefone, ISNULL(CASE WHEN A.Advogado_ID = " + IDAdvogado + " THEN (SELECT MAX(Status_ID) FROM ma_convite WHERE Status_ID <> 2 AND Audiencia_ID = A.IDAudiencia) ELSE (SELECT MAX(Status_ID) FROM ma_convite WHERE Audiencia_ID = A.IDAudiencia AND Advogado_ID = " + IDAdvogado + ") END, -1) as Status FROM ma_audiencia A INNER JOIN ma_forum B ON B.IDForum = A.Forum_ID INNER JOIN ma_area C ON C.IDArea = A.Area_ID INNER JOIN ma_advogado D ON D.IDAdvogado = A.Advogado_ID WHERE ((A.IDAudiencia IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID = " + IDAdvogado + " AND Status_ID IN (4,6)) AND A.IDAudiencia NOT IN (SELECT Audiencia_ID FROM ma_convite WHERE Advogado_ID <> " + IDAdvogado + " AND Status_ID IN (3,4,5))) OR Advogado_ID = " + IDAdvogado + ") AND A.Data BETWEEN '" + Datai.ToString("yyyy-MM-dd") + " 00:00:00.000' AND '" + Dataf.ToString("yyyy-MM-dd") + " 23:59:59.999' ORDER BY Data ASC";
            return new Data.Audiencia().Listar(Sql);
        }

        /** CONSULTA */
        public Entity.Audiencia ConsultarAudiencia(object IDAudiencia)
        {
            string Sql = "SELECT * FROM ma_audiencia WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno InserirAudiencia(Entity.Audiencia audiencia)
        {
            string Sql = "INSERT INTO ma_audiencia (Advogado_ID,Processo,Materia,Area_ID,Cidade_ID,Forum_ID,Vara_ID,Data,ParteRepresenta,ParteAudiencia,Valor,Descricao) VALUES ('" + audiencia.Advogado_ID + "','" + audiencia.Processo + "','" + audiencia.Materia + "','" + audiencia.Area_ID + "','" + audiencia.Cidade_ID + "','" + audiencia.Forum_ID + "','" + audiencia.Vara_ID + "'," + new Data.Generica().FormatarData(audiencia.Data) + ",'" + audiencia.ParteRepresenta + "','" + audiencia.ParteAudiencia + "','" + audiencia.Valor.ToString().Replace(",", ".") + "','" + audiencia.Descricao + "')";
            return new Data.Audiencia().Inserir(Sql);
        }

        public Entity.Retorno AlterarAudiencia(Entity.Audiencia audiencia)
        {
            string Sql = "UPDATE ma_audiencia SET Processo = '" + audiencia.Processo + "', Materia = '" + audiencia.Materia + "', Area_ID = " + audiencia.Area_ID + ", Cidade_ID = " + audiencia.Cidade_ID + ", Forum_ID = " + audiencia.Forum_ID + ", Vara_ID = " + audiencia.Vara_ID + ", Data = " + new Data.Generica().FormatarData(audiencia.Data) + ", ParteRepresenta = '" + audiencia.ParteRepresenta + "', ParteAudiencia = '" + audiencia.ParteAudiencia + "', Valor = " + audiencia.Valor.ToString().Replace(",", ".") + ", Descricao = '" + audiencia.Descricao + "' WHERE IDAudiencia = " + audiencia.IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno AlterarAudiencia(object IDAudiencia, string Campo, string Valor)
        {
            string Sql = "UPDATE ma_audiencia SET " + Campo + " = '" + Valor + "' WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno CurtiAudiencia(object IDAudiencia)
        {
            string Sql = "UPDATE ma_audiencia SET Curti = 1 WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno IndicoAudiencia(object IDAudiencia)
        {
            string Sql = "UPDATE ma_audiencia SET Indico = 1 WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno CancelarAudiencia(object IDAudiencia, string Cancelamento)
        {
            string Sql = "UPDATE ma_audiencia SET Cancelamento = '" + Cancelamento + "' WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        public Entity.Retorno ExcluirAudiencia(object IDAudiencia)
        {
            string Sql = "DELETE FROM ma_audiencia WHERE IDAudiencia = " + IDAudiencia;
            return new Data.Audiencia().Alterar(Sql);
        }

        #endregion

        public class Convite
        {
            #region SELECT

            public List<Entity.Audiencia.Convite> ListarConvite(object Audiencia_ID)
            {
                string Sql = "SELECT c.*, a.Nome, a.Oab, a.Seccional, a.Endereco, a.Bairro, a.Estado, a.Telefone, a.Celular, a.Email, a.Site, a.Apresentacao, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = a.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = a.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = a.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = a.IDAdvogado AND Curti = 1)) as Indicacoes FROM ma_convite c INNER JOIN ma_advogado a ON a.IDAdvogado = c.Advogado_ID WHERE c.Audiencia_ID = " + Audiencia_ID + " ORDER BY a.Nome ASC";
                return new Data.Audiencia.Convite().Listar(Sql);
            }

            public Entity.Audiencia.Convite ConsultarConvite(object IDConvite)
            {
                string Sql = "SELECT * FROM ma_convite WHERE IDConvite = " + IDConvite;
                return new Data.Audiencia.Convite().Consultar(Sql);
            }

            public Entity.Audiencia.Convite ConsultarConvite(object Audiencia_ID, object Advogado_ID)
            {
                string Sql = "SELECT * FROM ma_convite WHERE Audiencia_ID = " + Audiencia_ID + " AND Advogado_ID = " + Advogado_ID;
                return new Data.Audiencia.Convite().Consultar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno InserirConvite(Entity.Audiencia.Convite convite)
            {
                string Sql = "INSERT INTO ma_convite (Audiencia_ID,Status_ID,Advogado_ID) VALUES ('" + convite.Audiencia_ID + "','" + convite.Status_ID + "','" + convite.Advogado_ID + "')";
                return new Data.Audiencia.Convite().Inserir(Sql);
            }

            public Entity.Retorno AlterarConvite(object IDConvite, string Campo, string Valor)
            {
                string Sql = "UPDATE ma_convite SET " + Campo + " = '" + Valor + "' WHERE IDConvite = " + IDConvite;
                return new Data.Audiencia.Convite().Alterar(Sql);
            }

            public Entity.Retorno CurtiConvite(object IDConvite)
            {
                string Sql = "UPDATE ma_convite SET Curti = 1 WHERE IDConvite = " + IDConvite;
                return new Data.Audiencia.Convite().Alterar(Sql);
            }

            public Entity.Retorno IndicoConvite(object IDConvite)
            {
                string Sql = "UPDATE ma_convite SET Indico = 1 WHERE IDConvite = " + IDConvite;
                return new Data.Audiencia.Convite().Alterar(Sql);
            }

            public Entity.Retorno ExcluirConvite(object Audiencia_ID)
            {
                string Sql = "DELETE FROM ma_convite WHERE Audiencia_ID = " + Audiencia_ID;
                return new Data.Audiencia.Convite().Alterar(Sql);
            }

            #endregion
        }
    }
}