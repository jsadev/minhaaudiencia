﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace minhaaudiencia.Business
{
    public class Cidade
    {
        #region SELECT

        public List<Entity.Cidade> ListarCidade()
        {
            string Sql = "SELECT * FROM ma_cidade WHERE IDCidade IN (SELECT Cidade_ID FROM ma_forumcidade) ORDER BY Nome ASC";
            return new Data.Cidade().Listar(Sql);
        }

        public List<Entity.Cidade> ListarCidade(string[] Areas)
        {
            string Sql = "SELECT * FROM ma_cidade WHERE IDCidade IN (SELECT Cidade_ID FROM ma_forumcidade WHERE Forum_ID IN (SELECT Forum_ID FROM ma_forumarea WHERE Area_ID IN (" + String.Join(",", Areas) + ")))";
            return new Data.Cidade().Listar(Sql);
        }

        #endregion
    }
}