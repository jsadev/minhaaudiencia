﻿using System.Collections.Generic;

namespace minhaaudiencia.Business
{
    public class Area
    {
        #region SELECT

        public List<Entity.Area> ListarArea()
        {
            string Sql = "SELECT * FROM ma_area ORDER BY Nome ASC";
            return new Data.Area().Listar(Sql);
        }

        public Entity.Area ConsultarArea(object IDArea)
        {
            string Sql = "SELECT * FROM ma_area WHERE IDArea = " + IDArea;
            return new Data.Area().Consultar(Sql);
        }

        #endregion
    }
}