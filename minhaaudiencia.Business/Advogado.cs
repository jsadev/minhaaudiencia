﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace minhaaudiencia.Business
{
    public class Advogado
    {
        #region SELECT

        public List<Entity.Advogado> ListarAdvogado()
        {
            string Sql = "SELECT D.*, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Indico = 1)) as Indicacoes FROM ma_advogado D ORDER BY Nome";
            return new Data.Advogado().Listar(Sql);
        }

        public List<Entity.Advogado> ListarAdvogado(object IDAdvogado)
        {
            string Sql = "SELECT D.*, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Indico = 1)) as Indicacoes FROM ma_advogado D WHERE D.IDAdvogado <> " + IDAdvogado + " ORDER BY Nome";
            return new Data.Advogado().Listar(Sql);
        }

        public List<Entity.Advogado> ListarAdvogado(object IDAdvogado, object Area_ID, object Cidade_ID, DateTime Data, decimal Valor)
        {
            string Sql = "SELECT D.*, dbo.Curtidas(D.IDAdvogado) as Curtidas, dbo.Indicacoes(D.IDAdvogado) as Indicacoes FROM ma_advogado D WHERE D.IDAdvogado <> " + IDAdvogado + " AND D.IDAdvogado IN (SELECT Advogado_ID FROM ma_advogadoarea WHERE Area_ID = " + Area_ID + ") AND D.IDAdvogado IN (SELECT Advogado_ID FROM ma_advogadoforum WHERE Forum_ID IN (SELECT Forum_ID FROM ma_forumcidade WHERE Cidade_ID = " + Cidade_ID + ")) AND D.IDAdvogado NOT IN (SELECT Advogado_ID FROM ma_convite WHERE Status_ID = 4 AND Audiencia_ID IN (SELECT IDAudiencia FROM ma_audiencia WHERE Data = '" + Data.ToString("yyyy-MM-dd HH:mm:ss") + "')) AND D.ValorMinimo <= " + Valor.ToString().Replace(",", ".") + " ORDER BY D.Nome ASC";
            return new Data.Advogado().Listar(Sql);
        }

        public Entity.Advogado ConsultarAdvogado(string Email)
        {
            string Sql = "SELECT * FROM ma_advogado WHERE Email = '" + Email + "'";
            return new Data.Advogado().Consultar(Sql);
        }

        public Entity.Advogado ConsultarAdvogadoHash(string Guid)
        {
            string Sql = "SELECT * FROM ma_advogado WHERE Guid = '" + Guid + "'";
            return new Data.Advogado().Consultar(Sql);
        }

        public Entity.Advogado ConsultarAdvogado(int IDAdvogado)
        {
            string Sql = "SELECT D.*, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Curti = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Curti = 1)) as Curtidas, ((SELECT COUNT(*) FROM ma_audiencia WHERE Advogado_ID = D.IDAdvogado AND Indico = 1) + (SELECT COUNT(*) FROM ma_convite WHERE Advogado_ID = D.IDAdvogado AND Indico = 1)) as Indicacoes FROM ma_advogado D WHERE D.IDAdvogado = " + IDAdvogado;
            return new Data.Advogado().Consultar(Sql);
        }

        #endregion

        #region INSERT, UPDATE, DELETE

        public Entity.Retorno AlterarAdvogado(object IDAdvogado, string Campo, string Valor)
        {
            string Sql = "UPDATE ma_advogado SET " + Campo + " = '" + Valor + "' WHERE IDAdvogado = " + IDAdvogado;
            return new Data.Advogado().Alterar(Sql);
        }

        #endregion

        public class Forum
        {
            #region SELECT

            public List<Entity.Advogado.Forum> ListarForum(object IDAdvogado)
            {
                string Sql = "SELECT * FROM ma_forum WHERE IDForum IN (SELECT Forum_ID FROM ma_advogadoforum WHERE Advogado_ID = " + IDAdvogado + ")";
                return new Data.Advogado.Forum().Listar(Sql);
            }

            #endregion

            #region INSERT, UPDATE, DELETE

            public Entity.Retorno ExcluirForum(object Advogado_ID, object Forum_ID)
            {
                string Sql = "DELETE FROM ma_advogadoforum WHERE Advogado_ID = " + Advogado_ID + " AND Forum_ID = " + Forum_ID;
                return new Data.Advogado.Forum().Alterar(Sql);
            }

            #endregion
        }
    }
}