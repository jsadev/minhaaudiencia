﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="add-forum.aspx.cs" Inherits="minhaaudiencia.add_forum" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/ace.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset style="border: solid #eee 1px; border-radius: 5px; margin: 10px 0; padding: 5px">
                            <% MontarForuns(); %>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button id="btnCancelar" type="button" class="btn btn-default btn-sm">
                            Cancelar
                        </button>
                        <button id="btnOk" type="button" class="btn btn-success btn-sm">
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery.cookie.js"></script>
    <script type="text/javascript">
        jQuery(function () {
            var foruns = new Array();

            $('#btnCancelar').click(function () {
                parent.$.fancybox.close();
            });
            $('#btnOk').click(function () {
                if ($.cookie('foruns') != null)
                    foruns = $.cookie('foruns').split(',');

                $('input[name="chkForum"]:checked').each(function () {
                    foruns.push($(this).val() + ":" + $(this).parent().find('span').html());
                });

                $.cookie('foruns', foruns);

                top.window.fillForuns();
                parent.$.fancybox.close();
            });
        });
    </script>
</body>

</html>