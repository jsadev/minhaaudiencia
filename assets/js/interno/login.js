﻿jQuery(function () {
    $('._cpf').mask('999.999.999-99');
    $('._cnpj').mask('99.999.999/9999-99');
    $('._oab').mask('999999');
    $('._cep').mask('99999-999');
    $('._money').maskMoney({ thousands: '.', decimal: ',', allowZero: true, prefix: 'R$ ' });

    $('#conteudo_radTipoPF, #conteudo_radTipoPJ').click(function () {
        $('#conteudo_txtCpf').val("");
        $('#conteudo_txtCnpj').val("");

        var tipo = $(this).val();
        switch (tipo) {
            case "radTipoPF":
                $('#dCpf').removeClass("hidden");
                $('#dCnpj').addClass("hidden");
                break;
            case "radTipoPJ":
                $('#dCpf').addClass("hidden");
                $('#dCnpj').removeClass("hidden");
                break;
        }
    });

    $('#conteudo_txtCep').change(function () {
        $('#conteudo_txtEndereco').val("...");
        $('#conteudo_txtBairro').val("...");
        $('#conteudo_txtCidade').val("...");
        $('#conteudo_ddlEstado').val("");

        $.getJSON("//viacep.com.br/ws/" + $(this).val() + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                //Atualiza os campos com os valores da consulta.
                $('#conteudo_txtEndereco').val(dados.logradouro);
                $('#conteudo_txtBairro').val(dados.bairro);
                $('#conteudo_txtCidade').val(dados.localidade);
                $('#conteudo_ddlEstado').val(dados.uf);
            } //end if.
            else {
                //CEP pesquisado não foi encontrado.
                alert("CEP não encontrado.");
            }
        });
    });

    $('#btnArea').click(function () {
        var html = '<div class="col-sm-12">\n';
        for (i = 0; i < areas.length; i++) {
            html += '<div class="checkbox checkbox-inline">\n';
            if (CheckCookie(as, areas[i].id))
                html += '<input id="chkA' + areas[i].id + '" type="checkbox" name="chkArea1" value="' + areas[i].id + '" checked="checked" />\n';
            else
                html += '<input id="chkA' + areas[i].id + '" type="checkbox" name="chkArea1" value="' + areas[i].id + '" />\n';
            html += '<label for="chkA' + areas[i].id + '">' + areas[i].nome + '</label>\n';
            html += '</div>\n';
        }
        html += '</div>\n';

        $.prompt(html, {
            title: "Área de Atuação",
            buttons: { "OK": true, "Cancelar": false },
            submit: function (e, v, m, f) {
                if (v) {
                    var a = f["chkArea1"];

                    parent.$('#areas').html('');
                    parent.$('#labArea').html('');

                    if (a.length == 0)
                        parent.$('#lblArea').html("Selecione a Área de Atuação");
                    else if (a.length == 1)
                        parent.$('#lblArea').html("1 área selecionada");
                    else
                        parent.$('#lblArea').html(a.length + " áreas selecionadas");

                    as = [];
                    for (i = 0; i < a.length; i++) {
                        parent.$('#areas').append('<input type="checkbox" name="chkArea" value="' + a[i] + '" checked="checked" />\n');
                        parent.$('#labArea').append('<span class="badge">' + $('label[for="chkA' + a[i] + '"]').html() + '</span>\n');
                        as.push(a[i]);
                    }

                    $.prompt.close();
                }
            }
        });
    });

    $('#btnCidade').click(function () {
        if (as.length == 0) {
            alert('Selecione a Área de Atuação antes de continuar.');
            $.prompt.close();
            return;
        }

        $.ajax({
            url: Url + "/api/cidades.aspx",
            data: { area: as.toString() },
            success: function (data) {
                data = eval('(' + data + ')');
                if (data.error) {
                    alert(data.message);
                    return;
                }

                var html = '<div class="col-sm-12">\n';
                html += '<div class="form-group">\n';
                html += '<select name="ddlCidade" class="" style="width: auto">\n';
                html += '<option value="">Cidade</option>\n';
                for (i = 0; i < data.total; i++) {
                    var cidade = data.cidades[i];
                    html += '<option value="' + cidade.id + '">' + cidade.nome + '</option>\n';
                }
                html += '</select>\n';
                html += '</div>\n';
                html += '</div>\n';

                html += '<div id="cidades" class="col-sm-12 m-t-sm" style="border: solid #eee 1px; border-radius: 2px">\n';
                html += '</div>\n';

                $.prompt(html, {
                    title: "Fóruns que Atende",
                    buttons: { "OK": true, "Cancelar": false },
                    submit: function (e, v, m, f) {
                        if (v) {
                            parent.$('#foruns').html('');
                            parent.$('#labForum').html('');

                            if (fs.length == 0)
                                parent.$('#lblCidade').html("Selecione os Fóruns que Atende");
                            else if (fs.length == 1)
                                parent.$('#lblCidade').html("1 fórum selecionado");
                            else
                                parent.$('#lblCidade').html(fs.length + " fóruns selecionados");

                            for (i = 0; i < fs.length; i++) {
                                parent.$('#foruns').append('<input type="checkbox" name="chkForum" value="' + fs[i][0] + '" checked="checked" />\n');
                                parent.$('#labForum').append('<span class="badge">' + fs[i][1] + '</span>\n');
                            }

                            $.prompt.close();
                        }
                    }
                });

                $('select[name="ddlCidade"]').change(function () {
                    $.ajax({
                        url: Url + "/api/foruns",
                        data: { cidade: $(this).val(), area: as.toString() },
                        success: function (data) {
                            data = eval('(' + data + ')');
                            if (data.error) {
                                alert(data.message);
                                return;
                            }

                            html = '<div class="form-group">\n';
                            for (i = 0; i < data.total; i++) {
                                var forum = data.foruns[i];

                                html += '<div class="col-sm-6">\n';
                                html += '<div class="checkbox checkbox-inline">\n';
                                if (CheckCookie(fs, forum.id))
                                    html += '<input id="chkF' + forum.id + '" type="checkbox" name="chkForum1" value="' + forum.id + '" checked="checked" />\n';
                                else
                                    html += '<input id="chkF' + forum.id + '" type="checkbox" name="chkForum1" value="' + forum.id + '" />\n';
                                html += '<label for="chkF' + forum.id + '">' + forum.nome + '</label>\n';
                                html += '</div>\n';
                                html += '</div>\n';
                            }
                            html += '</div>\n';

                            $('#cidades').html(html);

                            $('input[name="chkForum1"]').click(function () {
                                var id = $(this).val();
                                var name = $('label[for="chkF' + id + '"]').html()

                                var f = [];
                                f.push(id);
                                f.push(name);

                                if ($(this).prop("checked")) {
                                    fs.push(f);
                                }
                                else {
                                    var g = [];
                                    for (i = 0; i < fs.length; i++) {
                                        if (fs[i][0] != $(this).val())
                                            g.push(fs[i]);
                                    }
                                    fs = g;
                                }
                            });
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            switch (xhr.status) {
                                case 404:
                                    alert('Página não encontrada.');
                                    break;
                                default:
                                    alert(thrownError);
                                    break;
                            }
                        }
                    });
                });
            },
            error: function (xhr, ajaxOptions, throwError) {
                switch (xhr.status) {
                    case 404:
                        alert('Página não encontrada.');
                        break;
                    default:
                        alert(thrownError);
                        break;
                }
            }
        });
    });

    $('#termos').fancybox();

    $('#conteudo_chkTermo').click(function () {
        if ($(this).prop("checked"))
            $('#conteudo_btnSalvar').removeAttr("disabled");
        else
            $('#conteudo_btnSalvar').attr("disabled", "disabled");
    });

    if ($('#conteudo_chkTermo').prop("checked"))
        $('#conteudo_btnSalvar').removeAttr("disabled");
});

function valida() {
    showDialog();

    if ($('#conteudo_txtNome').val() == "") {
        $('#conteudo_txtNome').focus();
        showAlert('Informe seu nome completo', 'warning');
        return;
    }

    if ($('#conteudo_radTipoPF').prop('checked') && $('#conteudo_txtCpf').val() == "") {
        $('#conteudo_txtCpf').focus();
        showAlert('Informe seu CPF.', 'warning');
        return;
    }

    if ($('#conteudo_radTipoPJ').prop('checked') && $('#conteudo_txtCnpj').val() == "") {
        $('#conteudo_txtCnpj').focus();
        showAlert('Informe seu CNPJ.', 'warning');
        return;
    }

    if ($('#conteudo_txtOab').val() == "") {
        $('#conteudo_txtOab').focus();
        showAlert('Informe o OAB', 'warning');
        return;
    }

    if ($('#conteudo_txtCep').val() == "") {
        $('#conteudo_txtCep').focus();
        showAlert('Informe o CEP', 'warning');
        return;
    }

    if ($('#conteudo_txtEndereco').val() == "") {
        $('#conteudo_txtEndereco').focus();
        showAlert('Informe o Endereço', 'warning');
        return;
    }

    if ($('#conteudo_txtBairro').val() == "") {
        $('#conteudo_txtBairro').focus();
        showAlert('Informe o Bairro', 'warning');
        return;
    }

    if ($('#conteudo_txtCidade').val() == "") {
        $('#conteudo_txtCidade').focus();
        showAlert('Informe o Cidade', 'warning');
        return;
    }

    if ($('#conteudo_ddlEstado').val() == "") {
        $('#conteudo_ddlEstado').focus();
        showAlert('Informe o Estado', 'warning');
        return;
    }

    if ($('#conteudo_txtEmail').val() == "") {
        $('#conteudo_txtEmail').focus();
        showAlert('Informe o E-mail', 'warning');
        return;
    }

    if ($('#conteudo_txtSenha').val() == "") {
        $('#conteudo_txtSenha').focus();
        showAlert('Informe a Senha.', 'warning');
        return;
    }

    if ($('#conteudo_txtSenha').val() != "" && $('#conteudo_txtSenha').val() != $('#conteudo_txtConfirmeSenha').val()) {
        $('#conteudo_txtConfirmeSenha').focus();
        showAlert('Confirmação de senha não confere.', 'warning');
        return;
    }

    if ($('#conteudo_txtValorMinimo').val() == "") {
        $('#conteudo_txtValorMinimo').focus();
        showAlert('Informe o valor mínimo.', 'warning');
        return;
    }

    if ($('#conteudo_txtValorMinimo').val() != "" && parseInt($('#conteudo_txtValorMinimo').val()) < 0) {
        $('#conteudo_txtValorMinimo').focus();
        showAlert('Informe valor mínimo maior ou igual a 0.', 'warning');
        return;
    }

    var chkArea = false;
    $('input[name="chkArea"]').each(function () {
        if ($(this).prop('checked'))
            chkArea = true;
    });
    if (!chkArea) {
        showAlert('Selecione a Área de Atuação.', 'warning');
        return;
    }

    var chkForum = false;
    $('input[name="chkForum"]').each(function () {
        if ($(this).prop('checked'))
            chkForum = true;
    });
    if (!chkForum) {
        showAlert('Selecione os Fóruns que Atende.', 'warning');
        return;
    }

    if (!$('#conteudo_chkTermo').prop("checked")) {
        showAlert('Necessário concordar com os Termos de Uso e Privacidade.', 'warning');
        return;
    }

    if ($('#conteudo_radTipoPF').prop('checked') && $('#conteudo_txtCpf').val() != "") {
        $.ajax({
            url: Url + '/api/cadastro-validacao.aspx',
            data: { campo: 'cpf', valor: $('#conteudo_txtCpf').val() },
            success: function (d) {
                d = eval('(' + d + ')');
                if (d.error) {
                    $('#conteudo_txtCpf').focus();
                    showAlert(d.message, 'error');
                    return;
                }

                if ($('#conteudo_txtEmail').val() != "") {
                    $.ajax({
                        url: Url + '/api/cadastro-validacao.aspx',
                        data: { campo: 'email', valor: $('#conteudo_txtEmail').val() },
                        success: function (d) {
                            d = eval('(' + d + ')');
                            if (d.error) {
                                $('#conteudo_txtEmail').focus();
                                showAlert(d.message, 'error');
                                return;
                            }

                            __doPostBack('ctl00$conteudo$btnSalvar', '')
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            error(xhr, ajaxOptions, thrownError);
                        }
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                error(xhr, ajaxOptions, thrownError);
            }
        });
    }

    if ($('#conteudo_radTipoPJ').prop('checked') && $('#conteudo_txtCnpj').val() != "") {
        $.ajax({
            url: Url + '/api/cadastro-validacao.aspx',
            data: { campo: 'cnpj', valor: $('#conteudo_txtCnpj').val() },
            success: function (d) {
                d = eval('(' + d + ')');
                if (d.error) {
                    $('#conteudo_txtCnpj').focus();
                    showAlert(d.message, 'error');
                    return;
                }

                if ($('#conteudo_txtEmail').val() != "") {
                    $.ajax({
                        url: Url + '/api/cadastro-validacao.aspx',
                        data: { campo: 'email', valor: $('#conteudo_txtEmail').val() },
                        success: function (d) {
                            d = eval('(' + d + ')');
                            if (d.error) {
                                $('#conteudo_txtEmail').focus();
                                showAlert(d.message, 'error');
                                return;
                            }

                            __doPostBack('ctl00$conteudo$btnSalvar', '');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            error(xhr, ajaxOptions, thrownError);
                        }
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                error(xhr, ajaxOptions, thrownError);
            }
        });
    }

    return false;
}

function CheckCookie(array, valor) {
    for (j = 0; j < array.length; j++) {
        if (array[j] == valor || array[j][0] == valor)
            return true;
    }

    return false;
}

function showDialog() {
    $.blockUI({
        message: "Aguarde, carregando...",
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}

function closeDialog() {
    setTimeout($.unblockUI, 1);
}

function showAlert(message, type) {
    $('.login-box').removeClass("visible");
    $('.signup-box').addClass("visible");
    swal({
        title: "",
        text: message,
        type: type
    },
    function () {
        closeDialog();
    });
}

var delete_cookie = function (name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

function error(xhr, ajaxOptions, thrownError) {
    if (xhr.status == 404)
        showAlert('Página não encontrada', 'error');
    else
        showAlert(thrownError, 'error');
}