﻿jQuery(function () {
    listar({});
});

function days(valor) {
    
    switch (valor) {
        case "semana": $('#btnDays').html('Semana <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "ontem": $('#btnDays').html('Ontem <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "hoje": $('#btnDays').html('Hoje <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "amanha": $('#btnDays').html('Amanhã <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
    }

    listar({ filtro: valor });
    return false;
}

function months(valor) {
    switch (valor) {
        case 1: $('#btnMonths').html('janeiro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 2: $('#btnMonths').html('fevereiro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 3: $('#btnMonths').html('março <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 4: $('#btnMonths').html('abril <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 5: $('#btnMonths').html('maio <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 6: $('#btnMonths').html('junho <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 7: $('#btnMonths').html('julho <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 8: $('#btnMonths').html('agosto <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 9: $('#btnMonths').html('setembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 10: $('#btnMonths').html('outubro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 11: $('#btnMonths').html('novembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 12: $('#btnMonths').html('dezembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
    }

    mes = valor;
    listar({ filtro: 'mes', ano: ano, mes: mes });
    return false;
}

function years(valor) {
    $('#btnYears').html(valor + ' <i class="fa fa-angle-down" aria-hidden="true"></i>');

    ano = valor;
    listar({ filtro: 'ano', ano: ano, mes: mes });
    return false;
}

function listar(data) {
    $.ajax({
        url: Url + '/api/ofertas',
        data: data,
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            var html = '';
            $('#ddlAno').html('');

            for (i = 0; i < data.anos.length; i++) {
                $('#ddlAno').append('<li><a href="#" onclick="return years(' + data.anos[i] + ')">' + data.anos[i] + '</a></li>');
            }

            if (data.total == 0) {
                html += '<div class="ofertas col-xs-12">\n';
                html += '<div class="sem-audiencia">\n';
                html += '<p>Ofereça Audiências Aqui!</p>\n';
                html += '<a href="' + Url + '/nova-audiencia">CADASTRAR AUDIÊNCIA</a>\n';
                html += '</div>\n';
                html += '</div>\n';
            }
            else {
                for (i = 0; i < data.datas.length; i++) {
                    html += '<div class="col-xs-12">\n';
                    html += '<h3>' + data.datas[i].data + '</h3>\n';
                    html += '</div>\n';

                    for (j = 0; j < data.datas[i].audiencias.length; j++) {
                        var audiencia = data.datas[i].audiencias[j];

                        var resp = 0;
                        var nresp = 0;

                        for (c = 0; c < audiencia.convites.length; c++) {
                            if (audiencia.convites[c].status == 0)
                                nresp++;
                            else
                                resp++;
                        }

                        html += '<div class="ofertas col-xs-12">\n';
                        html += '<div class="col-xs-4">\n';
                        html += '<div class="hora-' + (audiencia.vencido ? 'passada' : 'futura') + '">' + audiencia.hora + '</div>\n';
                        html += '</div>\n';
                        html += '<div class="col-xs-8">\n';
                        html += '<div class="tit-item"><a href="' + Url + '/ofertas/' + audiencia.id + '/alterar-audiencia">' + audiencia.materia + '</a></div>\n';
                        html += '<div class="nat-item">' + audiencia.area + '</div>\n';

                        if (audiencia.vencido) {
                            if (audiencia.status == 3 || audiencia.status == 4) {
                                for (k = 0; k < audiencia.convites.length; k++) {
                                    var advogado = audiencia.convites[k];

                                    if (advogado.status == 3 || advogado.status == 4) {
                                        html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                        html += '<ul class="aval">\n';
                                        html += '<li><a href="" class="ico-curti-' + (advogado.curti ? 'on' : 'off') + '" ' + (advogado.curti ? '' : 'onclick="curti(' + advogado.id + ')"') + '><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>\n';
                                        html += '<li><a href="" class="ico-recomendo-' + (advogado.indico ? 'on' : 'off') + '" ' + (advogado.indico ? '' : 'onclick="indico(' + advogado.id + ')"') + '><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>\n';
                                        html += '<li class="aval-off">' + (advogado.curti && advogado.indico ? 'avaliado' : 'avalie') + '</li>\n';
                                        html += '</ul>\n';

                                        html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                        html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                                        html += '<h3>Dados do Advogado</h3>\n';
                                        html += '<ul class="resumo-adv">\n';
                                        html += '<li><big>' + advogado.nome + '</big></li>\n';
                                        html += '<li>Avaliação \n';
                                        html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                                        html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                                        html += '</li>\n';
                                        html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + advogado.oab + '-' + advogado.seccional + '</a></li>\n';
                                        html += '<li>Endereço: ' + advogado.endereco + ' - ' + advogado.bairro + ' - ' + advogado.seccional + '</li>\n';
                                        html += '<li>Telefone: <a href="tel:+551199880099">' + advogado.telefone + '</a></li>\n';
                                        html += '<li>Celular: <a href="tel:+551199880099">' + advogado.celular + '</a> com WhatsApp</li>\n';
                                        html += '<li>Email: <a href="mailto:' + advogado.email + '">' + advogado.email + '</a></li>\n';
                                        html += '<li>Site: <a href="' + advogado.site + '" target="_blank">' + advogado.site + '</a></li>\n';
                                        html += '<li><strong>Apresentação</strong></li>\n';
                                        html += '<li>' + advogado.apresentacao + '</li>\n';
                                        html += '</ul>\n';
                                        html += '<div class="col-xs-4"></div>\n';
                                        html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                                        html += '<div class="col-xs-4"></div>\n';
                                        html += '</div>\n';
                                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                        html += '</div>\n';
                                    }
                                }
                            }
                            else if (audiencia.status == 5) {
                                for (k = 0; k < audiencia.convites.length; k++) {
                                    var advogado = audiencia.convites[k];

                                    if (advogado.status == 5) {
                                        var dv = audiencia.desistencias[0].nome + ':<br />' + audiencia.desistencias[0].motivo + '<br />';

                                        html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                        html += '<div class="desistencia"><a href="#" data-value="' + dv + '">Desistência <i class="fa fa-circle" aria-hidden="true"></i></a></div>\n';

                                        html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                        html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                                        html += '<h3>Dados do Advogado</h3>\n';
                                        html += '<ul class="resumo-adv">\n';
                                        html += '<li><big>' + advogado.nome + '</big></li>\n';
                                        html += '<li>Avaliação \n';
                                        html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                                        html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                                        html += '</li>\n';
                                        html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + advogado.oab + '-' + advogado.seccional + '</a></li>\n';
                                        html += '<li>Endereço: ' + advogado.endereco + ' - ' + advogado.bairro + ' - ' + advogado.seccional + '</li>\n';
                                        html += '<li>Telefone: <a href="tel:+551199880099">' + advogado.telefone + '</a></li>\n';
                                        html += '<li>Celular: <a href="tel:+551199880099">' + advogado.celular + '</a> com WhatsApp</li>\n';
                                        html += '<li>Email: <a href="mailto:' + advogado.email + '">' + advogado.email + '</a></li>\n';
                                        html += '<li>Site: <a href="' + advogado.site + '" target="_blank">' + advogado.site + '</a></li>\n';
                                        html += '<li><strong>Apresentação</strong></li>\n';
                                        html += '<li>' + advogado.apresentacao + '</li>\n';
                                        html += '</ul>\n';
                                        html += '<div class="col-xs-4"></div>\n';
                                        html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                                        html += '<div class="col-xs-4"></div>\n';
                                        html += '</div>\n';
                                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                        html += '</div>\n';
                                    }
                                }
                            }
                            else if (audiencia.status == 6) {
                                html += '<div class="desistencia">Cancelado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                        }
                        else if (audiencia.status == 0) {
                            html += '<div class="adv-item"><a href="#" onclick="return false">0 aceites</a></div>\n';
                            html += '<div class="aguard-resp">Aguardando Respostas <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                        }
                        else if (audiencia.status == 1) {
                            var x = 0;
                            var y = audiencia.convites.length;
                            for (k = 0; k < audiencia.convites.length; k++) {
                                var advogado = audiencia.convites[k];
                                if (advogado.status == 1)
                                    x++;
                            }

                            // ainda não contratado
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + (x) + ' ' + (x == 1 ? 'aceite' : 'aceites') + '</a></div>\n';
                            html += '<div class="contrata-pend"><a href="#" data-value="' + x + ' aceite' + (x > 1 ? 's' : '') + ' de ' + (y) + ' convite' + (y > 1 ? 's' : '') + '">Contratação Pendente <i class="fa fa-circle" aria-hidden="true"></i></a></div>\n';

                            html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<h3>Convites Aceitos</h3>\n';
                            html += '<div class="col-xs-12">\n';
                            html += '<h4>' + audiencia.data + '</h4>\n';
                            html += '</div>\n';
                            html += '<div class="ofertas col-xs-12 adv-aceites">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-passada">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item">' + audiencia.materia + '</div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '</div>\n';
                            html += '</div>\n';

                            for (k = 0; k < audiencia.convites.length; k++) {
                                var convite = audiencia.convites[k];

                                if (convite.status == 1) {
                                    html += '<div class="col-xs-12 adv-aceites">\n';
                                    html += '<div class="col-xs-1">\n';
                                    html += '<input type="radio" name="chkAdvogado' + audiencia.id + '" value="' + convite.id + '" />\n';
                                    html += '</div>\n';
                                    html += '<div class="col-xs-11">\n';
                                    html += '<ul class="resumo-adv-oferta">\n';
                                    html += '<li><big>' + convite.nome + '</big></li>\n';
                                    html += '<li>Avaliação \n';
                                    html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2\n';
                                    html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i>2\n';
                                    html += '</li>\n';
                                    html += '</ul>\n';
                                    html += '</div>\n';
                                    html += '</div>\n';
                                }
                            }

                            html += '<div class="col-xs-12">&nbsp;</div>\n';
                            html += '<div class="col-xs-12">\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                            html += '<div class="col-xs-4"><a href="#" class="close" onclick="contratar(' + audiencia.id + '); return false;">CONTRATAR</a></div>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 3 || audiencia.status == 4) {
                            for (k = 0; k < audiencia.convites.length; k++) {
                                var advogado = audiencia.convites[k];

                                if (advogado.status == 3) {
                                    html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                    html += '<div class="contrata-pend">Contratação à confirmar <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                                }
                                else if (advogado.status == 4) {
                                    html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                    html += '<div class="contratado">Contratado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                                }

                                html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                                html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                                html += '<h3>Dados do Advogado</h3>\n';
                                html += '<ul class="resumo-adv">\n';
                                html += '<li><big>' + advogado.nome + '</big></li>\n';
                                html += '<li>Avaliação \n';
                                html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                                html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                                html += '</li>\n';
                                html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + advogado.oab + '-' + advogado.seccional + '</a></li>\n';
                                html += '<li>Endereço: ' + advogado.endereco + ' - ' + advogado.bairro + ' - ' + advogado.seccional + '</li>\n';
                                html += '<li>Telefone: <a href="tel:+551199880099">' + advogado.telefone + '</a></li>\n';
                                html += '<li>Celular: <a href="tel:+551199880099">' + advogado.celular + '</a> com WhatsApp</li>\n';
                                html += '<li>Email: <a href="mailto:' + advogado.email + '">' + advogado.email + '</a></li>\n';
                                html += '<li>Site: <a href="' + advogado.site + '" target="_blank">' + advogado.site + '</a></li>\n';
                                html += '<li><strong>Apresentação</strong></li>\n';
                                html += '<li>' + advogado.apresentacao + '</li>\n';
                                html += '</ul>\n';
                                html += '<div class="col-xs-4"></div>\n';
                                html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                                html += '<div class="col-xs-4"></div>\n';
                                html += '</div>\n';
                                html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                html += '</div>\n';
                            }
                        }
                        else if (audiencia.status == 5) {
                            for (k = 0; k < audiencia.convites.length; k++) {
                                var advogado = audiencia.convites[k];

                                if (advogado.status == 5) {
                                    var dv = audiencia.desistencias[0].nome + ':<br />' + audiencia.desistencias[0].motivo + '<br />';

                                    html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                    html += '<div class="desistencia"><a href="#" data-value="' + dv + '">Desistência <i class="fa fa-circle" aria-hidden="true"></i></a></div>\n';

                                    html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                                    html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                    html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                                    html += '<h3>Dados do Advogado</h3>\n';
                                    html += '<ul class="resumo-adv">\n';
                                    html += '<li><big>' + advogado.nome + '</big></li>\n';
                                    html += '<li>Avaliação \n';
                                    html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                                    html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                                    html += '</li>\n';
                                    html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + advogado.oab + '-' + advogado.seccional + '</a></li>\n';
                                    html += '<li>Endereço: ' + advogado.endereco + ' - ' + advogado.bairro + ' - ' + advogado.seccional + '</li>\n';
                                    html += '<li>Telefone: <a href="tel:+551199880099">' + advogado.telefone + '</a></li>\n';
                                    html += '<li>Celular: <a href="tel:+551199880099">' + advogado.celular + '</a> com WhatsApp</li>\n';
                                    html += '<li>Email: <a href="mailto:' + advogado.email + '">' + advogado.email + '</a></li>\n';
                                    html += '<li>Site: <a href="' + advogado.site + '" target="_blank">' + advogado.site + '</a></li>\n';
                                    html += '<li><strong>Apresentação</strong></li>\n';
                                    html += '<li>' + advogado.apresentacao + '</li>\n';
                                    html += '</ul>\n';
                                    html += '<div class="col-xs-4"></div>\n';
                                    html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                                    html += '<div class="col-xs-4"></div>\n';
                                    html += '</div>\n';
                                    html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                    html += '</div>\n';
                                }
                            }
                        }
                        else if (audiencia.status == 6) {
                            //html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                            html += '<div class="desistencia">Cancelado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                        }

                        html += '</div>\n';
                        html += '</div>\n';
                    }
                }
            }

            var div = $('#resultado');
            div.html(html);
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function contratar(id) {
    var convite = $('input[name="chkAdvogado' + id + '"]:checked').val();

    if (convite == null) {
        alert('Selecione o advogado que deseja contratar.');
        return;
    }

    $.ajax({
        url: Url + '/api/ofertas/contratar',
        data: { convite: convite },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado contratado.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function curti(id) {
    $.ajax({
        url: Url + '/api/convites/curti',
        data: { convite: id },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado curtido.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function indico(id) {
    $.ajax({
        url: Url + '/api/convites/indico',
        data: { convite: id },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado indicado.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}