﻿var IDAudiencia = 0;

jQuery(function () {
    showDialog();
    listar({});

    $('#chkAdvogadoTodos').click(function () {
        var checked = $(this).prop('checked');
        $('input[name="chkAdvogado"]').each(function () {
            $(this).prop("checked", checked);
        });
    });
});

function days(valor) {

    switch (valor) {
        case "semana": $('#btnDays').html('Semana <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "ontem": $('#btnDays').html('Ontem <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "hoje": $('#btnDays').html('Hoje <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "amanha": $('#btnDays').html('Amanhã <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
    }

    listar({ filtro: valor });
    return false;
}

function months(valor) {
    switch (valor) {
        case 1: $('#btnMonths').html('janeiro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 2: $('#btnMonths').html('fevereiro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 3: $('#btnMonths').html('março <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 4: $('#btnMonths').html('abril <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 5: $('#btnMonths').html('maio <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 6: $('#btnMonths').html('junho <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 7: $('#btnMonths').html('julho <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 8: $('#btnMonths').html('agosto <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 9: $('#btnMonths').html('setembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 10: $('#btnMonths').html('outubro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 11: $('#btnMonths').html('novembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 12: $('#btnMonths').html('dezembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
    }

    mes = valor;
    listar({ filtro: 'mes', ano: ano, mes: mes });
    return false;
}

function years(valor) {
    $('#btnYears').html(valor + ' <i class="fa fa-angle-down" aria-hidden="true"></i>');

    ano = valor;
    listar({ filtro: 'ano', ano: ano, mes: mes });
    return false;
}

function listar(data) {
    $.ajax({
        url: Url + '/api/agenda',
        data: data,
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            var html = '';
            $('#ddlAno').html('');

            for (i = 0; i < data.anos.length; i++) {
                $('#ddlAno').append('<li><a href="#" onclick="return years(' + data.anos[i] + ')">' + data.anos[i] + '</a></li>');
            }

            if (data.total == 0) {
                html += '<div class="ofertas col-xs-12">\n';
                html += '<div class="sem-audiencia">\n';
                html += '<p>Não há itens na Agenda.</p>\n';
                html += '<a href="' + Url + '/nova-audiencia">CADASTRAR AUDIÊNCIA</a>\n';
                html += '</div>\n';
                html += '</div>\n';
            }
            else {
                for (i = 0; i < data.datas.length; i++) {
                    html += '<div class="col-xs-12">\n';
                    html += '<h3>' + data.datas[i].data + '</h3>\n';
                    html += '</div>\n';

                    for (j = 0; j < data.datas[i].audiencias.length; j++) {
                        var audiencia = data.datas[i].audiencias[j];
                        var tipo = '';

                        if (audiencia.status == -1)
                            tipo = 'P';
                        else if (audiencia.convites.length > 0)
                            tipo = 'O';
                        else
                            tipo = 'C';

                        html += '<div class="ofertas col-xs-12">\n';
                        html += '<div class="col-xs-4">\n';
                        html += '<div class="' + (audiencia.vencido ? 'hora-passada' : 'hora-futura') + '">' + audiencia.hora + '</div>\n';
                        html += '</div>\n';
                        html += '<div class="col-xs-8">\n';
                        html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                        html += '<div class="nat-item">' + audiencia.area + '</div>\n';

                        if (tipo == 'P') {
                            html += '<div class="contratado">Agendada <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                        }
                        else if (tipo == 'O') {
                            if (audiencia.status == 0) {
                                html += '<div class="adv-item"><a href="#" onclick="return false">0 aceites</a></div>\n';
                                html += '<div class="aguard-resp">Aguardando Respostas <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                            else if (audiencia.status == 1) {
                                var x = 0;
                                for (k = 0; k < audiencia.convites.length; k++) {
                                    var advogado = audiencia.convites[k];
                                    if (advogado.status == 1)
                                        x++;
                                }

                                html += '<div class="adv-item"><a href="#" onclick="return false">' + (x) + ' ' + (x == 1 ? 'aceite' : 'aceites') + '</a></div>\n';
                                html += '<div class="contrata-pend">Contratação Pendente <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                            else if (audiencia.status == 3 || audiencia.status == 4) {
                                for (k = 0; k < audiencia.convites.length; k++) {
                                    var advogado = audiencia.convites[k];

                                    if (advogado.status == 3) {
                                        html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                        html += '<div class="contrata-pend">Contratação à confirmar <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                                    }

                                    if (advogado.status == 4) {
                                        html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                        html += '<div class="contratado">Contratado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                                    }

                                    html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                                    html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                    html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                                    html += '<h3>Dados do Advogado</h3>\n';
                                    html += '<ul class="resumo-adv">\n';
                                    html += '<li><big>' + advogado.nome + '</big></li>\n';
                                    html += '<li>Avaliação \n';
                                    html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                                    html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                                    html += '</li>\n';
                                    html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + advogado.oab + '-' + advogado.seccional + '</a></li>\n';
                                    html += '<li>Endereço: ' + advogado.endereco + ' - ' + advogado.bairro + ' - ' + advogado.seccional + '</li>\n';
                                    html += '<li>Telefone: <a href="tel:+551199880099">' + advogado.telefone + '</a></li>\n';
                                    html += '<li>Celular: <a href="tel:+551199880099">' + advogado.celular + '</a> com WhatsApp</li>\n';
                                    html += '<li>Email: <a href="mailto:' + advogado.email + '">' + advogado.email + '</a></li>\n';
                                    html += '<li>Site: <a href="' + advogado.site + '" target="_blank">' + advogado.site + '</a></li>\n';
                                    html += '<li><strong>Apresentação</strong></li>\n';
                                    html += '<li>' + advogado.apresentacao + '</li>\n';
                                    html += '</ul>\n';
                                    html += '<div class="col-xs-4"></div>\n';
                                    html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                                    html += '<div class="col-xs-4"></div>\n';
                                    html += '</div>\n';
                                    html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                    html += '</div>\n';
                                }
                            }
                            else if (audiencia.status == 5) {
                                for (k = 0; k < audiencia.convites.length; k++) {
                                    var advogado = audiencia.convites[k];

                                    if (advogado.status == 5) {
                                        var dv = advogado.nome + ':<br />' + advogado.motivo + '<br />';

                                        html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">' + advogado.nome + '</a></div>\n';
                                        html += '<div class="desistencia"><a href="#" data-value="' + dv + '">Desistência <i class="fa fa-circle" aria-hidden="true"></i></a></div>\n';

                                        html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                        html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                                        html += '<h3>Dados do Advogado</h3>\n';
                                        html += '<ul class="resumo-adv">\n';
                                        html += '<li><big>' + advogado.nome + '</big></li>\n';
                                        html += '<li>Avaliação \n';
                                        html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                                        html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                                        html += '</li>\n';
                                        html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + advogado.oab + '-' + advogado.seccional + '</a></li>\n';
                                        html += '<li>Endereço: ' + advogado.endereco + ' - ' + advogado.bairro + ' - ' + advogado.seccional + '</li>\n';
                                        html += '<li>Telefone: <a href="tel:+551199880099">' + advogado.telefone + '</a></li>\n';
                                        html += '<li>Celular: <a href="tel:+551199880099">' + advogado.celular + '</a> com WhatsApp</li>\n';
                                        html += '<li>Email: <a href="mailto:' + advogado.email + '">' + advogado.email + '</a></li>\n';
                                        html += '<li>Site: <a href="' + advogado.site + '" target="_blank">' + advogado.site + '</a></li>\n';
                                        html += '<li><strong>Apresentação</strong></li>\n';
                                        html += '<li>' + advogado.apresentacao + '</li>\n';
                                        html += '</ul>\n';
                                        html += '<div class="col-xs-4"></div>\n';
                                        html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                                        html += '<div class="col-xs-4"></div>\n';
                                        html += '</div>\n';
                                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                                        html += '</div>\n';
                                    }
                                }
                            }
                            else if (audiencia.status == 6) {
                                var dv = audiencia.cancelamento + '<br />';
                                html += '<div class="desistencia"><a href="#" data-value="' + dv + '">Cancelado <i class="fa fa-circle" aria-hidden="true"></i></a></div>\n';
                            }
                        }
                        else if (tipo = 'C') {
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            if (audiencia.status == 0) {
                                html += '<div class="aguard-resp">Aguardando Respostas <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                            else if (audiencia.status == 1) {
                                html += '<div class="aguard-resp">Aguardando Resposta <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                            else if (audiencia.status == 3) {
                                html += '<div class="contrata-pend">Contratação à confirmar <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                            else if (audiencia.status == 4) {
                                html += '<div class="contratado">Contratado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            }
                            else if (audiencia.status == 6) {
                                var dv = audiencia.cancelamento + '<br />';
                                html += '<div class="desistencia"><a href="#" data-value="' + dv + '">Cancelado <i class="fa fa-circle" aria-hidden="true"></i></a></div>\n';
                            }

                            // modal contratante
                            html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<h3>Dados do Contratante</h3>\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>' + audiencia.contratante.nome + '</big></li>\n';
                            html += '<li>Avaliação \n';
                            html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>2\n';
                            html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i>2\n';
                            html += '</li>\n';
                            html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + audiencia.contratante.oab + '-' + audiencia.contratante.seccional + '</a></li>\n';

                            if (audiencia.status > 3) {
                                html += '<li>Endereço: ' + audiencia.contratante.endereco + ' - ' + audiencia.contratante.bairro + ' - ' + audiencia.contratante.seccional + '</li>\n';
                                html += '<li>Telefone: <a href="tel:+551199880099">' + audiencia.contratante.telefone + '</a></li>\n';
                                html += '<li>Celular: <a href="tel:+551199880099">' + audiencia.contratante.celular + '</a> com WhatsApp</li>\n';
                                html += '<li>Email: <a href="mailto:' + audiencia.contratante.email + '">' + audiencia.contratante.email + '</a></li>\n';
                                html += '<li>Site: <a href="' + audiencia.contratante.site + '" target="_blank">' + audiencia.contratante.site + '</a></li>\n';
                            }

                            html += '<li><strong>Apresentação</strong></li>\n';
                            html += '<li>' + audiencia.contratante.apresentacao + '</li>\n';
                            html += '</ul>\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';
                        }

                        html += '</div>\n';
                        html += '</div>\n';

                        // modal audiência
                        html += '<div class="modalDialog col-xs-12" id="modal-aud' + audiencia.id + '">\n';
                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                        html += '<h3>Detalhes da Audiência</h3>\n';
                        html += '<ul class="resumo-adv">\n';
                        html += '<li><big>' + audiencia.materia + '</big></li>\n';
                        html += '<li>' + audiencia.area + '</li>\n';
                        html += '<li>Comarca: <strong>' + audiencia.forum.cidade + '</strong></li>\n';
                        html += '<li>Fórum: <strong>' + audiencia.forum.nome + '</strong></li>\n';
                        html += '<li>Parte que Representa: <strong>' + audiencia.parte_representa + '</strong></li>\n';
                        html += '<li>Partes da Audiência: <strong>' + audiencia.parte_audiencia + '</strong></li>\n';
                        html += '<li>Valor a Receber: <strong>R$ ' + audiencia.valor + '</strong></li>\n';
                        html += '<li>Descrição: <strong>' + audiencia.descricao + '</strong></li>\n';
                        html += '<li>&nbsp;</li>\n';
                        html += '<li>Local: <strong>' + audiencia.forum.endereco + ' - ' + audiencia.forum.bairro + ' - ' + audiencia.forum.cidade + ' - ' + audiencia.forum.estado + '</strong></li>\n';
                        html += '</ul>\n';
                        html += '<div class="data-hora">Data e Horário: ' + audiencia.data + ' às ' + audiencia.hora + 'hs</div>\n';
                        html += '<div class="col-xs-4">';

                        if (audiencia.dono) {
                            if (audiencia.status >= 0 && audiencia.status != 5 && audiencia.status != 6)
                                html += '<a href="#modal-canc" class="close" onclick="cancelar(' + audiencia.id + ')">CANCELAR AUDIÊNCIA</a>\n';
                        }

                        html += '</div>\n';
                        html += '<div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>\n';

                        if (audiencia.status == -1)
                            html += '<div class="col-xs-4"><a href="#ofertar' + audiencia.id + '" class="close" onclick="ofertar(' + audiencia.id + ')">OFERTAR</a></div>\n';

                        html += '</div>\n';
                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html += '</div>\n';
                    }
                }
            }

            var div = $('#resultados');
            div.html(html);
            closeDialog();
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function curti(id) {
    showDialog();
    $.ajax({
        url: Url + '/api/ofertas/curti',
        data: { audiencia: id },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado curtido.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function indico(id) {
    showDialog();
    $.ajax({
        url: Url + '/api/ofertas/indico',
        data: { audiencia: id },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado indicado.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function cancelar(id) {
    IDAudiencia = id;
    $('textarea[name="txtMotivo"]').val("");
}

function cancelando() {
    $.ajax({
        url: Url + '/api/ofertas/cancelar',
        data: { audiencia: IDAudiencia, motivo: $('textarea[name="txtMotivo"]').val() },
        success: function (data) {
            data = eval('(' + data + ')');
    
            if (data.error) {
                alert(data.message);
                return;
            }
    
            window.location = window.location.toString();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            switch (xhr.status) {
                case 404:
                    alert('Página não encontrada.');
                    break;
                default:
                    alert(thrownError);
                    break;
            }
        }
    });
}

function ofertar(id) {
    IDAudiencia = id;
}

function enviar() {
    var advs = [];

    $('input[name="chkAdvogado"]').each(function () {
        if ($(this).prop("checked")) {
            advs.push($(this).val());
        }
    });

    if (advs.length == 0) {
        alert('Selecione ao menos 1 advogado.');
        return false;
    }

    showDialog();
    $.ajax({
        url: Url + '/api/agenda-ofertar',
        data: { audiencia: IDAudiencia, advogados: advs.toString() },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                closeDialog();
                alert(data.message);
                return;
            }

            alert('Audiência ofertada com sucesso.');
            listar({});
        },
        error: function (xhr, ajaxOptions, thrownError) {
            closeDialog();
            switch (xhr.status) {
                case 404: alert('Página não encontrada.'); break;
                default: alert(thrownError); break;
            }
        }
    });
}

function showDialog() {
    $.blockUI({
        message: "Aguarde...",
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}

function closeDialog() {
    setTimeout($.unblockUI, 1);
}

function confCancelar() {
    var motivo = $('textarea[name="txtMotivo"]').val();
    if (motivo == "") {
        alert('Obrigatório preencher o motivo do cancelamento.');
        return false;
    }
    else
        return true;
}