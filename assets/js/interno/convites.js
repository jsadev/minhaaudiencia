﻿jQuery(function () {
    showDialog();
    listar({});
});

function days(valor) {

    switch (valor) {
        case "semana": $('#btnDays').html('Semana <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "ontem": $('#btnDays').html('Ontem <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "hoje": $('#btnDays').html('Hoje <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case "amanha": $('#btnDays').html('Amanhã <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
    }

    listar({ filtro: valor });
    return false;
}

function months(valor) {
    switch (valor) {
        case 1: $('#btnMonths').html('janeiro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 2: $('#btnMonths').html('fevereiro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 3: $('#btnMonths').html('março <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 4: $('#btnMonths').html('abril <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 5: $('#btnMonths').html('maio <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 6: $('#btnMonths').html('junho <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 7: $('#btnMonths').html('julho <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 8: $('#btnMonths').html('agosto <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 9: $('#btnMonths').html('setembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 10: $('#btnMonths').html('outubro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 11: $('#btnMonths').html('novembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
        case 12: $('#btnMonths').html('dezembro <i class="fa fa-angle-down" aria-hidden="true"></i>'); break;
    }

    mes = valor;
    listar({ filtro: 'mes', ano: ano, mes: mes });
    return false;
}

function years(valor) {
    $('#btnYears').html(valor + ' <i class="fa fa-angle-down" aria-hidden="true"></i>');

    ano = valor;
    listar({ filtro: 'ano', ano: ano, mes: mes });
    return false;
}

function listar(data) {
    $.ajax({
        url: Url + '/api/convites',
        data: data,
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            var html = '';
            $('#ddlAno').html('');

            for (i = 0; i < data.anos.length; i++) {
                $('#ddlAno').append('<li><a href="#" onclick="return years(' + data.anos[i] + ')">' + data.anos[i] + '</a></li>');
            }

            if (data.total == 0) {
                html += '<div class="ofertas col-xs-12">\n';
                html += '<div class="sem-audiencia">\n';
                html += '<p>Não há Convites.</p>\n';
                html += '</div>\n';
                html += '</div>\n';
            }
            else {
                for (i = 0; i < data.datas.length; i++) {
                    html += '<div class="col-xs-12">\n';
                    html += '<h3>' + data.datas[i].data + '</h3>\n';
                    html += '</div>\n';

                    for (j = 0; j < data.datas[i].audiencias.length; j++) {
                        var audiencia = data.datas[i].audiencias[j];

                        if (audiencia.vencido) {
                            html += '<div class="convites col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-passada">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<ul class="aval">\n';
                            html += '<li><a href="" class="ico-curti-' + (audiencia.curti ? 'on' : 'off') + '" ' + (audiencia.curti ? 'onclick="return false"' : 'onclick="curti(' + audiencia.id + ')"') + '><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></li>\n';
                            html += '<li><a href="" class="ico-recomendo-' + (audiencia.indico ? 'on' : 'off') + '" ' + (audiencia.indico ? 'onclick="return false"' : 'onclick="indico(' + audiencia.id + ')"') + '><i class="fa fa-hand-o-right" aria-hidden="true"></i></a></li>\n';
                            html += '<li class="aval-off">' + (audiencia.curti && audiencia.indico ? 'avaliado' : 'avalie') + '</li>\n';
                            html += '</ul>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 0) {
                            html += '<div class="ofertas col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-futura">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<ul class="aval">\n';
                            html += '<li><a href="" class="ico-curti-on" onclick="disponivel(' + audiencia.convite + '); return false;">ESTOU DISPONÍVEL</a></li>\n';
                            html += '<li><a href="#modal-indisp' + audiencia.id + '" class="ico-curti-on">INDISPONÍVEL</a></li>\n';
                            html += '</ul>\n';
                            html += '</div>\n';
                            html += '</div>\n';

                            // modal desistência
                            html += '<div class="modalDialog col-xs-12" id="modal-indisp' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>Indisponível</big></li>\n';
                            html += '<li>Deseja confirmar a indisponibilidade?</li>\n';
                            html += '</ul>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                            html += '<div class="col-xs-8"><a href="" class="ico-curti-on" onclick="indisponivel(' + audiencia.convite + '); return false;">CONFIRMAR</a></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 1) {
                            html += '<div class="ofertas col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-futura">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<div class="contratado">aguardando confirmação <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 3) {
                            html += '<div class="convites col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-futura">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<div class="col-xs-12">\n';
                            html += '<div class="contrata-pend">aguardando confirmação <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12">\n';
                            html += '<ul class="aval">\n';
                            html += '<li><a href="#modal-desist' + audiencia.id + '" class="ico-curti-on">DESISTIR</a></li>\n';
                            html += '<li><a href="#modal-conf' + audiencia.id + '" class="ico-curti-on">CONFIRMAR</a></li>\n';
                            html += '</ul>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                            html += '</div>\n';

                            // modal desistência
                            html += '<div class="modalDialog col-xs-12" id="modal-desist' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>Desistir</big></li>\n';
                            html += '<li>Informe o motivo de sua desistência:</li>\n';
                            html += '<li>\n';
                            html += '<textarea name="txtMotivo' + audiencia.id + '" placeholder="insira o motivo"></textarea></li>\n';
                            html += '</ul>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                            html += '<div class="col-xs-8"><a href="#modal-conf-desist' + audiencia.id + '" class="close" onclick="return confDesistencia(' + audiencia.id + ')">CONFIRMAR DESISTÊNCIA</a></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';

                            // modal confirmação desistência
                            html += '<div class="modalDialog col-xs-12" id="modal-conf-desist' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>Tem certeza que deseja DESISTIR da AUDIÊNCIA?</big></li>\n';
                            html += '</ul>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">CANCELAR</a></div>\n';
                            html += '<div class="col-xs-8"><a href="#modal-conf-desist-ok' + audiencia.id + '" class="close" onclick="desistir(' + audiencia.id + ',' + audiencia.convite + ')">CONFIRMAR</a></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';

                            // modal confirmação desistência sucesso
                            html += '<div class="modalDialog col-xs-12" id="modal-conf-desist-ok' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>DESISTÊNCIA confirmada com Sucesso.</big></li>\n';
                            html += '</ul>\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';

                            // modal confirmação
                            html += '<div class="modalDialog col-xs-12" id="modal-conf' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<h3>Confirme sua Contratação<br />\n';
                            html += '<span>Você foi selecionado para uma audiência, confirme sua contratação</span>\n';
                            html += '</h3>\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>' + audiencia.materia + '</big></li>\n';
                            html += '<li>' + audiencia.area + '</li>\n';
                            html += '<li>Comarca: <strong>' + audiencia.forum.cidade + '</strong></li>\n';
                            html += '<li>Fórum: <strong>' + audiencia.forum.nome + '</strong></li>\n';
                            html += '<li>Parte que Representa: <strong>' + audiencia.parte_representa + '</strong></li>\n';
                            html += '<li>Partes da Audiência: <strong>' + audiencia.parte_audiencia + '</strong></li>\n';
                            html += '<li>Valor a Receber: <strong>R$ ' + audiencia.valor + '</strong></li>\n';
                            html += '<li>Descrição: <strong>' + audiencia.descricao + '</strong></li>\n';
                            html += '<li>&nbsp;</li>\n';
                            html += '<li>Local: <strong>' + audiencia.forum.endereco + ' - ' + audiencia.forum.bairro + ' - ' + audiencia.forum.cidade + ' - ' + audiencia.forum.estado + '</strong></li>\n';
                            html += '</ul>\n';
                            html += '<div class="data-hora">Data e Horário: ' + audiencia.data + ' às ' + audiencia.hora + 'hs</div>\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                            html += '<div class="col-xs-4"><a href="#modal-conf-aceite-ok' + audiencia.id + '" class="close" onclick="aceitar(' + audiencia.convite + ')">ACEITAR</a></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';

                            // modal confirmação sucesso.
                            html += '<div class="modalDialog col-xs-12" id="modal-conf-aceite-ok' + audiencia.id + '">\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                            html += '<ul class="resumo-adv">\n';
                            html += '<li><big>ACEITE enviado com Sucesso.</big></li>\n';
                            html += '<li>Aguarde o Contato do Contratante.</li>\n';
                            html += '</ul>\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '<div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>\n';
                            html += '<div class="col-xs-4"></div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 4) {
                            html += '<div class="convites col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-futura">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<div class="col-xs-12">\n';
                            html += '<div class="contratado">contratado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 5) {
                            html += '<div class="ofertas col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-futura">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<div class="desistencia">Desistência <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                        }
                        else if (audiencia.status == 6) {
                            html += '<div class="ofertas col-xs-12">\n';
                            html += '<div class="col-xs-4">\n';
                            html += '<div class="hora-futura">' + audiencia.hora + '</div>\n';
                            html += '</div>\n';
                            html += '<div class="col-xs-8">\n';
                            html += '<div class="tit-item"><a href="#modal-aud' + audiencia.id + '">' + audiencia.materia + '</a></div>\n';
                            html += '<div class="nat-item">' + audiencia.area + '</div>\n';
                            html += '<div class="adv-item"><a href="#modal-adv' + audiencia.id + '">por ' + audiencia.contratante.nome + '</a></div>\n';
                            html += '<div class="desistencia">Cancelado <i class="fa fa-circle" aria-hidden="true"></i></div>\n';
                            html += '</div>\n';
                            html += '</div>\n';
                        }

                        // modal audiência
                        html += '<div class="modalDialog col-xs-12" id="modal-aud' + audiencia.id + '">\n';
                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                        html += '<h3>Detalhes da Audiência</h3>\n';
                        html += '<ul class="resumo-adv">\n';
                        html += '<li><big>' + audiencia.materia + '</big></li>\n';
                        html += '<li>' + audiencia.area + '</li>\n';
                        html += '<li>Comarca: <strong>' + audiencia.forum.cidade + '</strong></li>\n';
                        html += '<li>Fórum: <strong>' + audiencia.forum.nome + '</strong></li>\n';
                        html += '<li>Parte que Representa: <strong>' + audiencia.parte_representa + '</strong></li>\n';
                        html += '<li>Partes da Audiência: <strong>' + audiencia.parte_audiencia + '</strong></li>\n';
                        html += '<li>Valor a Receber: <strong>R$ ' + audiencia.valor + '</strong></li>\n';
                        html += '<li>Descrição: <strong>' + audiencia.descricao + '</strong></li>\n';
                        html += '<li>&nbsp;</li>\n';
                        html += '<li>Local: <strong>' + audiencia.forum.endereco + ' - ' + audiencia.forum.bairro + ' - ' + audiencia.forum.cidade + ' - ' + audiencia.forum.estado + '</strong></li>\n';
                        html += '</ul>\n';
                        html += '<div class="data-hora">Data e Horário: ' + audiencia.data + ' às ' + audiencia.hora + 'hs</div>\n';
                        html += '<div class="col-xs-4"></div>\n';
                        html += '<div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>\n';
                        html += '</div>\n';
                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html += '</div>\n';

                        // modal contratante
                        html += '<div class="modalDialog col-xs-12" id="modal-adv' + audiencia.id + '">\n';
                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                        html += '<h3>Dados do Contratante</h3>\n';
                        html += '<ul class="resumo-adv">\n';
                        html += '<li><big>' + audiencia.contratante.nome + '</big></li>\n';
                        html += '<li>Avaliação \n';
                        html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' + audiencia.contratante.curtidas + '\n';
                        html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i>' + audiencia.contratante.indicacoes + '\n';
                        html += '</li>\n';
                        html += '<li>OAB: <a href="http://cna.oab.org.br/" target="_blank">' + audiencia.contratante.oab + '-' + audiencia.contratante.seccional + '</a></li>\n';
                        html += '<li><strong>Apresentação</strong></li>\n';
                        html += '<li>' + audiencia.contratante.apresentacao + '</li>\n';
                        html += '</ul>\n';
                        html += '<div class="col-xs-4"></div>\n';
                        html += '<div class="col-xs-4"><a href="#close" class="close">FECHAR</a></div>\n';
                        html += '</div>\n';
                        html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                        html += '</div>\n';
                    }
                }
            }

            var div = $('#resultados');
            div.html(html);
            closeDialog();
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function disponivel(convite) {
    showDialog();
    $.ajax({
        url: Url + '/api/convites/disponivel',
        data: { convite: convite },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function indisponivel(convite) {
    showDialog();
    $.ajax({
        url: Url + '/api/convites/indisponivel',
        data: { convite: convite },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function aceitar(convite) {
    showDialog();
    $.ajax({
        url: Url + '/api/convites/aceitar',
        data: { convite: convite },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function desistir(id, convite) {
    showDialog();
    var motivo = $('textarea[name="txtMotivo' + id + '"]').val();

    $.ajax({
        url: Url + '/api/convites/desistir',
        data: { convite: convite, motivo: motivo },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function curti(id) {
    showDialog();
    $.ajax({
        url: Url + '/api/ofertas/curti',
        data: { audiencia: id },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado curtido.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function indico(id) {
    showDialog();
    $.ajax({
        url: Url + '/api/ofertas/indico',
        data: { audiencia: id },
        success: function (data) {
            data = eval('(' + data + ')');
            if (data.error) {
                alert(data.message);
                return;
            }

            alert('Advogado indicado.');
            listar({});
        },
        error: function () {
            alert('Falha ao carregar objeto.');
        },
        statusCod: {
            404: function () {
                alert('Página não encontrada.');
            }
        }
    });
}

function showDialog() {
    $.blockUI({
        message: "Aguarde...",
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}

function closeDialog() {
    setTimeout($.unblockUI, 1);
}

function confDesistencia(id) {
    var motivo = $('textarea[name="txtMotivo' + id + '"]').val();
    if (motivo == "") {
        alert('Obrigatório preencher o motivo da desistência.');
        return false;
    }
    else
        return true;
}