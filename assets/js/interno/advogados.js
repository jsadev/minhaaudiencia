﻿$.ajax({
    url: Url + '/api/advogados',
    data: {},
    success: function (data) {
        data = eval('(' + data + ')');
        if (data.error) {
            alert(data.message);
            return;
        }

        var html = '';

        if (data.total == 0) {
            html += '<div class="ofertas col-xs-12">\n';
            html += '<div class="sem-audiencia">\n';
            html += '<p>Não há advogados.</p>\n';
            html += '</div>\n';
            html += '</div>\n';
        }
        else {
            for (i = 0 ; i < data.total; i++) {
                var advogado = data.advogados[i];

                html += '<div class="advogados col-xs-12">\n';
                html += '<div class="col-xs-6">\n';
                html += '<div class="adv-item"><a href="#modal-adv' + advogado.id + '">' + advogado.nome + '</a></div>\n';
                html += '<div class="adv-item">OAB: ' + advogado.oab + '-' + advogado.seccional + '</div>\n';
                html += '</div>\n';
                html += '<div class="col-xs-6">\n';
                html += '<ul class="aval">\n';
                html += '<li><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '</li>\n';
                html += '<li><i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '</li>\n';
                html += '</ul>\n';
                html += '</div>\n';
                html += '</div>\n';

                html += '<div class="modalDialog col-xs-12" id="modal-adv' + advogado.id + '">\n';
                html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                html += '<div class="col-xs-12 col-sm-6 col-md-4 pop-adv">\n';
                html += '<h3>Dados do Advogado</h3>\n';
                html += '<ul class="resumo-adv">\n';
                html += '<li><big>' + advogado.nome + '</big></li>\n';
                html += '<li>Avaliação \n';
                html += '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ' + advogado.curtidas + '\n';
                html += '<i class="fa fa-hand-o-right" aria-hidden="true"></i> ' + advogado.indicacoes + '\n';
                html += '</li>\n';
                html += '<li>OAB: ' + advogado.oab + '-' + advogado.seccional + '</li>\n';
                html += '<li><strong>Apresentação</strong></li>\n';
                html += '<li>' + advogado.apresentacao + '</li>\n';
                html += '</ul>\n';
                html += '<div class="col-xs-4"></div>\n';
                html += '<div class="col-xs-4"><a href="#close" class="close">VOLTAR</a></div>\n';
                html += '<div class="col-xs-4"><a href="#" class="close" style="display: none;">CONTRATAR</a></div>\n';
                html += '</div>\n';
                html += '<div class="col-xs-12 col-sm-3 col-md-4"></div>\n';
                html += '</div>\n';
            }
        }

        var div = $('#resultado');
        div.html(html);
    },
    error: function () {
        alert('Falha ao carregar objeto.');
    },
    statusCod: {
        404: function () {
            alert('Página não encontrada.');
        }
    }
});